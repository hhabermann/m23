<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for FreeIPA user management.
$*/

class CFreeIPAUser extends CFreeIPA
{





/**
**name CFreeIPAUser::addSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, $password, $shell = '/bin/bash', $sshKey = '')
**description Creates a user (pupil, teacher or admin) with additional school attributes in FreeIPA.
**parameter login: Login name
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter ID: The ID of the pupil or teacher from the Schulverwaltungssoftware.
**parameter role: pupil, teacher, admin.
**parameter school: Name of the school.
**parameter class: Class of the pupil (teacher)
**parameter password: Password
**parameter shell: Shell to use when logged in
**parameter sshPubKey: Public SSH key to allow passwordless login
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function addSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, $password, $shell = '/bin/bash', $sshKey = '')
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

// 		if (!CSchool::isSchoolRoleValid($role))
// 		{
// 			$this->addErrorMessage($I18N_schoolRoleInvalid);
// 			return(false);
// 		}

		$additionParams = '
		"employeenumber": "'.CFreeIPA::safeJSONString($ID).'",
		"employeetype": "'.CFreeIPA::safeJSONString($role).'",
		"title": "'.CFreeIPA::safeJSONString($role).'",
		"l": "'.CFreeIPA::safeJSONString($school).'",
		"street": "'.CFreeIPA::safeJSONString($class).'",
		';

		$group = $this->getUserGroupByRole($school, $role);

		return($this->addUser($login, $firstName, $lastName, $password, $shell, $sshKey, $additionParams) && $this->addLoginToGroup($login, $group));
	}





/**
**name CFreeIPAUser::changeSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, $password = '', $shell = '/bin/bash', $sshKey = '')
**description Changes a user (pupil, teacher or admin) with additional school attributes in FreeIPA.
**parameter login: Login name
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter ID: The ID of the pupil or teacher from the Schulverwaltungssoftware.
**parameter role: pupil, teacher, admin.
**parameter school: Name of the school.
**parameter class: Class of the pupil (teacher)
**parameter password: Password
**parameter shell: Shell to use when logged in
**parameter sshPubKey: Public SSH key to allow passwordless login
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function changeSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, $password = '', $shell = '/bin/bash', $sshKey = '')
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$additionParams = '
		"employeenumber": "'.CFreeIPA::safeJSONString($ID).'",
		"employeetype": "'.CFreeIPA::safeJSONString($role).'",
		"title": "'.CFreeIPA::safeJSONString($role).'",
		"l": "'.CFreeIPA::safeJSONString($school).'",
		"street": "'.CFreeIPA::safeJSONString($class).'",
		';

		if (!$this->removeLoginFromAllGroups($login))
		{
			print("<h4>ERROR: CFreeIPAUser::changeSchoolUser: removeLoginFromAllGroups($login)</h4>");
			return(false);
		}

		$group = $this->getUserGroupByRole($school, $role);
		
		if (!$this->addLoginToGroup($login, $group))
		{
			print("<h4>ERROR: CFreeIPAUser::changeSchoolUser: addLoginToGroup($login, $group)</h4>");
			return(false);
		}

		if (!$this->changeUser($login, $firstName, $lastName, $password, $shell, $sshKey, $additionParams))
		{
			print("<h4>ERROR: CFreeIPAUser::changeSchoolUser: changeUser($login, $firstName, $lastName, $password, $shell, $sshKey, $additionParams)</h4>");
			return(false);
		}

		return(true);
	}





/**
**name CFreeIPAUser::getNextFreeSchoolLogin($firstName, $lastName)
**description Generates a login name by first and last name and an optionally added increasing number.
**parameter firstName: Forename
**parameter lastName: Familyname
**returns Unused login name or false, if no login name could be generated.
**/
	protected function getNextFreeSchoolLogin($firstName, $lastName)
	{
		// Build the initial login name by the 1st letter of the fore and the rest of the last name
		$startWith = strtolower(I18N_replaceUmlaute($firstName[0].$lastName));

		// Search, if there are login names starting with the initial one
		$logins = $this->findLoginsStartingWith($startWith);

		// Check, if there are matching login names
		if (empty($logins) || ($logins == false))
			// No login name found => Use it directly
			return($startWith);
		else
		{
			// Get the amount of existing and matching login names
			$loginAmount = count($logins);

			// Add an increased trailing number
			for ($i = $loginAmount; $i < $loginAmount + 1000; $i++)
			{
				$tryLogin = $startWith.$i;
			
				// Return, if the guessed login doesn't exist
				if (!in_array($tryLogin, $logins))
					return($tryLogin);
			}
		}

		return(false);
	}





/**
**name CFreeIPAUser::addOrChangeSchoolUser($firstName, $lastName, $ID, $role, $school, $class)
**description Adds a user, if the given ID is unknown or changes an existing user found by ID.
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter ID: The ID of the pupil or teacher from the Schulverwaltungssoftware.
**parameter role: Role of the user (CSchool::SCHOOL_ROLE_TEACHER, ::SCHOOL_ROLE_PUPIL, ::SCHOOL_ROLE_SCHOOLADMIN)
**parameter school: Name of the school.
**parameter class: Class of the pupil (teacher)
**parameter password: Password
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function addOrChangeSchoolUser($firstName, $lastName, $ID, $role, $school, $class)
	{
		// Try to find an existing user by the employee number
		$userInfo = $this->findUserByEmployeenumber($ID);

		if (empty($userInfo))
		{
			// User doesn't exist
			$login = $this->getNextFreeSchoolLogin($firstName, $lastName);
			print("add:$login#$ID");

			$password = $this->getInitialPassword($ID, $class, $role);
			// TODO: In Gruppen eintragen: addLoginToGroup($login, $group)
			return($this->addSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, $password));
		}
		else
		{
			// User exists
			$login = $userInfo['uid'];
			$ID = $userInfo['employeenumber'];
			print("change:$login#$ID");
			// TODO: Ggf. Gruppen �ndern
			return($this->changeSchoolUser($login, $firstName, $lastName, $ID, $role, $school, $class, ''));
		}
		
		return(false);
	}





/**
**name CFreeIPAUser::parseCSV($file)
**description Parses a CSV file with information from the Schulverwaltungssoftware to add or change users in FreeIPA.
**parameter file: CSV file to parse
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function parseCSV($file)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$schoolInfo = CSchool::readSchoolInfoFile(NULL);

		// Get short name of the school from the CSV file name
		$parts = explode('.',$file);
		$schoolName = $parts[0];	

		// Try to open the CSV file
		$fp = @fopen($file, "r");

		// Check, if we have a valid file pointer
		if ($fp)
		{
			// Run thru the lines
			while (($line = trim(fgets($fp))) !== false)
			{
				// Split the lines into fields
				$parts = explode(';',$line);

				// Assign the fields to variables
				$ID = $parts[0];
				$role = $parts[1];
				$firstName = $parts[2];
				$lastName = $parts[3];
				$class = $parts[4];
				$deviceType = $parts[5];	// <= Unused
				$deviceID = $parts[6];		// <= Unused
				$deviceMAC = $parts[7];		// <= Unused

				switch ($role)
				{
					case 'L':
					{
						$role = CSchool::SCHOOL_ROLE_TEACHER;
						break;
					}
					case 'S':
					{
						$role = CSchool::SCHOOL_ROLE_PUPIL;
						break;
					}
 					case 'A':
 					{
 						$role = CSchool::SCHOOL_ROLE_SCHOOLADMIN;
 						break;
 					}
					default:
						die("ERROR: Role \"$role\" in \"$line\" unknown!");
				}

				// Make sure, the user groups were created for the given school
				$this->createm23SchoolGroupsIfNeeded($schoolName, $schoolInfo);

				// Add or change users in FreeIPA
				$this->addOrChangeSchoolUser($firstName, $lastName, $ID, $role, $schoolName, $class);
			}

			// After reading all lines the "end of file" should be reached, if not that's an error
			if (!feof($fp))
			{
				$this->addErrorMessage("ERROR reading CSV file \"$file\"");
				return(false);
			}
			fclose($fp);
		}
		else
		{
			$this->addErrorMessage("ERROR reading CSV file \"$file\"");
			return(false);
		}
	}





/**
**name CFreeIPAUser::getInitialPassword($ID, $class, $role)
**description Calculates the initial password with complexity choosen by the user's role (pupil, teacher, admin) and the pupil's age.
**parameter ID: The ID of the pupil or teacher from the Schulverwaltungssoftware.
**parameter class: Class of the pupil (teacher)
**parameter role: Role of the user (CSchool::SCHOOL_ROLE_TEACHER, ::SCHOOL_ROLE_PUPIL, ::SCHOOL_ROLE_SCHOOLADMIN)
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	protected function getInitialPassword($ID, $class, $role)
	{
		require_once('/m23/inc/schoolConfig.php');

		if (!defined('SCHOOL_PASSWD_SALT'))
			die('ERROR: SCHOOL_PASSWD_SALT not defined in /m23/inc/schoolConfig.php');

		// Remove all characters that are not digits, + or -
		$class = (int)filter_var($class, FILTER_SANITIZE_NUMBER_INT);

		// Give out the password with wanted complexity
		if ((CSchool::SCHOOL_ROLE_PUPIL == $role) && ($class <= 4))
			// Reduce password complexity for pupils till 4th class
			return(HELPER_getDerivedPassword(SCHOOL_PASSWD_SALT.$ID, 4, true));
		else
			return(HELPER_getDerivedPassword(SCHOOL_PASSWD_SALT.$ID, 4));
	}





/**
**name CFreeIPAUser::getUserGroupByRole($schoolName, $role)
**description Gets the name of a user group by the school short name and the user's role.
**parameter schoolName: Short name of the school.
**parameter role: Role of the user (CSchool::SCHOOL_ROLE_TEACHER, ::SCHOOL_ROLE_PUPIL, ::SCHOOL_ROLE_SCHOOLADMIN)
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	protected function getUserGroupByRole($schoolName, $role)
	{
		switch ($role)
		{
			case CSchool::SCHOOL_ROLE_TEACHER:
				return($this->getTeacherUserGroup($schoolName));
			case CSchool::SCHOOL_ROLE_PUPIL:
				return($this->getPupilUserGroup($schoolName));
			case CSchool::SCHOOL_ROLE_SCHOOLADMIN:
				return($this->getSchooladminUserGroup($schoolName));
			default:
				die("ERROR: Role \"$role\" unknown!");
		}
	}
}
?>