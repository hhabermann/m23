<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for school administration.
$*/

//CLdap
class CSchool extends CSchoolFreeIPA
{
	const
		SCHOOL_INFO_FILE = '/m23/inc/schoolInfoConf.php',
		SCHOOL_ROLE_TEACHER = 'teacher',
		SCHOOL_ROLE_PUPIL = 'pupil',
		SCHOOL_ROLE_SCHOOLADMIN = 'schooladmin',
		SCHOOL_RANGE_SORT_FIRST = 'LAN_EDU';

	private $schoolInfo = NULL,
		$range = NULL,
		$domain = NULL;





/**
**name CSchool::__construct($schoolName, $domain)
**description Constructor for new CSchool objects.
**parameter schoolName: Short name of the school.
**parameter domain: The domain of the school including the network range and optional the client name.
**/
	public function __construct($schoolName, $domain)
	{
		// Import the information of the given school
		$this->schoolInfo = $this->readSchoolInfoFile($schoolName);

		$this->domain = $domain;
		$this->range = CFreeIPA::getRangeFromDomain($domain);

		try
		{
			// Run the parent's constructor depending on parent's class
			if ($this->usesLDAPKerberos())
				parent::__construct(CLdap::get2ndLDAPServer());
			elseif($this->usesFreeIPA())
				parent::__construct();
		}
		catch (Exception $e)
		{
			MSG_showError($e->getMessage());
// 			throw new Exception(false);
		}


		

		// $description = "School: $schoolName";
		$description = $this->schoolInfo['description'];

		// Create a client group with the name of the school
		if (!GRP_exists($schoolName))
		{
// 			print("<h3>createm23Subgroups:GRP_add($schoolName, $description)</h3>");
			GRP_add($schoolName, $description, false);
		}


		// Create user/host groups and other structure in Kerberos/LDAP or FreeIPA
/*		if (true || !$this->isSchoolExisting($schoolName)) //DEBUG
		{*/
// 			$this->createSchoolStructure($schoolName, $description);	<= Not used in FreeIPA environments, but MAY be used with LDAP Kerberos
			$this->createm23Subgroups($schoolName, $description);
// 		}
		
		$this->setSchool($schoolName);


// 		$this->addErrorMessage('TEST');
		
// 		print($this->getFullDNByFdRDN('fdworkstationrdn'));
	}






/**
**name CSchool::getm23SubHostGroupsPostfixes()
**description Creates an array with the postfixes of the m23 subgroups for hosts.
**parameter Array with the postfixes of the m23 subgroups as key and their description as value.
**/
	public static function getm23SubHostGroupsPostfixes()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		return(array('TS' => $I18N_terminalServer, 'S' => $I18N_pupilPCs, 'L' => $I18N_teacherPCs, 'W' => $I18N_whiteboardPCs, 'TAB' => $I18N_tablets));
	}





/**
**name CSchool::checkNewgroupBeforeClientAdd($schoolName, $newgroup)
**description Checks, if the needed groups are selected in the needed amount
**parameter schoolName: Short name of the school.
**parameter newgroup: Array with selected groups.
**returns Error message(s) in case of errors or empty string when all is ok.
**/
	public static function checkNewgroupBeforeClientAdd($schoolName, $newgroup)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$err = '';

		// Check, if the main school group is selected
		if (!in_array($schoolName, $newgroup))
			$err .= "&bull; $I18N_schoolMainGroupNotSelected</br>";

		// Check, if the default group is selected
		if (!in_array('default', $newgroup))
			$err .= "&bull; $I18N_defaultGroupNotSelected</br>";

		// Count the selected m23subgroups
		$usedm23Subgroups = 0;
		foreach (CSchool::getm23SubHostGroupsPostfixes() as $posfix => $null)
			if (in_array("${schoolName}_$posfix", $newgroup))
				$usedm23Subgroups++;

		// Only exactly one subgroup is allowed
		if (0 == $usedm23Subgroups)
			$err .= "&bull; $I18N_noProfileGroupSelected</br>";

		if ($usedm23Subgroups > 1)
			$err .= "&bull; $I18N_moreThanOneProfileGroupSelected</br>";

		return($err);
	}





/**
**name CSchool::getSchoolGrades() <= PROTOTYPE
**description Get the grades of a school.
**parameter schoolName: Short name of the school.
**returns Array with the school grades as key and value.
**/
	public static function getSchoolGrades($schoolName)
	{
		$grades = array();

		for ($i = 1; $i <= 13; $i++)
			foreach (array('a', 'b') as $j)
				$grades["$i$j"] = "$i$j";
		
		return($grades);
	}





/**
**name CSchool::getUserSchoolRoles($addAdmin = true)
**description Gets an array with the school roles.
**parameter: addAdmin: If set to true, the admin role will be included in the array.
**returns Array with the school roles: Nummeric value as key, description as value.
**/
	public static function getUserSchoolRoles($addAdmin = true)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		
		$out = array();
		$out[CSchool::SCHOOL_ROLE_TEACHER] = $I18N_teacher;
		$out[CSchool::SCHOOL_ROLE_PUPIL] = $I18N_pupil;

		if ($addAdmin)
			$out[CSchool::SCHOOL_ROLE_SCHOOLADMIN] = $I18N_schoolAdmin;

		return($out);
	}





/**
**name CSchool::isSchoolRoleValid($role)
**description Checks, if a given school role exists.
**parameter: role: Role name to check.
**returns true, if the role is valid, otherwise false.
**/
	public static function isSchoolRoleValid($role)
	{
		return(in_array($role, array_keys(CSchool::getUserSchoolRoles())));
	}





/**
**name CSchool::isSchoolModeActive()
**description Checks, if the school mode is active.
**returns true, if the school mode is active, otherwise false.
**/
	public static function isSchoolModeActive()
	{
		return(file_exists(CSchool::SCHOOL_INFO_FILE));
	}





/**
**name CSchool::getSchoolNameWithDescription()
**description Get an array with the short school names as key and the short school names with description as value.
**returns Associative array with short school names and their descriptions.
**/
	public static function getSchoolNameWithDescription()
	{
		$out = array();
	
		$schoolInfo = CSchool::readSchoolInfoFile(NULL);

		foreach ($schoolInfo as $si => $info)
			$out[$si] = "$si - $info[description]";

		return($out);
	}





/**
**name CSchool::getSchoolNames()
**description Get the names of all known schools.
**returns Associative array with the names of all known schools as key and value.
**/
	public static function getSchoolNames()
	{
		return(HELPER_array2AssociativeArray(array_keys(CSchool::readSchoolInfoFile(NULL))));
	}





/**
**name CSchool::getLongSchoolName()
**description Get the long name of a school.
**parameter schoolName: Short name of the school.
**returns Long name of the given school.
**/
	public static function getLongSchoolName($schoolName)
	{
		$schoolInfo = CSchool::readSchoolInfoFile($schoolName);
		return($schoolInfo['description']);
	}





/**
**name CSchool::readSchoolInfoFile($schoolName)
**description Reads and parses the school info file for a given school.
**parameter schoolName: Name of the school or NULL, if all schools should be given out.
**returns Information about the given school or all schools
**/
	public static function readSchoolInfoFile($schoolName)
	{
		// Check for the school info file
		if (!file_exists(CSchool::SCHOOL_INFO_FILE))
			die('ERROR: '.CSchool::SCHOOL_INFO_FILE.' is missing!');

		// Read the school info file
		include(CSchool::SCHOOL_INFO_FILE);
		
		if (!isset($schoolInfo))
			die('ERROR: School "$schoolInfo" not defined in '.CSchool::SCHOOL_INFO_FILE.'!');

		// Return full school info array if no school is given
		if (is_null($schoolName))
			return($schoolInfo);

		// Check, if the given school is defined as key
		if (!isset($schoolInfo[$schoolName]))
			die('ERROR: School "'.$schoolName.'" not defined in '.CSchool::SCHOOL_INFO_FILE.'!');

		// Return information for the given school
		return($schoolInfo[$schoolName]);
	}





/**
**name CSchool::getRangeInfo($option)
**description Get the value of an option from a range in the school info.
**parameter option: Name of the option.
**returns Value of the option (or dies if no value is present).
**/
	private function getRangeInfo($option)
	{
		// Check, if range and option are present
		if (!isset($this->schoolInfo[$this->range]) || !isset($this->schoolInfo[$this->range][$option]))
			die('ERROR: $this->schoolInfo['.$this->range.']['.$option.'] invalid!');

		return($this->schoolInfo[$this->range][$option]);
	}





/**
**name CSchool::getNetmask()
**description Get the netmask for m23 clients.
**returns Netmask for m23 clients.
**/
	public function getNetmask()
	{
		return($this->getRangeInfo('netmask'));
	}





/**
**name CSchool::getGateway()
**description Get the gateway for m23 clients.
**returns Gateway for m23 clients.
**/
	public function getGateway()
	{
		return($this->getRangeInfo('gateway'));
	}





/**
**name CSchool::getDNS()
**description Get the DNS server for m23 clients.
**returns DNS server for m23 clients.
**/
	public function getDNS()
	{
		return($this->getRangeInfo('dns1'));
	}






/**
**name CSchool::getNextFreeIP($amount)
**description Returns an array with free IPs or a single free IP that can be used for the next client(s).
**parameter amount: Amount of IDs to generate.
**returns Array with free IPs in the given amount or a single free IP.
**/
	public function getNextFreeIP($amount)
	{
		// Array for all possible IPs (converted to long)
		$freeIPsL = array();

		// Get minimum and maximum possible IPs (converted to long)
		$minimumIPL = ip2longSafe($this->getRangeInfo('min'));
		$maximumIPL = ip2longSafe($this->getRangeInfo('max'));

		// All free possible IPs
		for ($i = $minimumIPL; $i <= $maximumIPL; $i++)
			$freeIPsL[$i] = $i;

		// TO-DO: Remove other IPs: 172.28.40.201-253 Switchs, 172.28.40.101-199 APs
		// Get all IPs that are handled by m23
		foreach ($this->getUsedIPs(true, true) as $unwantedIP)
			if (isset($freeIPsL[$unwantedIP]))
				unset($freeIPsL[$unwantedIP]);

		// Multiple IPs or single ID?
		if (is_null($amount) && ($amount > 1))
			// Give out the free IPs in the needed amount (counting starts from 0 and NOT from the key that is $minimumID)
			return(array_map('long2ip', array_slice($freeIPsL, 0, $amount)));
		else
			// Return the first IP from the array (key won't be 0, so reset gives the first element)
			return(long2ip((int)reset($freeIPsL)));
	}





/**
**name CSchool::getUsedIPs($intIPs = false, $allM23IPs)
**description Gets the IPs that are managed by the Kerberos/LDAP or the FreeIPA server.
**parameter intIPs: Set to true, to return the IPs as int values.
**parameter allM23IPs: Set to true, to get all IPs that are handled by m23.
**returns Array with used IPs.
**/
	public function getUsedIPs($intIPs = false, $allM23IPs)
	{
		$usedIPs = array();

		$sqlAllM23IPs = $allM23IPs ? ' OR school = ""' : '';
	
		// Get the IPs of all clients in the given school from SQL
		$sql = 'SELECT INET_ATON( ip ) AS intip, client FROM clients WHERE school = "'.$this->schoolName.'"'.$sqlAllM23IPs;
		$result = DB_query($sql);

		// Add the used IPs
		while ($line = mysqli_fetch_row($result))
			$usedIPs[$line[0]] = (!$intIPs ? long2ip($line[0]) : $line[0]);

		// Array with other unwanted IPs
		$serverIP = ip2longSafe(getServerIP());
		$usedIPs[$serverIP] = ($intIPs ? ip2longSafe($serverIP) : $serverIP);
		foreach (getDNSServers() as $dns)
			$usedIPs[$dns] = ($intIPs ? ip2longSafe($dns) : $dns);
		$gateway = getServerGateway();
		$usedIPs[$gateway] = ($intIPs ? ip2longSafe($gateway) : $gateway);
		// Add used IPs from LDAP, if LDAP/Kerberos is used
		if ($this->usesLDAPKerberos()) $usedIPs = array_merge($usedIPs, $this->getUsedLDAPIPs($intIPs));
		// FreeIPA server was integrated (config file present) => Add the IPs (as int) that are handled by FreeIPA
		if ($this->usesFreeIPA()) $usedIPs = array_merge($usedIPs, $this->getUsedFreeIPAIPs($this->domain, $intIPs));

		return($usedIPs);
	}





/**
**name CSchool::IPexists($ip, $addErrorMessageIfIPExists = false))
**description Checks if an IP with the selected IP exists in Kerberos/LDAP or the FreeIPA for the current school.
**parameter ip: IP to check
**parameter addErrorMessageIfIPExists: Set to true, if an error message should be added to the message stack, if the IP exists
**returns true, if the IP exists (is used), otherwise false.
**/
	public function IPexists($ip, $addErrorMessageIfIPExists = false)
	{
		$isUsed = in_array($ip, $this->getUsedIPs(false, false));
// ���
		// Add an error message to the message stack if the IP exists
		if ($addErrorMessageIfIPExists && $isUsed)
		{
			include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
			$this->addErrorMessage("$I18N_ip_exists ($ip/CSchool::IPexists)");
		}

		return($isUsed);
	}





/**
**name CSchool::usesLDAPKerberos()
**description Checks, if LDAP/Kerberos with FusionDirectory is used for user and groups management.
**returns true, if LDAP/Kerberos with FusionDirectory is used for user and groups management, otherwise false.
**/
	private function usesLDAPKerberos()
	{
		return(get_parent_class($this) == 'CSchoolLDAPKerberos');
	}





/**
**name CSchool::usesFreeIPA()
**description Checks, if FreeIPA is used for user and groups management.
**returns true, if FreeIPA is used for user and groups management, otherwise false.
**/
	private function usesFreeIPA()
	{
		return(get_parent_class($this) == 'CSchoolFreeIPA');
	}





/**
**name CSchool::createCSchoolObject($schoolName, $domain, &$errorMsg)
**description Tries to create a CSchool object.
**parameter schoolName: Name if the school to create the object for.
**parameter domain: The domain of the school including the network range and optional the client name.
**parameter errorMsg: In case of an error the error message is written into this variable.
**returns CSchool object on success, otherwise false.
**/
	public static function createCSchoolObject($schoolName, $domain, &$errorMsg)
	{
		try
		{
			$CSchoolO = new CSchool($schoolName, $domain);
		}
		catch (Exception $e)
		{
			$errorMsg = $e->getMessage();
		}

		return(isset($CSchoolO) && is_object($CSchoolO) ? $CSchoolO : false);
	}
}
?>