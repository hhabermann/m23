<?php
/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Functions for using a LDAP server
$*/


/*
	BUG?
	$servers->newServer('ldap_pla'); fehlt zwischen neuem REALM LDAP-Server am Anfang und dem alten (dc=nodomain) in /m23/data+scripts/m23admin/phpldapadmin/config/config.php.
*/


define('PHPLDAPADMIN_CONFIG', '/m23/data+scripts/m23admin/phpldapadmin/config/config.php');
define('PHPLDAPADMIN_CONFIGDIR', '/m23/data+scripts/m23admin/phpldapadmin/config');





/**
**name LDAP_getPasswordHashTypes()
**description Returns an array with the known password hash types.
**returns Array with the known password hash types.
**/
function LDAP_getPasswordHashTypes()
{
	return(array('ssha' => 'ssha', 'md5' => 'md5'));
}





/**
**name LDAP_getSSHA($pwd)
**description Hashes a password in SSHA as LDAP/PAM expects it.
**parameter pwd: Password to hash.
**returns Hashed password.
**/
function LDAP_getSSHA($pwd)
{
	return('{CRYPT}'.crypt($pwd, '$6$rounds=5000$'.(HELPER_generateSalt(16).'$')));
}





/**
**name LDAP_getMD5($pwd)
**description Hashes a password in MD5 as LDAP/PAM expects it.
**parameter pwd: Password to hash.
**returns Hashed password.
**/
function LDAP_getMD5($pwd)
{
	return('{MD5}'.base64_encode(pack('H*',md5($pwd))));
}





/**
**name LDAP_needsBase64UTF8($in)
**description Checks, if a string must be converted to UTF-8 and Base64 because it contains non-ASCII characters.
**parameter in: String to check.
**returns true, of converting is needed, otherwise flase.
**/
function LDAP_needsBase64UTF8($in)
{
	return(I18N_replaceUmlaute($in) != $in);
}





/**
**name LDAP_getBase64UTF8($in, $forceConvert = false)
**description Converts a string to UTF-8 and Base64.
**parameter in: String to convert.
**parameter forceConvert: Force conversion.
**returns String in UTF-8 and Base64
**/
function LDAP_getBase64UTF8($in, $forceConvert = false)
{
	if ($forceConvert || LDAP_needsBase64UTF8($in))
		return(':'.base64_encode(I18N_safeUTF8_encode($in)));
	else
		return($in);
}





/**
**name LDAP_getTypes()
**description Returns an array with the known LDAP types.
**returns Array with the known LDAP types.
**/
function LDAP_getTypes()
{
	return(array('none', 'read', 'write', 'FreeIPARead', 'FreeIPAWrite'));
}





/**
**name LDAP_connectServer($name)
**description Connects with read/write access to the LDAP server on the m23 server.
**parameter name: name of the LDAP server stored in the configuration file
**/
function LDAP_connectServer($name, &$server2 = NULL)
{
	$server = LDAP_loadServer($name);
	
	$ds = LDAP_makeConnection($server['host'] , $server['base'], $server['login_pass']);
	
	$server2 = $server;

	return($ds);
};





/**
**name LDAP_makeConnection($host, $baseDN, $pwd="")
**description Connects to a LDAP server.
**parameter host: hostname or IP of the LDAP server
**parameter baseDN: baseDN for the LDAP server
**parameter pwd: password for the administrator or empty for anonymous access
**/
function LDAP_makeConnection($host, $baseDN, $pwd="")
{
// 	ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);

	$ds = ldap_connect("$host");

	ldap_set_option( $ds, LDAP_OPT_PROTOCOL_VERSION, 3 );

	if (!empty($pwd))
		$r=ldap_bind($ds,"cn=admin,$baseDN", $pwd);
	else
		$r=ldap_bind($ds);

	if ($r === FALSE)
		return($r);

	return($ds);
}





/**
**name LDAP_getValueFromConfigLine($line)
**description Returns the value from a phpLDAPadmin configuration line.
**returns Value from a phpLDAPadmin configuration line.
**/
function LDAP_getValueFromConfigLine($line)
{
	$value = preg_replace("/\\\$servers->setValue\('[^']*','[^']*',[']?/", '', $line);

	// Remove unwanted parts around the value
	$value = str_replace ('array(\'' , '' , $value);
	$value = str_replace ('\'))' , '' , $value);
	$value = str_replace ('\')' , '' , $value);
	$value = str_replace (')' , '' , $value);
	$value = str_replace ('//' , '' , $value);
	$value = trim($value);

	return($value);
}





/**
**name LDAP_listServers()
**description Returns an associative array with the LDAP server names as keys and values.
**returns Associative array with the LDAP server names as keys and values.
**/
function LDAP_listServers()
{
	$configTxt = SERVER_getFileContents(PHPLDAPADMIN_CONFIG);
	$lines = explode(';', $configTxt);

	$out = array();

	// Run thru the lines of the config file (interesting parts are separated by ';')
	foreach ($lines as $line)
	{
		// Get the lines that contain LDAP server names
		if (strpos($line, "'server','name','") !== false)
		{
			// Read out the LDAP server name and add it to the output array as key and value
			$val = LDAP_getValueFromConfigLine($line);
			$out[$val] = $val;
		}
	}

	return($out);
}





/**
**name LDAP_loadServer($name)
**description Loads the variables from a LDAP server.
**parameter name: server name
**/
function LDAP_loadServer($name)
{
	$configTxt = SERVER_getFileContents(PHPLDAPADMIN_CONFIG);
	$lines = explode(';', $configTxt);
	$serverFound = false;
	$server = array();

	// Array containing the variable names of the config values as key and the keys of the output array as values
	$wantedVariables = array('host' => 'host', 'base' => 'base', 'port' => 'port', 'bind_pass' => 'login_pass', 'kdcServicePass' => 'kdcServicePass', 'kadminServicePass' => 'kadminServicePass', 'KRB_realm' => 'KRB_realm', 'KRB_server' => 'KRB_server', 'KRB_adminServer' => 'KRB_adminServer', 'KRB_defaultDomain' => 'KRB_defaultDomain', 'min' => 'min', 'passwordHashAlgorithm' => 'passwordHashAlgorithm');

	// Run thru the lines of the config file (interesting parts are separated by ';')
	foreach ($lines as $line)
	{
		// It is time to quit, if there is another LDAP server name line after the wanted server
		if ($serverFound && (strpos($line, "'server','name','") !== false))
			return($server);

		// Check, if the line contains the correct LDAP server
		if (strpos($line, "'server','name','$name'") !== false)
		{
			$serverFound = true;
			$server['name'] = $name;
		}
		else
		{
			// Check, if the line contains wanted variable names
			foreach ($wantedVariables as $varFile => $var)
				if (strpos($line, "','$varFile',") !== false)
				{
					$val = LDAP_getValueFromConfigLine($line);
					// min variable contains an array in string represenattion that must be parsed
					if ('min' == $var)
					{
						$val = HELPER_arrayString2AssociativeArray("'$val");
						$server['uidMin'] = $val["'uidNumber'"];
						$server['gidMin'] = $val["'gidNumber'"];
					}
					else
						$server[$var] = $val;
				}
		}
	}

	// Set md5 for backwards compatibility, if "passwordHashAlgorithm" is unset on old m23 installations
	if (!isset($server['passwordHashAlgorithm'])) $server['passwordHashAlgorithm'] = 'md5';

	return($server);
}





/**
**name LDAP_addPosix($ldapServer,$account,$forename,$familyname,$pwd,$uid,$gid)
**description Adds a posix account to the LDAP server and encrypts the password with MD5.
**parameter ldapServer: name of the LDAP server stored in the configuration file
**parameter account: the login name
**parameter forename: the forename of the user
**parameter familyname: the familyname of the user
**parameter pwd: the unencrypted password
**parameter uid: Linux user ID
**parameter gid: Linux group ID
**returns true or error message string in case of an error.
**/
function LDAP_addPosix($ldapServer,$account,$forename,$familyname,$pwd,$uid,$gid)
{
	include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

	$forename=utf8_encode($forename);
	$familyname=utf8_encode($familyname);
	if (empty($familyname))
		$familyname="-";

	$server = LDAP_loadServer($ldapServer);
	$ds = LDAP_connectServer($ldapServer);
	
	if ($ds === false)
		return(false);
	
// 	print(serialize($ds));


// 	'{CRYPT}'.crypt($pwd, '$6$rounds=5000$'.(HELPER_generateSalt(16).'$'))
// cn=config
// 
// olcPasswordHash: {CRYPT}
// olcPasswordCryptSaltFormat: $6$%.16s


	// Build the array for creating a new user entry
	$out=array();
	$out['uid'][0]=$account;
	$out['gn'][0]=$forename;
	$out['sn'][0]=$familyname;
	$out['cn'][0]="$forename $familyname";
	$out['userPassword'][0]='{MD5}'.base64_encode(pack('H*',md5($pwd)));
	$out['loginShell'][0]="/bin/bash";
	$out['uidNumber'][0]=$uid;
	$out['gidNumber'][0]=$gid;
	$out['homeDirectory'][0]="/home/$account";
	$out['shadowMin'][0]=0;
	$out['shadowMax'][0]=999999;
	$out['shadowWarning'][0]=7;
	$out['shadowInactive'][0]=0;
	$out['shadowExpire'][0]=999999;
	$out['shadowFlag'][0]=0;
	$out['objectClass'][0]="top";
	$out['objectClass'][1]="person";
	$out['objectClass'][2]="posixAccount";
	$out['objectClass'][3]="shadowAccount";
	$out['objectClass'][4]="inetOrgPerson";


	// Add the new user
	$dn = "uid=$account,ou=people,$server[base]";
// 	print("<h4>@ldap_add($ds , $dn</h4>");
// 	print_r($out);
	$ret = @ldap_add($ds , $dn, $out);
	if ($ret === false)
	{
		$err = "$I18N_LDAPcouldNotCreatePosixAccount ($dn): ".ldap_error($ds).'/'.ldap_errno($ds);
		ldap_close($ds);
		return($err);
	}
	
// 	print('<h4>'.ldap_error($ds).'</h4>');

	// Create a group name by the GID
	$groupname = "gid$gid";

	// Build the array for creating a new group entry
	$out=array();
	$out['cn'][0] = $groupname;
	$out['gidNumber'][0] = $gid;
	$out['memberUid'] = $account;
	$out['objectClass'][0]="top";
	$out['objectClass'][1]="posixGroup";

	// Try to create the new group
	$dn="cn=$groupname,ou=Group,$server[base]";
	$ret = @ldap_add($ds, $dn, $out);
// 	print('<h4>'.ldap_error($ds).'</h4>');
	if (false === $ret)
	{
		// The group seems to exist => Add the uid to list of users who are in the group
		$miniOut=array();
		$miniOut['memberUid'] = $uid;
		$ret = @ldap_mod_add($ds, $dn, $miniOut);
		if ($ret === false)
		{
			$err = "$I18N_LDAPcouldNotCreatePosixGroup ($dn): ".ldap_error($ds).'/'.ldap_errno($ds);
			ldap_close($ds);
			return($err);
		}
	}

	DB_addNewUserID($uid);
	DB_addNewGroupID($gid);

	ldap_close($ds);

	return(true);
}





/**
**name LDAP_fqdn2dn($domain)
**description Returns the DN converted from a FQDN
**parameter domain: a full qualified domain name (e.g. test.m23.de)
**/
function LDAP_fqdn2dn($domain)
{
	if (!(strpos($domain,"dc=")===FALSE))
	{
		$domain=str_replace("\.",", dc=",$domain);
		$domain="dc=".$domain;
	}
	return($domain);
}





/**
**name LDAP_addServerTophpLdapAdmin($name, $host, $base, $pwd, $port=389, $minUidNumber = 1000, $minGidNumber = 1000, $passwordHashAlgorithm = 'ssha', $additionalA = array())
**description Adds an LDAP server to the phpLDAPadmin configuration file.
**parameter name: how the LDAP server should be called
**parameter host: the IP or hostname of the LDAP server
**parameter base: the base DN (e.g. dc=m23, dc=de)
**parameter pwd: the unencrypted password for the admin
**parameter port: Port number, the LDAP server runs on.
**parameter minUidNumber: Minimum user ID.
**parameter minGidNumber: Minimum group ID.
**parameter passwordHashAlgorithm: Algorithm for hashing the passwords.
**parameter additionalA: Associative array with additional variables and values (eg. array('realm' => 'REALM', 'krbpass' => 'test'))
**/
function LDAP_addServerTophpLdapAdmin($name, $host, $base, $pwd, $port=389, $minUidNumber = 1000, $minGidNumber = 1000, $passwordHashAlgorithm = 'ssha', $additionalA = array())
{
	//##################################################################
	// !!! ADJUST m23-ldap/postinst whenever parameters are changing!!!
	//##################################################################


	// Check parameters for emptyness
	foreach (array('name' => $name, 'host' => $host, 'base' => $base, 'pwd' => $pwd, 'port' => $port, 'minUidNumber' => $minUidNumber, 'minGidNumber' => $minGidNumber, 'passwordHashAlgorithm' => $passwordHashAlgorithm) as $var => $val)
		if (empty($val))
			die("ERROR: Parameter \"$var\" is empty!\n");

	// Generate lines with additional variables and values that are commented out but parsable LDAP_getValueFromConfigLine
	$additionalS = '';
	foreach($additionalA as $var => $val)
		$additionalS .= "\n//\$servers->setValue('additional','$var','$val');";

LDAP_checkphpLdapAdminConfiguration();

$cmd=
EDIT_searchLastLineNumber(PHPLDAPADMIN_CONFIG, "new Datastore();").
EDIT_insertAfterLineNumber(PHPLDAPADMIN_CONFIG, SED_foundLine, "
\$servers->newServer('ldap_pla');
\$servers->setValue('server','name','$name');
\$servers->setValue('server','host','$host');
\$servers->setValue('server','port',$port);
\$servers->setValue('server','base',array('$base'));
\$servers->setValue('login','auth_type','config');
\$servers->setValue('login','bind_id','cn=admin,$base');
\$servers->setValue('login','bind_pass','$pwd');
\$servers->setValue('server','tls',false);
\$servers->setValue('server','read_only',false);
\$servers->setValue('auto_number','enable',false);
\$servers->setValue('auto_number','mechanism','search');
\$servers->setValue('auto_number','search_base','ou=People,$base');
\$servers->setValue('auto_number','min',array('uidNumber'=>$minUidNumber,'gidNumber'=>$minGidNumber));
\$servers->setValue('server','visible',true);
//\$servers->setValue('passwordHashAlgorithm','passwordHashAlgorithm','$passwordHashAlgorithm');$additionalS
");

SERVER_runInBackground("LDAP_addServerTophpLdapAdmin", $cmd, 'root', false);
}





/**
**name LDAP_delServerFromphpLdapAdmin($name)
**description Deletes a LDAP server from the phpLDAPadmin configuration file.
**parameter name: the name of the LDAP server that should be deleted
**/
function LDAP_delServerFromphpLdapAdmin($name)
{
	SERVER_runInBackground("LDAP_delServerFromphpLdapAdmin", "/m23/bin/deleteLDAPServerFromConfig $name", 'root', false);
}





/**
**name LDAP_checkphpLdapAdminConfiguration()
**description Checks if the phpLDAPadmin configuration file is existing and creates it if it's missing
**/
function LDAP_checkphpLdapAdminConfiguration()
{
	// Check, if the new config file exists. If yes, nothing is to do here
	if (file_exists(PHPLDAPADMIN_CONFIG)) return(true);

	// Make sure the directory for storing the new openLDAPadmin config file exists
	SERVER_multiMkDir(PHPLDAPADMIN_CONFIGDIR, 0755);

$file = fopen(PHPLDAPADMIN_CONFIG, "w");
fwrite($file,"<?php
\$config->custom->appearance['friendly_attrs'] = array(
	'facsimileTelephoneNumber' => 'Fax',
	'gid'                      => 'Group',
	'mail'                     => 'Email',
	'telephoneNumber'          => 'Telephone',
	'uid'                      => 'User Name',
	'userPassword'             => 'Password'
);

\$servers = new Datastore();
?>
");
fclose($file);
};





/**
**name LDAP_showServerManagementDialog()
**description Shows a dialog for adding, removing and changing LDAP servers.
**/
function LDAP_showServerManagementDialog()
{
	$installStarted = false;
	$htmlLDAPType = '';

	include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
	LDAP_checkphpLdapAdminConfiguration();

	$SEL_server = isset($_POST['SEL_server']) ? $_POST['SEL_server'] : '';
	$ED_servername = isset($_POST['ED_servername']) ? $_POST['ED_servername'] : '';
	$ED_serverhost = isset($_POST['ED_serverhost']) ? $_POST['ED_serverhost'] : '';
	$ED_baseDN = isset($_POST['ED_baseDN']) ? $_POST['ED_baseDN'] : '';
	$ED_serverport = isset($_POST['ED_serverport']) ? $_POST['ED_serverport'] : '';
	$ED_uidMin = isset($_POST['ED_uidMin']) ? $_POST['ED_uidMin'] : '';
	$ED_gidMin = isset($_POST['ED_gidMin']) ? $_POST['ED_gidMin'] : '';
	$SEL_passwordHash = isset($_POST['SEL_passwordHash']) ? $_POST['SEL_passwordHash'] : 'ssha';
	$ED_password1 = isset($ED_password1) ? $ED_password1 : '';
	$ED_password2 = isset($ED_password2) ? $ED_password2 : '';
	

	if (empty($ED_serverport)) $ED_serverport = 389;
	if (empty($ED_uidMin)) $ED_uidMin = 1000;
	if (empty($ED_gidMin)) $ED_gidMin = 1000;

	$servers = LDAP_listServers();
	
	/*
		changing a LDAP server means:
		1. delete the old entry like done by BUT_delete
		2. add a new entry like done by BUT_add
	*/

	//delete a LDAP server from the configuration file
	if ((isset($_POST['BUT_delete'])) || (isset($_POST['BUT_change'])))
	{
		LDAP_delServerFromphpLdapAdmin($SEL_server);
		$servers = LDAP_listServers();
		
		//if the entry should be deleted only (not changed) the dialog values should be resetted
		if (!isset($_POST['BUT_change']))
		{
// 			$SEL_server=$ED_servername=$ED_serverhost=$ED_baseDN="";
			MSG_showInfo($I18N_LDAPServerDeleted);
		}
	}

	//add a new LDAP server to the configuration file
	if (isset($_POST['BUT_add']) || (isset($_POST['BUT_change'])))
	{
		if ($_POST['ED_password1'] == $_POST['ED_password2'])
		{
			if (in_array($ED_servername,$servers))
				MSG_showError($I18N_LDAPServerExists);
			elseif (LDAP_makeConnection($ED_serverhost,$ED_baseDN,$_POST['ED_password1']) === FALSE)
				MSG_showError($I18N_LDAPServerConnectionRefused);
			else
			{
				LDAP_addServerTophpLdapAdmin($ED_servername,$ED_serverhost,$ED_baseDN,$_POST['ED_password1'],$ED_serverport,$ED_uidMin, $ED_gidMin,$SEL_passwordHash);
				$servers = LDAP_listServers();
//				$ED_servername = $ED_serverhost = $ED_baseDN = "";
				
				if (!isset($_POST['BUT_change']))
					MSG_showInfo($I18N_LDAPServerAdded);
				else
				{	//if it should be changed, the selection should be the new server name
					MSG_showInfo($I18N_LDAPServerChanged);
					$SEL_server=$ED_servername;
				}
			}
		}
		else
			MSG_showError($I18N_passwords_dont_match);
	}


	//loads the variables from a LDAP server in the dialog
	if (isset($_POST['BUT_load']))
	{
		$server = LDAP_loadServer($SEL_server);
		$ED_servername = $server['name'];
		$ED_serverhost = $server['host'];
		$ED_baseDN = $server['base'];
		$ED_serverport = $server['port'];
		$ED_password1 = $ED_password2 = $server['login_pass'];
		$SEL_passwordHash = $server['passwordHashAlgorithm'];
		if (isset($server['uidMin'])) $ED_uidMin = $server['uidMin'];
		if (isset($server['gidMin'])) $ED_gidMin = $server['gidMin'];

		if (empty($server['login_pass']))
			$htmlLDAPType="<br><span class=\"subhighlight_red\">$I18N_readOnlyLDAPServer</span>
			<br><br>";
		else
			$htmlLDAPType="<br><span class=\"subhighlight_red\">$I18N_readWriteLDAPServer</span><br><br>";
	}
	
	
	$passwordHashTypes = LDAP_getPasswordHashTypes();

	HTML_showTableHeader();
	echo("
	<form method=\"post\">
		<tr>
			<td>".HTML_listSelection("SEL_server",$servers,$SEL_server)."</td>
			<td align=\"center\">
				<input type=\"submit\" value=\"$I18N_load\" name=\"BUT_load\">
				<input type=\"submit\" value=\"$I18N_delete\" name=\"BUT_delete\">
			</td>
		</tr>

		<tr>
			<td colspan=\"2\"><hr></td>
		</tr>

		<tr>
			<td>$I18N_LDAPServerName</td>
			<td>
				<input type=\"text\" name=\"ED_servername\" value=\"$ED_servername\" size=\"20\" maxlength=\"200\">
			</td>
		</tr>
		
		<tr>
			<td>$I18N_LDAPServerHost</td>
			<td>
				<input type=\"text\" name=\"ED_serverhost\" value=\"$ED_serverhost\" size=\"20\" maxlength=\"200\">
			</td>
		</tr>
		
		<tr>
			<td>$I18N_LDAPServerPort</td>
			<td>
				<input type=\"text\" name=\"ED_serverport\" value=\"$ED_serverport\" size=\"6\" maxlength=\"6\">
			</td>
		</tr>
		
		<tr>
			<td>$I18N_baseDN</td>
			<td>
				<input type=\"text\" name=\"ED_baseDN\" value=\"$ED_baseDN\" size=\"20\" maxlength=\"200\">
			</td>
		</tr>

		<tr>
			<td>$I18N_minimumUserID</td>
			<td>
				<input type=\"text\" name=\"ED_uidMin\" value=\"$ED_uidMin\" size=\"6\" maxlength=\"6\">
			</td>
		</tr>

		<tr>
			<td>$I18N_minimumGroupID</td>
			<td>
				<input type=\"text\" name=\"ED_gidMin\" value=\"$ED_gidMin\" size=\"6\" maxlength=\"6\">
			</td>
		</tr>

		<tr>
			<td>$I18N_passwordHashAlgorithm</td>
			<td>
				".HTML_listSelection("SEL_passwordHash", $passwordHashTypes, $SEL_passwordHash)."
			</td>
		</tr>


		<tr>
			<td>$I18N_password</td>
			<td><input type=\"password\" name=\"ED_password1\" value=\"".(isset($ED_password1) ? $ED_password1 : '')."\" size=\"20\" maxlength=\"200\"></td>
		</tr>

		<tr>
			<td>$I18N_repeated_password</td>
			<td><input type=\"password\" name=\"ED_password2\" value=\"".(isset($ED_password2) ? $ED_password2 : '')."\" size=\"20\" maxlength=\"200\"></td>
		</tr>

		<tr>
			<td colspan=\"2\" align=\"center\">
			$htmlLDAPType
			<input type=\"submit\" value=\"$I18N_add\" name=\"BUT_add\">
			<input type=\"submit\" value=\"$I18N_saveChanges\" name=\"BUT_change\">
			</td>
		</tr>

		<tr>
			<td colspan=\"2\"><hr></td>
		</tr>

		<tr>
			<td colspan=\"2\" align=\"center\">
			<a href=\"/m23admin/phpldapadmin/\" target=\"_blank\"><img src=\"/gfx/phpLdapAdmin.png\"><br>$I18N_startphpLDAPadmin</a>
			</td>
		</tr>

	");
	HTML_showTableEnd();

	echo("</form>\n");
};
//ldap_delete(\$ds,\"uid=\$account,ou=people,dc=nodomain\");





/**
**name LDAP_I18NLdapType($status)
**description Returns the human readable description of the LDAP usage type 
**parameter status: status string
**/
function LDAP_I18NLdapType($status)
{
	include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

	switch ($status)
	{
		case "none": return($I18N_dontUseLDAP);
		case "read": return($I18N_readLoginFromLDAP);
		case "write": return($I18N_addNewLoginToLDAP);
		default: return($I18N_dontUseLDAP);
	};
};





/**
**name LDAP_matchLDAPserver($host,$base)
**description Searches for the name of a LDAP server and returns the name of the found server or false
**parameter host: the IP or hostname of the LDAP server
**parameter base: the base DN (e.g. dc=m23, dc=de)
**/
function LDAP_matchLDAPserver($host,$base)
{
	$servers = LDAP_listServers();

	foreach($servers as $sever)
	{
		$data=LDAP_loadServer($sever);
		if ($data['host']==$host && $data['base']==$base)
			return($sever);
	}
	return(false);
}





/**
**name LDAP_getNextID($serverName, $attribute, $container, $filter, $serverMinimumSettingsVar, $amount = 1, $minimumID = NULL)
**description Returns an array with free (posix user or group) IDs or a single free ID from the LDAP database.
**parameter serverName: Name of the LDAP server
**parameter attribute: Name ID attribute (eg. uidnumber)
**parameter container: Container (subtree) in the DIT to search for the IDs. If empty the whole database will be searched.
**parameter filter: Filter for only handling entries that match.
**parameter serverMinimumSettingsVar: The name of the variable that holds the minimum allowed ID value.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free IDs in the given amount or a single free ID as int.
**/
function LDAP_getNextID($serverName, $attribute, $container, $filter, $serverMinimumSettingsVar, $amount = 1, $minimumID = NULL)
{
	$freeIDs = $ids = array();

	$ds = LDAP_connectServer($serverName, $serverInfo);

	// Overwrite the start ID, if parameter was not given?
	if (is_null($minimumID)) $minimumID = $serverInfo[$serverMinimumSettingsVar];
	
	// All free IDs with maximum allowed (g/u)ID for Linux: 60000
	for ($i = $minimumID; $i <= 60000; $i++)
		$freeIDs[$i] = $i;

	// Where to start the search in the DIT?
	$dn = "$container$serverInfo[base]";

	// Only give out the given attributes in result
	$justthese = array($attribute);
	
	// Start search
	$sr = ldap_search($ds, $dn, $filter, $justthese);
	
	// Get results as associative array
	$info = ldap_get_entries($ds, $sr);

	// Amount of found IDs in LDAP
	$idAmount = $info['count'];

	// Get the amount of results
	if ($idAmount > 0)
	{
		// Remove found IDs from the list of free IDs
		for ($i = 0; $i < $idAmount; $i++)
			if (isset($info[$i][$attribute][0]))
				unset($freeIDs[$info[$i][$attribute][0]]);
	}

	// Multiple IDs or single ID?
	if ($amount > 1)
		// Give out the free IDs in the needed amount (counting starts from 0 and NOT from the key that is $minimumID)
		return(array_slice($freeIDs, 0, $amount));
	else
		// Return the first ID from the array (key won't be 0, so reset gives the first element)
		return(reset($freeIDs));
}





/**
**name LDAP_getFreeUserIDs($serverName, $amount = 1, $minimumID = NULL)
**description Returns an array with free posix group IDs or a single free ID from the LDAP database.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free user IDs in the given amount or a single free user ID as int.
**/
function LDAP_getFreeUserIDs($serverName, $amount = 1, $minimumID = NULL)
{
	return(LDAP_getNextID($serverName, 'uidnumber', '' /*'ou=people,'*/, '(uid=*)', 'uidMin', $amount, $minimumID));
}





/**
**name LDAP_getFreeGroupIDs($serverName, $amount = 1, $minimumID = NULL)
**description Returns an array with free posix group IDs or a single free ID from the LDAP database.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free group IDs in the given amount or a single free group ID as int.
**/
function LDAP_getFreeGroupIDs($serverName, $amount = 1, $minimumID = NULL)
{
	return(LDAP_getNextID($serverName, 'gidnumber', '' /*'ou=Group,'*/, '(cn=*)', 'gidMin', $amount, $minimumID));
}
?>