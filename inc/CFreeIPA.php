<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for FreeIPA access.
$*/

class CFreeIPA extends CChecks
{
	const FREEIPA_CONFIG_CACHE_FILE = '/m23/var/freeIPAConfigCache.php',
		LDAP_TRIM = " \n\r\t\v\0,",
		M23ADMIN = 'm23admin';

	protected
		$defaultDomain = NULL,
		$KRB_realm = NULL,
		$freeIPAServerFQDN = NULL,
		$freeIPAServerIP = NULL,
		$password = NULL,
		$curlHandle = NULL,
		$curlCookieFile = NULL;





/**
**name CFreeIPA::__construct($xmlFile, $argv)
**description Constructor for new CFreeIPA objects.
**parameter serverName: Name of the LDAP server
**/
	public function __construct()
	{
		if (!$this->loadFreeIPAConfigFile())
			throw new Exception($this->popErrorMessagesHTML());

		$this->curlInit();
		$this->curlLogin();
	}





/**
**name CFreeIPA::safeJSONString($in)
**description Makes the input string safe for using it in a JSON query.
**parameter in: Input string.
**returns Safe made string.
**/
	public static function safeJSONString($in)
	{
		return(I18N_safeUTF8_encode(str_replace ('"', '\"', $in)));
	}





/**
**name CFreeIPA::curlInit()
**description Prepares curl to connect to the FreeIPA server.
**/
	private function curlInit()
	{
		$this->curlHandle = curl_init();
		$this->curlCookieFile = uniqid('/m23/tmp/curlCookieFile-');
	}





/**
**name CFreeIPA::curlClose()
**description Closes the connection to the FreeIPA server.
**/
	private function curlClose()
	{
		@unlink($this->curlCookieFile);
		curl_close($this->curlHandle);
	}





/**
**name CFreeIPA::curlExec($post, $json = true)
**description Executes a command on the FreeIPA server via the JSONRPC API.
**parameter post: Data (urlencoded or JSON) to send via POST to the FreeIPA server.
**parameter json: Set to true, if the post data is in JSON format.
**returns Associative array, if JSON is active, otherwise direct output of curl.
**/
	private function curlExec($post, $json = true)
	{
		$ipaServer = $this->getServerFQDN();
	
		// Get select the connection parameter by the POST data format
		if ($json)
		{
			$headers[] = 'Content-Type:application/json';
			$headers[] = 'Accept:applicaton/json';
			$url = "https://$ipaServer/ipa/session/json";
		}
		else
		{
			$headers[] = 'Content-Type:application/x-www-form-urlencoded';
			$headers[] = 'Accept:text/plain';
			$url = "https://$ipaServer/ipa/session/login_password";
		}

		// Open the API URL on the FreeIPA server
		curl_setopt($this->curlHandle, CURLOPT_URL, $url);

		// Set the referer. Needed for security
		curl_setopt($this->curlHandle, CURLOPT_REFERER, "https://$ipaServer/ipa/");

		// Activate to get get the result (JSON or direct output of curl) from the webserver
		curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
	
		// Set send and receive data format in the header
		curl_setopt($this->curlHandle, CURLOPT_HTTPHEADER, $headers);

		// Needs to be set for using the connection after login for more commands, even if the cookie file isn't written
		curl_setopt($this->curlHandle, CURLOPT_COOKIEFILE, $this->curlCookieFile);
		curl_setopt($this->curlHandle, CURLOPT_COOKIEJAR, $this->curlCookieFile);
	
		// Don't check FreeIPA's SSL certificate
		curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYPEER, false);
	
		// Add post data
		curl_setopt($this->curlHandle, CURLOPT_POST, true);
		curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $post);

		// Debug
//	 	curl_setopt($this->curlHandle, CURLOPT_HEADER, true);
//	 	curl_setopt($this->curlHandle, CURLOPT_VERBOSE, true);

		// Curl FreeIPA's JSONRPC and get the result
		$ret = curl_exec($this->curlHandle);

		if ($json)
			// Decode JSON data into an associative array
			return(json_decode($ret, true));
		else
			// Give back (HTML) data directly
			return($ret);
	}





/**
**name CFreeIPA::curlLogin()
**description Logins into the FreeIPA server via the JSONRPC API.
**returns true, if login was successfully, false otherwise.
**/
	private function curlLogin()
	{
		$username = CFreeIPA::M23ADMIN;
		$password = $this->getm23AdminPassword();
		$ret = $this->curlExec("user=$username&password=$password", false);
		
		return($ret == '');
	}





/**
**name CFreeIPA::loadFreeIPAConfigFile()
**description Loads the FreeIPA configuration from CFreeIPA::FREEIPA_CONFIG_CACHE_FILE.
**returns true, if the config file could be read, false otherwise.
**/
	private function loadFreeIPAConfigFile()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		
		$temp = array();
	
		// Check, if the config file is present
		if (file_exists(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE))
			$temp = (unserialize(file_get_contents(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE)));
		else
		{
			$this->addErrorMessage($I18N_freeIPAErrorNoConfigFileFound);
			return(false);
		}

		// Read the config file
		$temp = (unserialize(file_get_contents(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE)));

		// Get the needed variables and complain, if they are missing
		foreach (array('defaultDomain', 'KRB_realm', 'freeIPAServerFQDN', 'freeIPAServerIP', 'password') as $var)
		{
			if (!isset($temp[$var][1]))
				$this->addErrorMessage("$I18N_freeIPAErrorConfigVariableMissing: $var");
			else
				$this->$var = $temp[$var];
		}

		if ($this->hasErrors())
			return(false);
		else
			return(true);
	}





/**
**name CFreeIPA::getMainDomain()
**description Gets the main domain that the FreeIPA server manages.
**returns Domain that the FreeIPA server manages
**/
	public function getMainDomain()
	{
		return($this->defaultDomain);
	}





/**
**name CFreeIPA::range2Domain($range)
**description Converts the range name into domain format (eg. ADM_LAN => adm-lan)
**parameter range: Range name.
**returns Range name converted into domain format.
**/
	static public function range2Domain($range)
	{
		return(strtolower(str_replace('_', '-', $range)));
	}





/**
**name CFreeIPA::getRangeFromDomain($domain)
**description Gets the range name from a domain format (eg. adm-lan.s1.example.com => ADM_LAN)
**parameter domain: Domain or client name in FQDN format.
**returns Range name converted into domain format.
**/
	static public function getRangeFromDomain($domain)
	{
		// Depending on the type of the domain, the range part is at the 3th or 4th (domain containg ".schulen.") position.
		$wantedPos = strpos($domain, '.schulen.') != false ? 5 : 4;
	
		// adm-lan.s1.example.com. => adm-lan.s1.example.com
		$domain = rtrim($domain,'.');

		// adm-lan.s1.example.com => [0] => adm-lan, [1] => s1, [2] => example, [3] => com
		$parts = explode('.', $domain);
		
		// Assume the range is just before the last 3 or 4 parts of the domain
		$pos = count($parts) - $wantedPos;

		// eg. adm-lan => ADM_LAN
		return(strtoupper(str_replace('-', '_', $parts[$pos])));
	}





/**
**name CFreeIPA::getSubdomains($schoolShort)
**description Gets possible subdomains for a given school short name.
**parameter schoolShort: Short name of the school.
**returns Associative array, with all possible subdomains for a school.
**/
	public function getSubdomains($schoolShort)
	{
		$out = array();

		// Get the network ranges
		require_once('/m23/inc/schoolConfig.php');

		// Add the subdomain (without network range) of the school to the array
		$url = $this->range2Domain($schoolShort.'.'.$this->defaultDomain);
		$out[$url] = $url;

		// Add the subdomains without network ranges to the array
		foreach (explode(',', SCHOOL_NETWORK_RANGES) as $range)
		{
			$url = $this->range2Domain($range.'.'.$schoolShort.'.'.$this->defaultDomain);
			$out[$url] = $url;
		}

		// Make sure, the local school network range is the first in the array
		array_makeFirst($out, $this->range2Domain(CSchool::SCHOOL_RANGE_SORT_FIRST.'.'.$schoolShort.'.'.$this->defaultDomain));

		return($out);
	}





/**
**name CFreeIPA::getRealm()
**description Gets the Kerberos realm that the FreeIPA server manages.
**returns Kerberos realm that the FreeIPA server manages
**/
	public function getRealm()
	{
		return($this->KRB_realm);
	}





/**
**name CFreeIPA::getServerFQDN()
**description Gets the FQDN of the FreeIPA server.
**returns FQDN of the FreeIPA server
**/
	public function getServerFQDN()
	{
		return($this->freeIPAServerFQDN);
	}





/**
**name CFreeIPA::getm23AdminPassword()
**description Gets the password of the m23admin user on the FreeIPA server.
**returns Password of the m23admin user on the FreeIPA server.
**/
	public function getm23AdminPassword()
	{
		return($this->password);
	}





/**
**name CFreeIPA::getServerIP()
**description Gets the IP of the FreeIPA server.
**returns IP of the FreeIPA server
**/
	public function getServerIP()
	{
		return($this->freeIPAServerIP);
	}





/**
**name CFreeIPA::deleteConfigCacheFile()
**description Deletes the config file with cached information about the FreeIPA server.
** returns: true, if the config file could be deleted, false otherwise.
**/
	public function deleteConfigCacheFile()
	{
		// Remove the FreeIPA server entry form /etc/hosts
		SERVER_delEtcHosts($this->getServerFQDN());
		return(@unlink(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE));
	}





/**
**name CFreeIPA::generateConfigCacheFileFromFreeIPA($ipaServerIP)
**description Logs into the FreeIPA server via SSH and reads out needed information and stores them in the config file. This requires the user m23admin to be created in FreeIPA with admin right
** returns: true on success, false on errors when storing the config file, string with error message on SSH errors.
**/
	public static function generateConfigCacheFileFromFreeIPA($ipaServerIP)
	{
		$out = array();
		
		$cmds = 'export LC_ALL=\'C.UTF-8\'

# Read domain from IPA database
domain="$(ipa realmdomains-show | grep Domain | head -1 | tr -d \'[:blank:]\' | cut -d\':\' -f2)"

# Read domain from hostname/FQDN (FQDN hostname - domain)
domain2="$(hostname -f | sed "s/$(hostname -a)\.//")"

if [ "$domain" != "$domain2" ]
then
	echo "ERROR: Domain mismatch $domain vs. $domain2!"
	exit 1
fi


# Get Kerberos REALM
realm="$(grep default_realm /etc/krb5.conf | tr -d "[:blank:]" | cut -d"=" -f2)"

if [ -z "$realm" ]
then
	echo "ERROR: No Kerberos REALM given!"
	exit 2
fi

# Read FQDN of the server
fqdn="$(hostname -f)"

if [ -z "$fqdn" ]
then
	echo "ERROR: IPA server has no FQDN!"
	exit 3
fi

if [ "$(echo $fqdn | sed \'s/\./\n/g\' | wc -l)" -eq 0 ]
then
	echo "ERROR: $fqdn is no FQDN!"
	exit 4
fi

# Get IP of the server
ip=$(hostname -I | cut -d\' \' -f1)

if [ $(echo $ip | wc -l) -ne 1 ]
then
	echo "ERROR: $ip is no single IPv4 IP!"
	exit 5
fi

if [ $(echo $ip | sed \'s/\./\n/g\' | wc -l) -ne 4 ]
then
	echo "ERROR: $ip is no IPv4 IP!"
	exit 6
fi

# Show the gathered information
echo "defaultDomain:$domain
KRB_realm:$realm
freeIPAServerFQDN:$fqdn
freeIPAServerIP:$ip"';

		$ret = CLIENT_executeOnClientOrIPSimple($ipaServerIP, 'generateConfigCacheFileFromFreeIPA', $cmds, 'm23admin', '/var/tmp');


		// Check for apparent error messages
		if (strpos($ret, 'Permission denied') != false || strpos($ret, 'Kerberos credentials') != false || strpos($ret, 'ERROR:') != false)
			return($ret);


		// Run thru the lines
		foreach (explode("\n", $ret) as $line)
		{
			if ('' == $line) continue;
			$varVal = explode(':', $line);
			$out[$varVal[0]] = $varVal[1];
		}

		if (count($out) >= 4)
		{
			// Add the FQDN with its IP to /etc/hosts to make it resolvable
			SERVER_addEtcHosts($out['freeIPAServerFQDN'], $out['freeIPAServerIP']);
			 
			// Store the config file
			return(CFreeIPA::saveFreeIPAConfigFile($out));
		}
		else
			return(false);
	}





/**
**name CFreeIPA::saveFreeIPAConfigFile($append)
**description Writes/appends entries to the FreeIPA configuration file.
**parameter append: Array with entries to append.
**returns true, if the config file could be written, false otherwise.
**/
	public static function saveFreeIPAConfigFile($append)
	{
		$out = array();

		// Read an existing config file into an array
		if (file_exists(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE))
			$out = (unserialize(file_get_contents(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE)));

		// Append new entries
		$out = array_merge($out, $append);

		// Write all entries
		return(file_put_contents(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE, serialize($out)) !== false);
	}






/**
**name CFreeIPA::__destruct()
**description Destructor for a CFreeIPA object.
**/
	function __destruct()
	{
		$this->curlClose();
		// Show messages before destructing the object
		$this->showMessages();
	}





/**
**name CFreeIPA::getBash_checkRootAndKerberosTicket()
**description Generates BASH code, to check, if the code is run as root and if a valid Kerberos ticket is present.
**returns Generated BASH code
**/
	public static function getBash_checkRootAndKerberosTicket()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		return('
export LC_ALL=\'C.UTF-8\'

# '.$I18N_Run_as_root_to_make_chown_work_on_the_administrators_home_directory.'
if [ $(whoami) != \'root\' ]
then
	echo "'.$I18N_ERROR_Not_logged_in_as_root.'"
	exit 1
fi

# '.$I18N_If_there_is_no_ticket_for_admin_pull_one_to_make_ipa_user_add_etc_work.'
if ! klist -s
then
	kinit admin
fi
');
	}





/**
**name CFreeIPA::getBash_addAdminRole($login)
**description Generates BASH code, to assign the admin role to an existing user.
**parameter login: Login name
**returns Generated BASH code
**/
	public static function getBash_addAdminRole($login)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		return('
# '.$I18N_Assign_admin_role.'
ipa group-add-member --users="'.$login.'" admins
ipa group-add-member --users="'.$login.'" \'trust admins\'
');
	}





/**
**name CFreeIPA::getBash_getKerberosTicketOnLogin($login, $password)
**description Generates BASH code, to fetch a kerberos ticket on login automatically.
**parameter login: Login name
**returns Generated BASH code
**/
	public static function getBash_getKerberosTicketOnLogin($login, $password)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		return('
# '.$I18N_Get_home_directory_of_the_user.'
homeDir="$(ipa -n user-show "'.$login.'" --raw | grep \'homedirectory\' | sed \'s/.*: //\')"
keytab="$homeDir/ipa.keytab"
bashprofile="$homeDir/.bash_profile"

# '.$I18N_Create_home_directory_for_the_user.'
mkdir -p "$homeDir"
chmod 750 "$homeDir"

# '.$I18N_Read_and_save_keytab.'
rm -f "$keytab"
yes "'.$password.'" | ipa-getkeytab -P -p "'.$login.'" -k "$keytab"
chmod 640 "$keytab"

# '.$I18N_Get_kerberos_ticket_on_login.'
if [ ! -f $bashprofile ] || [ $(grep -c $keytab $bashprofile) -eq 0 ]
then
	echo "
export LC_ALL=\'C.UTF-8\'
if ! klist -s
then
	kinit -k -t \'$keytab\' \''.$login.'\'
fi
" >> $bashprofile

fi

# '.$I18N_Restart_SSS_and_SSH_to_make_UID_and_GID_available_in_FreeIPA.'
service sssd restart
service sshd restart

sleep 5

# '.$I18N_Read_UID_and_GID_from_FreeIPA.'
uid=$(ipa -n user-show "'.$login.'" --raw | grep \'uidnumber:\' | tr -d \'[:blank:]\' | cut -d\':\' -f2)
gid=$(ipa -n user-show "'.$login.'" --raw | grep \'gidnumber:\' | tr -d \'[:blank:]\' | cut -d\':\' -f2)

# '.$I18N_Set_UID_and_GID_on_the_home_directory_of_the_user.'
chown -R $uid.$gid "$homeDir"
');
	}





/**
**name CFreeIPA::getBash_createUser($login, $firstName, $lastName, $password, $shell = '/bin/bash', $sshPubKey = NULL)
**description Generates BASH code, to create a user in FreeIPA.
**parameter login: Login name
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter password: Password
**parameter shell: Shell to use when logged in
**parameter sshPubKey: Public SSH key to allow passwordless login
**returns Generated BASH code
**/
	public static function getBash_createUser($login, $firstName, $lastName, $password, $shell = '/bin/bash', $sshPubKey = NULL)
	{
		$date = '2050-01-01Z';
	
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		// Generate parameter for adding the SSH pubkey, if given
		$sshPubKeyParam = is_null($sshPubKey) ? '' : '--sshpubkey="'.$sshPubKey.'"';

		return('
# '.$I18N_Create_user_in_FreeIPA.'
yes "'.$password.'" | ipa user-add "'.$login.'" '.$sshPubKeyParam.' --first="'.$firstName.'" --last="'.$lastName.'" --shell="'.$shell.'"  --password

ipa user-mod "'.$login.'" --user-auth-type=password

');
	}





/**
**name CFreeIPA::getBash_deleteUser($login)
**description Generates BASH code, to delete a user from FreeIPA.
**parameter login: Login name
**returns Generated BASH code
**/
	public static function getBash_deleteUser($login)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		return('
		# Get home directory of the user
		homeDir="$(ipa -n user-show "'.$login.'" --raw | grep "homedirectory" | sed "s/.*: //")"

		# Delete the user, if it exists
		if [ "$homeDir" ]
		then
			# Delete the user account
			ipa user-del "'.$login.'" --no-preserve
			
			# Delete the home directory
			rm -r -f "$homeDir"
		fi
		');
	}





/**
**name CFreeIPA::getBash_setPasswordExpiration($login, $password, $date = '2050-01-01Z')
**description Generates BASH code, to set expiration date for the password (far into the future so that you are not prompted to change the password).
**parameter login: Login name
**parameter password: Password
**parameter date: Expiration date.
**returns Generated BASH code
**/
	public static function getBash_setPasswordExpiration($login, $password, $date = '2050-01-01Z')
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		return('
yes "'.$password.'" | ipa user-mod "'.$login.'" --password

# '.$I18N_Set_password_expiration_date_far_in_the_future.'
ipa user-mod "'.$login.'" --password-expiration="'.$date.'"
');
	}




/**
**name CFreeIPA::getFreeIPIntegrateCommands()
**description Generates commands to create an administrator account, that can be used by m23, on the FreeIPA server.
**returns BASH commands to create an administrator account, that can be used by m23, on the FreeIPA server.
**/
	public static function getFreeIPIntegrateCommands()
	{

		$login = CFreeIPA::M23ADMIN;
		$firstName = 'm23';
		$lastName = 'Admin';
		$password = HELPER_generateSalt(64);
		$shell = '/bin/bash';
		$sshPubKey = SERVER_getPublicSSHKey();

		CFreeIPA::saveFreeIPAConfigFile(array('password' => $password));

		return(
			CFreeIPA::getBash_checkRootAndKerberosTicket().
			CFreeIPA::getBash_deleteUser($login).
			CFreeIPA::getBash_createUser($login, $firstName, $lastName, $password, $shell, $sshPubKey).
			CFreeIPA::getBash_addAdminRole($login).
			CFreeIPA::getBash_setPasswordExpiration($login, $password).
			CFreeIPA::getBash_getKerberosTicketOnLogin($login, $password)
			);
	}





/**
**name CFreeIPA::GUI_showConfigureServer()
**description Shows a dialog to integrate an existing FreeIPA server into m23.
**/
	public static function GUI_showConfigureServer()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
// 		HTML_logArea('LA_freeIPIntegrateCommands', 100, 25, CFreeIPA::getFreeIPIntegrateCommands());
		
		// Commands for integrating a FreeIPA server into m23
		$freeIPAWgetCommands = "wget http://".getServerIP()."/getFreeIPIntegrateCommands.php?lang=$GLOBALS[m23_language] -O /tmp/FreeIPIntegrate.sh\nbash /tmp/FreeIPIntegrate.sh";
		HTML_logArea('LA_freeIPAWgetCommands', 100, 2, $freeIPAWgetCommands);

		// IP of the FreeIPA server
		$ip = HTML_input('ED_ip', false, 17, 17);

		// Button to connect to the FreeIPA server and store settings
		if (HTML_submit("BUT_checkConnectionAndSaveSettingsOnSuccess", $I18N_checkConnectionAndSaveSettingsOnSuccess))
		{
			if (checkIP($ip))
			{
				$ret = CFreeIPA::generateConfigCacheFileFromFreeIPA($ip);
				
				if ($ret === true)
					MSG_showInfo($I18N_connectionTestWithIPAServerWasSuccessfully);
				elseif ($ret === false)
					MSG_showError($I18N_errorWhileTryingToConnectToTheFreeIPAServer);
				else
					MSG_showError($ret);
			}
			else
				MSG_showError($I18N_invalid_ip);
		}

		// Button to disconnect from the FreeIPA server
		if (HTML_submit("BUT_disconnectfromFreeIPAserver", $I18N_disconnectfromFreeIPAserver))
		{
			$CFreeIPAO = CFreeIPA::createCFreeIPAObject($createErrorMsg);
			if ($CFreeIPAO === false)
				MSG_showError($I18N_configFileForConnectingTheFreeIPAserverCouldNotBeDeleted);
			else
			{
				if ($CFreeIPAO->deleteConfigCacheFile())
					MSG_showInfo($I18N_configFileForConnectingTheFreeIPAserverWasDeleted);
				else
					MSG_showError($I18N_configFileForConnectingTheFreeIPAserverCouldNotBeDeleted);
			}
		}

		// Try to create a CFreeIPA object. This only works if there is a configuration file on the m23 server
		$CFreeIPAO = CFreeIPA::createCFreeIPAObject($createErrorMsg);
		if ($CFreeIPAO === false)
			MSG_showWarning($createErrorMsg);

		// FreeIPA server was integrated (config file present)
		if ($CFreeIPAO !== false)
		{
			HTML_showTitle($I18N_settings);
			HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
			HTML_showTableRow($I18N_FreeIPAServerHostname, $CFreeIPAO->getServerFQDN());
			HTML_showTableRow($I18N_FreeIPAServerIP, $CFreeIPAO->getServerIP());
			HTML_showTableRow($I18N_domain, $CFreeIPAO->getMainDomain());
			HTML_showTableRow($I18N_KerberosRealm, $CFreeIPAO->getRealm());
			HTML_showTableRow($I18N_status, $CFreeIPAO->isOnline() ? $I18N_computerOn : $I18N_computerOff);
			HTML_showTableRow(array('td' => 'colspan="2"'), '<center>'.BUT_disconnectfromFreeIPAserver.'</center>');
			
			HTML_showTableEnd(true);
		}
		// FreeIPA server was NOT integrated (no config file present)
		else
		{
			HTML_showTitle($I18N_connectWithFreeIPAServer);
			HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
			echo("
			<table align=\"center\">
			<tr>
				<td><div class=\"subtable_shadow\">
					<table align=\"center\" class=\"subtable2\">
						<tr>
							<td class=\"numberBorder\">1.</td>
							<td>
								".LA_freeIPAWgetCommands."
							<td> $I18N_help_freeIPAIntegrateCommands</td>
						</tr>
						<tr>
							<td class=\"numberBorder\">2.</td>
							<td>
								$I18N_ipAddressOfFreeIPAServer: ".ED_ip."</br></br>
								".BUT_checkConnectionAndSaveSettingsOnSuccess."
							</td>
							<td>$I18N_help_checkConnectionToFreeIPA</td>
						</tr>
					</table>
				</div></td>
			<tr></table>
			");
		}

	}





/**
**name CFreeIPA::isAvailable()
**description Checks, if a FreeIPA server is integrated into m23, by checking the presence ofthe configuration file on the m23 server. A presence doesn't mean, that the FreeIPA server is currently running.
**returns true, if the config file is present, otherwise false.
**/
	public static function isAvailable()
	{
		return(file_exists(CFreeIPA::FREEIPA_CONFIG_CACHE_FILE));
	}





/**
**name CFreeIPA::createCFreeIPAObject(&$errorMsg)
**description Tries to create a CFreeIPA object. This only works if there is a configuration file on the m23 server.
**parameter errorMsg: In case of an error the error message is written into this variable.
**returns CFreeIPA object on success, otherwise false.
**/
	public static function createCFreeIPAObject(&$errorMsg)
	{
		try
		{
			$CFreeIPAO = new CFreeIPA();
		}
		catch (Exception $e)
		{
			$errorMsg = $e->getMessage();
		}

		return(isset($CFreeIPAO) && is_object($CFreeIPAO) ? $CFreeIPAO : false);
	}





/**
**name CFreeIPA::executeOnFreeIPA($jobName, $cmds)
**description Runs a BASH script on the FreeIPA server.
**parameter jobName: name of the job screen should show
**parameter cmds: the commands of the script
**returns Output of the script.
**/
	private function executeOnFreeIPA($jobName, $cmds)
	{
		return(CLIENT_executeOnClientOrIPSimple($this->getServerIP(), $jobName, $cmds, 'm23admin', '/var/tmp'));
	}





/**
**name CFreeIPA::isOnline()
**description Checks, if the FreeIPA server, that was integrated into m23, is currently running.
**returns true, if the FreeIPA server is running, false otherwise.
**/
	private function isOnline()
	{
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_find",
			"params": [
				[
					"'.CFreeIPA::M23ADMIN.'"
				],
				{
				}
			]
		}');
		return(strpos($ret['result']['summary'], '1 user matched') === 0);
// 		return(strpos($this->executeOnFreeIPA('isOnline', 'ipa user-find "'.CFreeIPA::M23ADMIN.'"'),'m23admin') !== false);
	}





/**
**name CFreeIPA::getUsedFreeIPAIPs($clientFQDN, $intIPs = false)
**description Gets the IPs that are managed in a DNS zone by the FreeIPA server.
**parameter clientFQDN: FQDN of the client to extract the DNS zone name from.
**parameter intIPs: Set to true, to return the IPs as int values.
**returns IPs that are managed by the FreeIPA server.
**/
	public function getUsedFreeIPAIPs($clientFQDN, $intIPs = false)
	{
		HELPER_splitFQDNClientName($clientFQDN, $hostname, $domain);
	
		$out = array();

		// TODO: Does it work with multiple domains?

		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "dnsrecord_find",
			"params": [
				[
					"'.$domain.'"
				],
				{
					"sizelimit": 0
				}
			]
		}');

		// Give out an empty array, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(array());

		// Run thru the results
		foreach ($ret['result']['result'] as $entry)
		{
			if (isset($entry['arecord'][0]))
			{
				if ($intIPs)
					$out[$entry['arecord'][0]] = ip2longSafe($entry['arecord'][0]);
				else
					$out[$entry['arecord'][0]] = $entry['arecord'][0];
			}
		}

		return($out);
	}





/**
**name CFreeIPA::checkCurlResult($ret)
**description Checks the answer of a FreeIPA curl call.
**parameter ret: Answer of a FreeIPA curl call.
**returns true, if there are no errors in the query, otherwise false.
**/
	protected function checkCurlResult($ret, $ignoreError = 'EiJ5Toowephei2vohXovojo')
	{
		if (isset($ret['error']['message'][1]))
		{
			if (strpos($ret['error']['message'], $ignoreError)!==false)
				return(true);
		
			$this->addErrorMessage($ret['error']['message']);
			return(false);
		}

		if (isset($ret['error']) && is_array($ret['error']))
		{
			$error = print_r($ret['error'], true);

			if (strpos($error, $ignoreError)!==false)
				return(true);

			$this->addErrorMessage($error);
			return(false);
		}

		return(true);
	}





/**
**name CFreeIPA::curlResultHasErrorMessage($ret, $error)
**description Checks the given error message is included in the result of a FreeIPA curl call.
**parameter ret: Answer of a FreeIPA curl call.
**parameter error: Error message to find.
**returns true, if the error message is found, otherwise false.
**/
	private function curlResultHasErrorMessage($ret, $error)
	{
		if (isset($ret['error']['message'][1]))
		{
			if (strpos($ret['error']['message'], $error)!==false)
				return(true);

			return(false);
		}

		if (isset($ret['error']) && is_array($ret['error']))
		{
			$error = print_r($ret['error'], true);

			if (strpos($error, $error)!==false)
				return(true);

			return(false);
		}

		return(false);
	}





/**
**name CFreeIPA::getReverseZone($ip, &$d)
**description Calcultes the reverse DNS zone for a given IP address.
**parameter ip: IP address
**parameter d: Variable to that the last part of the IP address is written to.
**returns reverse DNS zone.
**/
	public static function getReverseZone($ip, &$d)
	{
		// eg. 192.168.1.2
		$ipParts = explode('.', $ip);
		$a = $ipParts[0];	// 192
		$b = $ipParts[1];	// 168
		$c = $ipParts[2];	// 1
		$d = $ipParts[3];	// 2
	
		// eg. 1.168.192.in-addr.arpa.
		return("$c.$b.$a.in-addr.arpa.");
	}





/**
**name CFreeIPA::addDNSEntry($client, $clientIP, $addReverse = true)
**description Adds an DNS entry for a client in FreeIPA.
**parameter client: Name of the client (FQDN).
**parameter clientIP: IP of the client.
**parameter addReverse: Set to true, if a reverse DNS entry should be created.
**returns true, if there are no errors in the query, otherwise false.
**/
	public function addDNSEntry($client, $clientIP, $addReverse = true)
	{
		// Get the domain name
		HELPER_splitFQDNClientName($client, $hostname, $domain);

		if ($addReverse)
		{
			$reverseIPZone = CFreeIPA::getReverseZone($clientIP, $d);
	
			// Add reverse DNS record
			$q = '{
				"id": 0,
				"method": "dnsrecord_add",
				"params": [
					[
						"'.$reverseIPZone.'",
						{
							"__dns_name__": "'.$d.'"
						}
					],
					{
						"ptrrecord": [
							"'.$client.'"
						]
					}
				]
			}';
			$this->curlExec($q);
		}


		// Add DNS record
		$q = '{
			"id": 0,
			"method": "dnsrecord_add",
			"params": [
				[
					 "'.$domain.'",
					{
						"__dns_name__": "'.$hostname.'"
					}
				],
				{
					"a_part_ip_address": "'.$clientIP.'"
				}
			]
		}';


		$ret = $this->curlExec($q);
		return($this->checkCurlResult($ret, 'no modifications to be performed'));
	}





/**
**name CFreeIPA::addComputer($client, $clientIP, $mac, $description = '', $class = '', $locality = '', $location = '', $platform = '', $os = '')
**description Adds an m23 client to FreeIPA as host and into FreeIPA's DNS.
**parameter client: Name of the computer/client to add.
**parameter clientIP: The IP of the client.
**parameter mac: MAC address of the client.
**parameter description: Description to store in FreeIPA.
**parameter class: Host category
**parameter locality: Locality where the computer is placed.
**parameter location: Location where the computer is placed.
**parameter platform: Platform (e.g. CPU, computer model, VM, etc.)
**parameter os: The distribution, OS that the computer runs.
**returns true, if the client was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function addComputer($client, $clientIP, $mac, $description = '', $class = '', $locality = '', $location = '', $platform = '', $os = '')
	{
		// Is set to true until the host entry was made
		$hostAddRepeat = true;

		// Counter for the amount of failed tries
		$hostAddTries = 0;

		// Convert MAC address to proper format
		$mac = strtoupper(CLIENT_convertMac($mac, ':'));

		// Make sure there is a DNS entry for the client
		$ret = $this->addDNSEntry($client, $clientIP);

		// Return error message in case of an error
		if ($ret !== true) return($ret);

		/*
			* After adding a DNS entry it will NOT be available at once.
			* A smal amount of time is needed until it becomes valid.
			* "ipa host_add" needs a valid DNS entry and will fail, if it's called too early after adding the according DNS entry.
			* "ipa host_add" will be called multiple times with a smal waiting time between calls, until it is successfully.
		*/

		do
		{
			// Wait 1 second
			sleep(1);

			// Query for adding the host entry
			$q = '
				{
					"id": 0,
					"method": "host_add",
					"params": [
						[
							"'.$client.'"
						],
						{
							"description": "'.CFreeIPA::safeJSONString($description).'",
							"l": "'.CFreeIPA::safeJSONString($locality).'",
							"nshardwareplatform": "'.CFreeIPA::safeJSONString($platform).'",
							"nshostlocation": "'.CFreeIPA::safeJSONString($location).'",
							"macaddress": "'.$mac.'",
							"userclass": "'.CFreeIPA::safeJSONString($class).'",
							"nsosversion": "'.CFreeIPA::safeJSONString($os).'"
						}
					]
				}';
	
			// Try to add host
			$ret = $this->curlExec($q);

			// Check, if there is the error message that complains about missing DNS entry
			$hostAddRepeat = $this->curlResultHasErrorMessage($ret, 'does not have corresponding');
			
			$hostAddTries++;
		}
		while ($hostAddRepeat && $hostAddTries < 30);


		$ret = $this->checkCurlResult($ret);
		if ($ret !== true)
		{
			// Remove DNS entry in case of an error while adding the host
			$this->delComputer($client);
			// Return error message in case of an error
			return($ret);
		}
		else
			return(true);
	}





/**
**name CFreeIPA::setComputerBulkPassword($client, $bulkPasswd)
**description Sets the bulk password of an m23 client in FreeIPA.
**parameter client: Name of the computer/client to add.
**parameter bulkPasswd: Bulk password for running ipa-client-install on the client.
**returns true, if the bulk password was set successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function setComputerBulkPassword($client, $bulkPasswd)
	{
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "host_disable",
			"params": [
				[
					"'.$client.'"
				],
				{
				}
			]
		}
		');

		// Return error message in case of an error
		$ret = $this->checkCurlResult($ret,'already disabled');
		if ($ret !== true) return($ret);

		// After disabling the host, the A record needs to be added again (but not the reverse entry).
		$clientIP = CLIENT_getIPbyName($client);
		$this->addDNSEntry($client, $clientIP, false);

		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "host_mod",
			"params": [
				[
					"'.$client.'"
				],
				{
					"userpassword": "'.$bulkPasswd.'"
				}
			]
		}');
		
		// Return error message in case of an error
		return($this->checkCurlResult($ret, 'enrolled Xhost'));
	}





/**
**name CFreeIPA::addUser($login, $firstName, $lastName, $password, $shell = '/bin/bash', $sshKey = '', $additionParams = '')
**description Creates a user in FreeIPA.
**parameter login: Login name
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter password: Password
**parameter shell: Shell to use when logged in
**parameter sshPubKey: Public SSH key to allow passwordless login
**parameter additionParams: Additional parameters in line and comma separated key value pairs and in JSON notation.
**returns true, if the user was added successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function addUser($login, $firstName, $lastName, $password, $shell = '/bin/bash', $sshKey = '', $additionParams = '')
	{
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_add",
			"params": [
				[
					"'.$login.'"
				],
				{
					"givenname": "'.CFreeIPA::safeJSONString($firstName).'",
					"ipasshpubkey": [
						"'.$sshKey.'"
					],
					"ipauserauthtype": [
						"password"
					],
					"krbpasswordexpiration": {
						"__datetime__": "20500101000000Z"
					},
					'.$additionParams.'"loginshell": "'.$shell.'",
					"sn": "'.CFreeIPA::safeJSONString($lastName).'",
					"userpassword": "'.CFreeIPA::safeJSONString($password).'"
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::changeUser($login, $firstName, $lastName, $password = '', $shell = '/bin/bash', $sshKey = '', $additionParams = '')
**description Changes a user in FreeIPA.
**parameter login: Login name (cannot be changed as it is used for identifying the user)
**parameter firstName: Forename
**parameter lastName: Familyname
**parameter password: Password
**parameter shell: Shell to use when logged in
**parameter sshPubKey: Public SSH key to allow passwordless login
**parameter additionParams: Additional parameters in line and comma separated key value pairs and in JSON notation.
**returns true, if the user was changed successfully, otherwise (error) messages from the FreeIPA API.
**/
	public function changeUser($login, $firstName, $lastName, $password = '', $shell = '/bin/bash', $sshKey = '', $additionParams = '')
	{
		if (isset($password[0]))
			$passwdParam = ',
					"userpassword": "'.CFreeIPA::safeJSONString($password).'"';
		else
			$passwdParam = '';

		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_mod",
			"params": [
				[
					"'.$login.'"
				],
				{
					"givenname": "'.CFreeIPA::safeJSONString($firstName).'",
					"ipasshpubkey": [
						"'.$sshKey.'"
					],
					'.$additionParams.'"loginshell": "'.$shell.'",
					"sn": "'.CFreeIPA::safeJSONString($lastName).'"'.$passwdParam.'
				}
			]
		}');
		
		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::delUser($login)
**description Deletes a user from FreeIPA.
**parameter login: Login name
**returns true, if the user was deleted successfully, otherwise (error) messages from the ipa tool.
**/
	public function delUser($login)
	{
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_del",
			"params": [
				[
					[
						"'.$login.'"
					]
				],
				{
					"preserve": false
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::delComputer($client)
**description Deletes an m23 client form FreeIPA's host and DNS entries.
**parameter client: Name of the computer/client to add.
**returns true, if the client was deleted successfully, otherwise (error) messages from the ipa tool.
**/
	public function delComputer($client)
	{
		$allOK = true;
	
		// Get the domain name
		HELPER_splitFQDNClientName($client, $hostname, $domain);

		// Remove DNS entry
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "dnsrecord_del",
			"params": [
				[
					"'.$domain.'",
					{
						"__dns_name__": "'.$hostname.'"
					}
				],
				{
					"del_all": true
				}
			]
		}');

		// Return error message in case of an error
		$allOK &= $this->checkCurlResult($ret);

/*
		Maybe not needed, because the reverse is deleted by dnsrecord_del with parameter: "del_all": true

		// Delete reverse DNS record
		$reverseIPZone = CFreeIPA::getReverseZone(CLIENT_getIPbyName($client), $d);
		$q = '{
			"id": 0,
			"method": "dnsrecord_del",
			"params": [
				[
					"'.$reverseIPZone.'",
					{
						"__dns_name__": "'.$d.'"
					}
				],
				{
					"del_all": true
				}
			]
		}';
		$ret = $this->curlExec($q);
*/
// 		$allOK &= $this->checkCurlResult($ret);

		// Remove host entry
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "host_del",
			"params": [
				[
					[
						"'.$client.'"
					]
				],
				{
				}
			]
		}');
		$allOK &= $this->checkCurlResult($ret);
		
		$allOK = (bool)$allOK;
		return($allOK);
	}





/**
**name CFreeIPA::existUserGroup($group)
**description Checks, if a user group is existing in FreeIPA.
**parameter group: Name of the group to check.
**returns true, if the group exists, otherwise false.
**/
	public function existUserGroup($group)
	{
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "group_show",
			"params": [
				[
					"'.$group.'"
				],
				{
				}
			]
		}
		');

		// Group names are case insensitive
		return(isset($ret['result']['value']) && (strtoupper($ret['result']['value']) === strtoupper($group)));
	}





/**
**name CFreeIPA::createUserGroup($group, $description)
**description Creates a user group in FreeIPA.
**parameter group: Name of the group
**parameter description: description of the group
**returns true, if the group was created, otherwise false.
**/
	public function createUserGroup($group, $description)
	{
		// Don't create a group that already exists
		if ($this->existUserGroup($group)) return(true);

		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "group_add",
			"params": [
				[
					"'.$group.'"
				],
				{
					"description": "'.CFreeIPA::safeJSONString($description).'"
				}
			]
		}
		');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::addLoginToGroup($login, $group)
**description Adds an user to a group in FreeIPA.
**parameter group: Name of the group
**parameter login: Login name
**returns true, if the login was added or is in the group already, otherwise false.
**/
	public function addLoginToGroup($login, $group)
	{
	
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "group_add_member",
			"params": [
				[
					"'.$group.'"
				],
				{
					"user": [
						"'.$login.'"
					]
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
}





/**
**name CFreeIPA::addGroupToUserGroup($group, $mainGroup)
**description Adds a group to another group in FreeIPA.
**parameter group: Name of the group to add to another group
**parameter mainGroup: Name of the main group where the another group should be added
**returns true, if the group was added or is in the group already, otherwise false.
**/
	public function addGroupToUserGroup($group, $mainGroup)
	{
	
// 	print("<h3>addGroupToUserGroup($group, $mainGroup)</h3>");
	
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "group_add_member",
			"params": [
				[
					"'.$mainGroup.'"
				],
				{
					"group": [
						"'.$group.'"
					]
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::createHostGroup($group, $description)
**description Creates a host (m23 client) group in FreeIPA.
**parameter group: Name of the group
**parameter description: description of the group
**returns true, if the group was created, otherwise false.
**/
	public function createHostGroup($group, $description)
	{
// print("<h4>createHostGroup($group, $description)</h4>");
		// Don't create a group that already exists
		if ($this->existHostGroup($group)) return(true);
// print("<h4>createHostGroup($group, $description): weiter</h4>");

		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "hostgroup_add",
			"params": [
				[
					"'.$group.'"
				],
				{
					"description": "'.CFreeIPA::safeJSONString($description).'"
				}
			]
		}
		');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::existHostGroup($group)
**description Checks, if a host group is existing in FreeIPA.
**parameter group: Name of the group to check.
**returns true, if the group exists, otherwise false.
**/
	public function existHostGroup($group)
	{
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "hostgroup_show",
			"params": [
				[
					"'.$group.'"
				],
				{
				}
			]
		}
		');

		// Group names are case insensitive
		return(isset($ret['result']['value']) && (strtoupper($ret['result']['value']) === strtoupper($group)));
	}





/**
**name CFreeIPA::addClientToHostGroup($client, $group)
**description Adds an m23 client to a host group in FreeIPA.
**parameter group: Name of the group
**returns true, if the client was added or is in the group already, otherwise false.
**/
	public function addClientToHostGroup($client, $group)
	{
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "hostgroup_add_member",
			"params": [
				[
					"'.$group.'"
				],
				{
					"host": [
						"'.$client.'"
					]
				}
			]
		}
		');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::addGroupToHostGroup($group, $mainGroup)
**description Adds a host group to another host group in FreeIPA.
**parameter group: Name of the host group to add to another host group
**parameter mainGroup: Name of the main host group where the another host group should be added
**returns true, if the host group was added or is in the host group already, otherwise false.
**/
	public function addGroupToHostGroup($group, $mainGroup)
	{
	
// 	print("<h3>addGroupToUserGroup($group, $mainGroup)</h3>");
	
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "hostgroup_add_member",
			"params": [
				[
					"'.$group.'"
				],
				{
					"hostgroup": [
						"'.$mainGroup.'"
					]
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::getDNSZones()
**description Gets the list of DNS zones managed by the FreeIPA server.
**returns Associative array with the DN as key and DNS zone as value.
**/
	public function getDNSZones()
	{
		$out = array();

		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "dnszone_find",
			"params": [
				[],
				{
					"sizelimit": 0
				}
			]
		}');

		// Give out an empty array, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(array());

		// Run thru the results
		foreach ($ret['result']['result'] as $entry)
		{
			if (isset($entry['idnsname'][0][0]))
				$out[$entry['dn']] = $entry['idnsname'][0];
		}

		return($out);
	}





/**
**name CFreeIPA::getDNSZoneInfo($zoneName)
**description Gets information about the settings of DNS zone.
**parameter zoneName: Name of the zone.
**returns Associative array with the name of the setting as key and its value as value.
**/
	public function getDNSZoneInfo($zoneName)
	{
		$out = array();

		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "dnsrecord_find",
			"params": [
				[
					"'.$zoneName.'"
				],
				{
					"sizelimit": 0
				}
			]
		}
		');

		// Give out an empty array, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(array());

		// Run thru the results
		foreach ($ret['result']['result'] as $entry)
		{
			if (isset($entry['dn']))
				$out['dn'] = $entry['dn'];
		
			if (isset($entry['idnsname'][0]))
			{
				$recordName = false;
				
				// Get the key that contains "record" because there are many types of records (eg. txtrecord or nsrecord)
				foreach ($entry as $eK => $eV)
				{
					if (strpos($eK, 'record') !== false)
					{
						$recordName = $eK;
						break;
					}
				}

				// Check, if a record name was found
				if ($recordName === false) continue;
			
// 				print("<h4>$recordName</h4>");
// 				print("<h4>".$entry[$recordName][0]."</h4>");

				// Add the setting and its value to the output array
				$out[$entry['idnsname'][0]] = $entry[$recordName][0];
			}
		}

		return($out);
	}





/**
**name CFreeIPA::updateSchoolInfoFileFromDNSZones()
**description (Over)Writes the school info file with settings from the DNS zones.
**returns true, on sucessfully writing, false otherwise.
**/
	public static function updateSchoolInfoFileFromDNSZones()
	{
		// Get the network ranges
		require_once('/m23/inc/schoolConfig.php');
		$ranges = explode(',', SCHOOL_NETWORK_RANGES);

		$CFreeIPAO = new CFreeIPA();

		// Array to store the settings from DNS
		$schoolInfo = array();

		// Run thru the DNS zones
		foreach ($CFreeIPAO->getDNSZones() as $dn => $dnsZone)
		{
			// Remove trailing dot
			$dnsZone = rtrim($dnsZone,'.');
		
			// Skip reverse DNS entries
			if (strpos($dnsZone, 'in-addr.arpa')!==false) continue;

			// Get all information about the current DNS zone
			$zI = $CFreeIPAO->getDNSZoneInfo($dnsZone);

			// Skipt DNS entries that don't contain school information
			if (!isset($zI['schoolshort'])) continue;

			// Get the school short name
			$schoolshort = $zI['schoolshort'];

			// Split the DNS zone (URL) into parts
			$URLParts = explode(".", $dnsZone);
			$URLPartAmount = count($URLParts);

// print("<h3>$dnsZone</h3>");
// print_r2($URLPartAmount);

			// If there are 4 parts (eg. sch1.schools.district.xyz)
			if ($URLPartAmount == 4)
			{
				// Create initial entry with basic parameters in the array
				$schoolInfo[$schoolshort] = array
				(
					'description' => $zI['schoolname'],
				);
			}
			// If there are 5 parts (eg. admin-net.sch1.schools.district.xyz)
			elseif ($URLPartAmount == 5)
			{
				// Get the network range from the URL
				$range = $URLParts[0];

				// Convert range from eg. admin-net => ADMIN_NET
				$range = strtoupper(str_replace('-', '_', $range));

				// Add the information
				$schoolInfo[$schoolshort][$range] = array
				(
					'min' => $zI["min"],
					'max' => $zI["max"],
					'netmask' => $zI["netmask"],
					'gateway' => $zI["gateway"],
					'dns1' => $zI["dns1"],
					'dns2' => $zI["dns2"]
				);
			}
		}
		
		$out = "<?php\n\$schoolInfo = ".var_export($schoolInfo, true).";\n?>";
// 		file_put_contents('/tmp/schoolInfo.php', $out);
// 		print_r2($schoolInfo);

		// Write the new school info file
		return(SERVER_putFileContents(CSchool::SCHOOL_INFO_FILE, $out, '644', HELPER_getApacheUser(), HELPER_getApacheGroup()));
	}





/**
**name CFreeIPA::findUserByEmployeenumber($en)
**description Find a user by the employee number and give back all user information.
**returns User information or empty array on error or if no user could be found.
**/
	public function findUserByEmployeenumber($en)
	{
		if (!isset($en[0])) return(false);
	
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_find",
			"params": [
				[
				],
				{
					"sizelimit": 0,
					"all": true,
					"employeenumber": "'.$en.'"
				}
			]
		}');

		// Return here, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(false);

		return($this->parseCurlResult($ret));
	}





/**
**name CFreeIPA::parseCurlResult($ret)
**description Parses the answer of a FreeIPA curl call into a more handy associative array.
**parameter ret: Answer of a FreeIPA curl call.
**returns Associative array with parsed answer of a FreeIPA curl call, or empty array on error.
**/
	protected function parseCurlResult($ret)
	{
		$out = array();

		// Get the base where the values are stored in the resulting array
		$base = isset($ret['result']['result'][0]) ? $ret['result']['result'][0] : $ret['result']['result'];
	
		foreach ($base as $key => $value)
		{
			if (is_array($value))
			{
				// Check, if there is anything to add
				if (isset($value[0]))
				{
					// Check, if there are multiple values
					if (isset($value[1]))
						// There are multiple values
						$out[$key] = $value;
					else
						// There is only one value
						$out[$key] = $value[0];
				}
			}
			else
				$out[$key] = $value;
		}

		return($out);
	}





/**
**name CFreeIPA::findLoginsStartingWith($startWith)
**description Find a login names that start with a given string.
**parameter startWith: String the login name should start with.
**returns Login names that start with the given string.
**/
	public function findLoginsStartingWith($startWith)
	{
		$out = array();
	
		// Search for all login names that contain $startWith at any position
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_find",
			"params": [
				[
					"'.$startWith.'"
				],
				{
					"sizelimit": 0
				}
			]
		}');

		// Return here, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(false);

		// Run thru the entries with user data
		foreach ($ret['result']['result'] as $userData)
		{
			// Check, if the login name is given
			if (isset($userData['uid'][0]))
			{
				$login = $userData['uid'][0];
				
				// Add the login name to the output array, if it starts with the search term
				if (strpos($login, $startWith) === 0)
					$out[] = $login;
			}
		}

		return($out);
	}





/**
**name CFreeIPA::createm23Subgroups($schoolName, $description)
**description Create client and user groups with the name of the school and different postfixes.
**parameter schoolName: Name of the school.
**parameter description: Base description of the school.
**/
	protected function createm23Subgroups($schoolName, $description)
	{
		// Groups for hosts (m23 clients)
			$this->createHostGroup($schoolName, "$description: All computers and devices");
			foreach ($this->getm23SubHostGroupsPostfixes() as $shortName => $groupDescription)
			{
				$groupName = "${schoolName}_$shortName";
				$groupDescription = "$description - $groupDescription";
	
				// Create a client group with the name of the school and the postfix
				if (!GRP_exists($groupName))
				{
// 					print("<h3>createm23Subgroups:GRP_add($groupName, $groupDescription)</h3>");
					GRP_add($groupName, $groupDescription, false);
				}

				$this->createHostGroup($groupName, $groupDescription);
				$this->addGroupToHostGroup($groupName, $schoolName);
			}

		// Groups for users
			// Create group for all users (teachers, pupils, schooladmin)
			$this->createUserGroup($schoolName, "$description - All users");

			// Create separate groups for all users (teachers, pupils, schooladmin) and hosts (m23 clients)
			$this->createUserGroup($this->getTeacherUserGroup($schoolName), "$description - Teacher");
			$this->createUserGroup($this->getPupilUserGroup($schoolName), "$description - Pupil");
			$this->createUserGroup($this->getSchooladminUserGroup($schoolName), "$description - Schooladmin");

			// Add the sub-groups to the main school group
			$this->addGroupToUserGroup($this->getTeacherUserGroup($schoolName), $schoolName);
			$this->addGroupToUserGroup($this->getPupilUserGroup($schoolName), $schoolName);
			$this->addGroupToUserGroup($this->getSchooladminUserGroup($schoolName), $schoolName);
	}





/**
**name CFreeIPA::getTeacherUserGroup($schoolName)
**description Get the name of the user group for teachers in a given school.
**parameter schoolName: Name of the school.
**parameter description: Name of the user group for teachers.
**/
	public function getTeacherUserGroup($schoolName)
	{
		return($schoolName.'-'.CSchool::SCHOOL_ROLE_TEACHER);
	}





/**
**name CFreeIPA::getPupilUserGroup($schoolName)
**description Get the name of the user group for pupils in a given school.
**parameter schoolName: Name of the school.
**parameter description: Name of the user group for pupils.
**/
	public function getPupilUserGroup($schoolName)
	{
		return($schoolName.'-'.CSchool::SCHOOL_ROLE_PUPIL);
	}





/**
**name CFreeIPA::getSchooladminUserGroup($schoolName)
**description Get the name of the user group for admins in a given school.
**parameter schoolName: Name of the school.
**parameter description: Name of the user group for admins.
**/
	public function getSchooladminUserGroup($schoolName)
	{
		return($schoolName.'-'.CSchool::SCHOOL_ROLE_SCHOOLADMIN);
	}





/**
**name CFreeIPA::createm23SchoolGroupsIfNeeded($schoolName, &$schoolInfo)
**description Creates client and user groups with the name of the school and different postfixes for a given school, if it was not marked as done from a previous function call.
**parameter schoolName: Short name of the school.
**parameter schoolInfo: Pointer to the associative array with information about all schools and flags to mark schools with finished group creation.
**/
	public function createm23SchoolGroupsIfNeeded($schoolName, &$schoolInfo)
	{
		// Check, if the school groups were NOT created before
		if (!isset($schoolInfo[$schoolName]['groupsCreated']))
		{
			// Create the groups
			$this->createm23Subgroups($schoolName, $schoolInfo[$schoolName]['description']);

			// Mark that the school has finished group creation
			$schoolInfo[$schoolName]['groupsCreated'] = true;
		}
	}





/**
**name CFreeIPA::getUserinfoByLogin($login)
**description Get all user information for a given login.
**returns User information or empty array on error or if no user could be found.
**/
	public function getUserinfoByLogin($login)
	{
		$ret = $this->curlExec(
		'{
			"id": 0,
			"method": "user_show",
			"params": [
				[
					"'.$login.'"
				],
				{
					"all": true
				}
			]
		}');

		// Return here, if there is no (valid) answer from the FreeIPA server
		if (!is_array($ret))
			return(false);

		return($this->parseCurlResult($ret));
	}





/**
**name CFreeIPA::delLoginFromGroup($login, $group)
**description Removes an user from a group in FreeIPA.
**parameter group: Name of the group
**parameter login: Login name
**returns true, if the login was removed or is in not the group, otherwise false.
**/
	public function delLoginFromGroup($login, $group)
	{
		$ret = $this->curlExec('
		{
			"id": 0,
			"method": "group_remove_member",
			"params": [
				[
					"'.$group.'"
				],
				{
					"user": [
						"'.$login.'"
					]
				}
			]
		}');

		// Return error message in case of an error
		return($this->checkCurlResult($ret));
	}





/**
**name CFreeIPA::removeLoginFromAllGroups($login, $omitGroups = array('ipausers'))
**description Removes an user from all groups in FreeIPA.
**parameter login: Login name
**parameter omitGroups: Array with groups to NOT remove the user from.
**returns true, if the login was removed from all groups successfully, otherwise false.
**/
	public function removeLoginFromAllGroups($login, $omitGroups = array('ipausers'))
	{
		$ret = true;
	
		// Get the user information
		$userInfo = $this->getUserinfoByLogin($login);

		// Return here, if there is no (valid) answer from the FreeIPA server
		if (!is_array($userInfo) || !isset($userInfo['memberof_group']) || !is_array($userInfo['memberof_group']))
			return(true);

		// Run thru the groups the user is in
		foreach ($userInfo['memberof_group'] as $group)
			// Check, if the current group is not in the list with groups to omit
			if (!in_array($group, $omitGroups))
				// Remove user from current group and combine error codes (if one delete command fails, return value will be false)
 				$ret &= $this->delLoginFromGroup($login, $group);

		// Make sure it's a boolean
		$ret = (bool)$ret;
		return($ret);
	}
}
?>