<?php

class CSchoolLDAPKerberos extends CLdap
{

	protected $schoolName = '';





/**
**name CSchoolLDAPKerberos::isSchoolExisting($schoolName)
**description Checks, if a school is existing in LDAP.
**parameter schoolName: Name of the school.
**/
	private function isSchoolExisting($schoolName)
	{
	
		if (false === $this->search('(cn=*)', array("*"), "dc=$schoolName"))
			return(false);
		else
			return(true);
	}





/**
**name CSchoolLDAPKerberos::setSchool($schoolName)
**description Sets the name of the school (must be a DC in the LDAP) under the base.
**parameter schoolName: Name of the school.
**/
	public function setSchool($schoolName)
	{
		$this->schoolName = $schoolName;
		$this->setStartRDN("dc=$schoolName");
	}





/**
**name CSchoolLDAPKerberos::addServer($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
**description Adds a client to LDAP as FusionDirectory server.
**parameter clientName: Name of the client
**parameter ip: IP address of the client
**parameter mac: MAC address of the client
**parameter serial: Serial number
**parameter description: Description for the client
**parameter location: Location of the client
**parameter pwd: the unencrypted password
**returns true, if the client could be added, otherwise false.
**/
	public function addServer($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
	{
		return($this->addComputer('fdserverrdn', 'fdServer', $clientName, $ip, $mac, $serial, $description, $location, $pwd));
	}





/**
**name CSchoolLDAPKerberos::addWorkstation($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
**description Adds a client to LDAP as FusionDirectory workstation.
**parameter clientName: Name of the client
**parameter ip: IP address of the client
**parameter mac: MAC address of the client
**parameter serial: Serial number
**parameter description: Description for the client
**parameter location: Location of the client
**parameter pwd: the unencrypted password
**returns true, if the client could be added, otherwise false.
**/
	public function addWorkstation($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
	{
		return($this->addComputer('fdworkstationrdn', 'fdWorkstation', $clientName, $ip, $mac, $serial, $description, $location, $pwd));
	}





/**
**name CSchoolLDAPKerberos::addComputer($fdConfigVar, $fdObjectClass, $computerName, $ip, $mac, $serial, $description = '', $location = '')
**description Adds a computer to LDAP as FusionDirectory object.
**parameter fdConfigVar: Name of the FusionDirectory config variable holding the RDN.
**parameter fdObjectClass: FusionDirectory object class of the new element.
**parameter computerName: Name of the client
**parameter ip: IP address of the client
**parameter mac: MAC address of the client
**parameter serial: Serial number
**parameter desciption: Description for the client
**parameter location: Location of the client
**parameter pwd: the (un)encrypted password
**parameter cryptPw: set to true, if the password should be encrypted or false, if it's already encrypted
**returns true, if the client could be added, otherwise false.
**/
	public function addComputer($fdConfigVar, $fdObjectClass, $computerName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '', $cryptPw = true)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		
		$pwdUnhashed = '';

		$computerName = I18N_safeUTF8_encode($computerName);
		$description = I18N_safeUTF8_encode($description);
		$location = I18N_safeUTF8_encode($location);

		// Full DN where to add
		$dn = $this->getFullDNByFdRDN($fdConfigVar,"cn=$computerName");
		
		// Ensure that the MAC parts are separated by ":"
		$mac = CLIENT_convertMac($mac, ':');

		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]=$fdObjectClass;
		$ldapAttributes['objectClass'][1]="ipHost";
		$ldapAttributes['objectClass'][2]="ieee802Device";
		$ldapAttributes['fdMode'][0]='unlocked';
		$ldapAttributes['cn'][0]=$computerName;
		$ldapAttributes['macAddress'][0]=$mac;
		$ldapAttributes['ipHostNumber'][0]=$ip;

		$ldapAttributes['description'][0] = I18N_safeUTF8_encode($serial);

		if (isset($description[0]))
			$ldapAttributes['description'][0] .= ' <= '.I18N_safeUTF8_encode($description);
			
		if (isset($location[0]))
			$ldapAttributes['l'][0]=I18N_safeUTF8_encode($location);

		// Add the password to LDAP, if set
// 		if (isset($pwd[1]))
// 		{
// 			// Encrypt the password if requested
// 			if ($cryptPw) $pwd = $this->getPasswordHash($pwd);
// 
// 			$ldapAttributes['userPassword'][0]=$pwd;
// 			$ldapAttributes['objectClass'][3]="posixAccount";
// 		}


// 		print_r($ldapAttributes);
// 		print("<h4>DN: $dn</h4>");

		if ($this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotAddComputer) === false)
			return(false);

		// Add Kerberos computer entries, if the password is given
		if (isset($pwd[1]))
			return($this->addKerberosComputer($fdConfigVar, $computerName, $pwd));
		else
			return(true);
	}






/**
**name CSchoolLDAPKerberos::deleteComputer($fdConfigVar, $fdObjectClass, $computerName)
**description Deletes a computer object from LDAP.
**parameter fdConfigVar: Name of the FusionDirectory config variable holding the RDN.
**parameter fdObjectClass: FusionDirectory object class of the new element.
**parameter computerName: Name of the client
**returns true, if the client could be added, otherwise false.
**/
	public function deleteComputer($fdConfigVar, $fdObjectClass, $computerName)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
	
		$computerName = I18N_safeUTF8_encode($computerName);

		// Full DN where to add
		$dn = $this->getFullDNByFdRDN($fdConfigVar,"cn=$computerName");
	
		return($this->ldap_delete($dn, $I18N_LDAPcouldNotDeleteComputer));
	}





/**
**name CSchoolLDAPKerberos::deleteServer($clientName)
**description Deletes a client/server from LDAP.
**parameter clientName: Name of the client
**returns true, if the client could be deleted, otherwise false.
**/
	public function deleteServer($clientName)
	{
		return($this->deleteComputer('fdserverrdn', 'fdServer', $clientName));
	}





/**
**name CSchoolLDAPKerberos::deleteWorkstation($clientName)
**description Deletes a client/workstation from LDAP.
**parameter clientName: Name of the client
**returns true, if the client could be deleted, otherwise false.
**/
	public function deleteWorkstation($clientName)
	{
		return($this->deleteComputer('fdworkstationrdn', 'fdWorkstation', $clientName));
	}





/**
**name CSchoolLDAPKerberos::createUser($account, $forename, $familyname, $pwd, $cryptPw = true)
**description Adds a posix account to the LDAP server and encrypts the password with MD5.
**parameter account: the login name
**parameter forename: the forename of the user
**parameter familyname: the familyname of the user
**parameter pwd: the unencrypted password
**parameter uid: Linux user ID
**parameter gid: Linux group ID
**parameter cryptPw: set to true, if the password should be encrypted or false, if it's already encrypted
**returns true or false in case of an error.
**/
	public function createUser($account, $forename, $familyname, $pwd, $cryptPw = true)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		
		$pwdUnhashed = NULL;
	
		$forename = I18N_safeUTF8_encode($forename);
		$familyname = I18N_safeUTF8_encode($familyname);
		if (empty($familyname))
			$familyname="-";

		$uid = $this->getFreeUserIDs();
		$gid = $this->getFreeGroupIDs();
	
		// Pa�wort-Hash-Algorithmus in LDAP umstellen
		// cn=config
		// olcPasswordHash: {CRYPT}
		// olcPasswordCryptSaltFormat: $6$%.16s

		// Radius-Attribute
		// radiusTunnelMediumType: IEEE-802
		// radiusTunnelType: VLAN
		// radiusFramedProtocol: PPP
		// radiusFramedIPAddress: 255.255.255.254
		// radiusFramedMTU: 1500
		// radiusFramedCompression: Van-Jacobsen-TCP-IP
		// radiusServiceType: Framed-User
		// radiusIdleTimeout: 10
		// radiusPortLimit: 2

		// Zus�tzliche Attribute, die verwendet werden k�nnten
		// 	employeeType: Lehrer
		// 	o: EIS
		// 	ou: Klasse1a
		// preferredLanguage: de_DE

		
		// Encrypt the password if requested
		if ($cryptPw)
		{
			$pwdUnhashed = $pwd;
			$pwd = $this->getPasswordHash($pwd);
		}

		// Build the array for creating a new user entry
		$ldapAttributes=array();
		$ldapAttributes['uid'][0]=$account;
		$ldapAttributes['givenName'][0]=$forename;
		$ldapAttributes['sn'][0]=$familyname;
		$ldapAttributes['cn'][0]="$forename $familyname";
		$ldapAttributes['gecos'][0]=I18N_replaceUmlaute("$forename $familyname");
		$ldapAttributes['userPassword'][0]=$pwd;
		$ldapAttributes['loginShell'][0]="/bin/bash";
		$ldapAttributes['uidNumber'][0]=$uid;
		$ldapAttributes['gidNumber'][0]=$gid;
		$ldapAttributes['homeDirectory'][0]="/home/$account";
		$ldapAttributes['shadowMin'][0]=0;
		$ldapAttributes['shadowMax'][0]=999999;
		$ldapAttributes['shadowWarning'][0]=7;
		$ldapAttributes['shadowInactive'][0]=0;
		$ldapAttributes['shadowExpire'][0]=999999;
		$ldapAttributes['shadowFlag'][0]=0;
		$ldapAttributes['objectClass'][0]="top";
		$ldapAttributes['objectClass'][1]="person";
		$ldapAttributes['objectClass'][2]="posixAccount";
		$ldapAttributes['objectClass'][3]="shadowAccount";
		$ldapAttributes['objectClass'][4]="inetOrgPerson";
		$ldapAttributes['objectClass'][5]="organizationalPerson";
		$ldapAttributes['objectClass'][6]="radiusprofile";
		
	// 	print("<h4>@ldap_add($ds , $dn</h4>");
// 		print_r($ldapAttributes);
	
		// Full DN where to add
		$dn = $this->getFullDNByFdRDN('fduserrdn',"uid=$account");
		
		if (!$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreatePosixAccount))
			return(false);

		// Create a group name by the account
		if (!$this->createUserGroup($account, $gid, "Group: $account"))
			return(false);

		// Add the user to the group
		if (!$this->addAccountToGroup($account, $account))
			return(false);

		// Add Kerberos attributes
		if (!is_null($pwdUnhashed))
			$this->addKerberosPrincipalToExistingLDAP($dn, $account, $pwdUnhashed);

		return(true);
	}





/**
**name CSchoolLDAPKerberos::deleteUser($account)
**description Removes a posix account from the LDAP server.
**parameter account: the login name
**returns true or false in case of an error.
**/
	public function deleteUser($account)
	{
		$ret = true;

/*
	Commented out, because removing the user from the group isn't needed, when the group is removed too

		// Delete the user from the group
		if (!$this->delAccountFromGroup($account, $account))
		{
			$this->showMessages();
			return(false);
		}
*/

		// Remove Kerberos attributes
		$this->delKerberosPrincipalFromExistingLDAP($account);

/*
	Commented out, because Kerberos principal is added globally (?) in LDAP and this function will be called on each school. So removing the Kerberos principal will work only on fist call and fail on all others.

		if (!$this->delKerberosPrincipalFromExistingLDAP($account))
		{
			$this->showMessages();
			$ret &= false;
		}
*/
		// Delete a group
		if (!$this->deleteGroup($account))
		{
			$this->showMessages();
			$ret &= false;
		}

		// Delete the user from LDAP
		$dn = $this->getFullDNByFdRDN('fduserrdn',"uid=$account");
		if (!$this->ldap_delete($dn, $I18N_LDAPcouldNotDeletePosixAccount))
		{
			$this->showMessages();
			$ret &= false;
		}

		$ret = (bool)$ret;
		return($ret);
}





/**
**name CSchoolLDAPKerberos::deleteGroup($groupname)
**description Deletes a posix group from the LDAP server.
**parameter groupname: Name of the group.
**returns true or error message string in case of an error.
**/
	public function deleteGroup($groupname)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$dn = $this->getFullDNByFdRDN('fdgrouprdn',"cn=$groupname");
		if ($this->ldap_delete($dn, $I18N_LDAPcouldNotDeletePosixGroup) === false)
		{
			$this->showMessages();
			return(false);
		}

		return(true);
	}





/**
**name CSchoolLDAPKerberos::delKerberosPrincipalFromExistingLDAP($dn, $account)
**description Removess Kerberos principal attributes from an existing LDAP object.
**parameter dn: DN where to delete the attributes from.
**parameter account: the login name
**returns Output of kadmin.local delprinc...
**/
	public function delKerberosPrincipalFromExistingLDAP($account)	// <= UNTESTED
	{
		$cmds = "
		kadmin.local delprinc -force '$account'
		";

		return(SERVER_runInBackground(uniqid('delKerberosPrincipalFromExistingLDAP'), $cmds, "root", false));
	}





/**
**name CSchoolLDAPKerberos::addKerberosPrincipalToExistingLDAP($dn, $account, $pwd)
**description Adds Kerberos principal attributes to an existing LDAP object.
**parameter dn: DN where to add the attributes.
**parameter account: the login name
**parameter pwd: the unencrypted password
**returns Output of kadmin.local addprinc...
**/
	public function addKerberosPrincipalToExistingLDAP($dn, $account, $pwd)
	{
		$cmds = "
		yes '$pwd' | kadmin.local addprinc -x 'dn=$dn' '$account'
		";
	
		return(SERVER_runInBackground(uniqid('addKerberosPrincipalToExistingLDAP'), $cmds, "root", false));
	}




/**
**name CSchoolLDAPKerberos::addKerberosWorkstation($computerName, $pwd)
**description Adds a Kerberos entries to an existing FusionDirectory workstation object in LDAP.
**parameter computerName: Name of the client
**parameter pwd: the (un)encrypted password
**returns true, if the Kerberos entries could be added, otherwise false.
**/
	public function addKerberosWorkstation($computerName, $pwd)
	{
		return($this->addKerberosComputer('fdworkstationrdn', $computerName, $pwd));
	}





/**
**name CSchoolLDAPKerberos::addKerberosServer($computerName, $pwd)
**description Adds a Kerberos entries to an existing FusionDirectory server object in LDAP.
**parameter computerName: Name of the client
**parameter pwd: the (un)encrypted password
**returns true, if the Kerberos entries could be added, otherwise false.
**/
	public function addKerberosServer($computerName, $pwd)
	{
		return($this->addKerberosComputer('fdserverrdn', $computerName, $pwd));
	}





/**
**name CSchoolLDAPKerberos::addKerberosComputer($fdConfigVar, $computerName, $pwd)
**description Adds a Kerberos entries to an existing FusionDirectory computer object in LDAP.
**parameter fdConfigVar: Name of the FusionDirectory config variable holding the RDN.
**parameter computerName: Name of the client
**parameter pwd: the (un)encrypted password
**returns true, if the Kerberos entries could be added, otherwise false.
**/
	public function addKerberosComputer($fdConfigVar, $computerName, $pwd)
	{
		// Add Kerberos entries only, if Kerberos' default domain and realm are given
		if (($this->getKRB_defaultDomain() === false) || ($this->getKRB_realm() === false))
			return(false);

		// DN of the existing computer object in LDAP
		$dn = $this->getFullDNByFdRDN($fdConfigVar, "cn=$computerName");

		// The account is a "host" entry, containing the hostname, domain and Kerberos realm
		// eg. "host/testclient.example.com@$REALM"
		$pricipal = $this->getHostPrincipalName($computerName);

		print("<h4>$dn, $pricipal, $pwd</h4>");

		return($this->addKerberosPrincipalToExistingLDAP($dn, $pricipal, $pwd));
	}





/**
**name CSchoolLDAPKerberos::getHostPrincipalName($computerName)
**description Gets the Kerberos host principal for a given computer.
**parameter computerName: Name of the client
**returns Kerberos host principal.
**/
	private function getHostPrincipalName($computerName)
	{
		return("host/$computerName.".$this->getKRB_defaultDomain()."@".$this->getKRB_realm());
	}





/**
**name CSchoolLDAPKerberos::getHostPrincipalKeytab($computerName)
**description Gets the Kerberos keytab for a given computer in base64 encoded format.
**parameter computerName: Name of the client
**returns Kerberos keytab.
**/
	public function getHostPrincipalKeytab($computerName)
	{
		$pricipal = $this->getHostPrincipalName($computerName);

		$tempFile = uniqid('/tmp/');

		$cmds = "
			kadmin.local ktadd -k $tempFile -q $pricipal && base64 $tempFile; rm $tempFile
		";

		return(SERVER_runInBackground(uniqid('getHostPrincipalKeytab'), $cmds, "root", false));
	}





/**
**name CSchoolLDAPKerberos::createUserGroup($groupname, $gid, $description)
**description Creates a posix group on the LDAP server.
**parameter groupname: Name of the group.
**parameter gid: Linux group ID
**parameter description: Description of the group.
**returns true or error message string in case of an error.
**/
	public function createUserGroup($groupname, $gid, $description)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$groupname = I18N_safeUTF8_encode($groupname);
		$description = I18N_safeUTF8_encode($description);

		// Build the array for creating a new group entry
		$ldapAttributes=array();
		$ldapAttributes['cn'][0] = $groupname;
		$ldapAttributes['gidNumber'][0] = $gid;
		$ldapAttributes['objectClass'][0]="top";
		$ldapAttributes['objectClass'][1]="posixGroup";
		$ldapAttributes['description'][0] = $description;
		$dn = $this->getFullDNByFdRDN('fdgrouprdn',"cn=$groupname");
		return($this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreatePosixGroup));
	}





/**
**name CSchoolLDAPKerberos::addAccountToGroup($groupname, $account)
**description Adds a posix account to a posix group on the LDAP server.
**parameter groupname: Name of the group.
**parameter gid: Linux group ID
**returns true or error message string in case of an error.
**/
	public function addAccountToGroup($groupname, $account)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		// Build the array for creating a new group entry
		$ldapAttributes=array();
		$ldapAttributes['memberUid'] = $account;
		$dn = $this->getFullDNByFdRDN('fdgrouprdn',"cn=$groupname");
		return($this->ldap_mod_add($dn, $ldapAttributes, "$I18N_LDAPcouldNotAddUserToPosixGroup: $account => $groupname"));
	}





/**
**name CSchoolLDAPKerberos::delAccountFromGroup($groupname, $account)
**description Removes a posix account from a posix group on the LDAP server.
**parameter groupname: Name of the group.
**parameter gid: Linux group ID
**returns true or error message string in case of an error.
**/
	public function delAccountFromGroup($groupname, $account)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		// Build the array for creating a new group entry
		$ldapAttributes=array();
		$ldapAttributes['memberUid'] = $account;
		$dn = $this->getFullDNByFdRDN('fdgrouprdn',"cn=$groupname");
		return($this->ldap_mod_del($dn, $ldapAttributes, "$I18N_LDAPcouldNotDelUserFromPosixGroup: $account != $groupname"));
	}





/**
**name CSchoolLDAPKerberos::createSchoolStructure($schoolName, $description)
**description Creates the LDAP structure for a new school.
**parameter schoolName: Short name of the school.
**parameter description: Full school name.
**/
	public function createSchoolStructure($schoolName, $description)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$description = I18N_safeUTF8_encode($description);

		// Clear the start RDN because the school container doesn't exist right now
		$this->clearStartRDN();

		// Create a new school container
		$ldapAttributes=array();
		$ldapAttributes['dc'][0] = $schoolName;
		$ldapAttributes['ou'][0] = $schoolName;
		$ldapAttributes['objectClass'][0]="top";
		$ldapAttributes['objectClass'][1]="domain";
		$ldapAttributes['objectClass'][2]="gosaDepartment";
		$ldapAttributes['description'][0]=$description;
		$this->ldap_add("dc=$schoolName,".$this->getStartRDN(), $ldapAttributes, $I18N_LDAPcouldNotCreateSchool);

		// Set the school name and its RDN again
		$this->setSchool($schoolName);

		// Create container for devices (server, workstations, network devices)
		$dn = $this->getFullDNByFdRDN('fdsystemrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="systems";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_devices);//	<== LEER?
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateDevices);


		// Create container for workstations
		$dn = $this->getFullDNByFdRDN('fdworkstationrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="workstations";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_workstations);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateWorkstations);


		// Create container for servers
		$dn = $this->getFullDNByFdRDN('fdserverrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="servers";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_servers);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateServers);


		// Create container for users
		$dn = $this->getFullDNByFdRDN('fduserrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="people";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_users);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateUsers);


		// Create container for groups
		$dn = $this->getFullDNByFdRDN('fdgrouprdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="groups";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_groups);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateGroups);


		// Create container for printers
		$dn = $this->getFullDNByFdRDN('fdprinterrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="printers";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_printers);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreatePrinters);


		// Create container for network devices
		$dn = $this->getFullDNByFdRDN('fdcomponentrdn');
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="organizationalUnit";
		$ldapAttributes['ou'][0]="netdevices";
		$ldapAttributes['description'][0]=I18N_safeUTF8_encode($I18N_LDAP_DIT_netdevices);
		$this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateNetdevices);

		// Create a group for all users who should be able to become root by sudoing
		$this->createUserGroup('m23sudo', 777, 'Group for sudo root users');

		// Create groups for different access levels
		$this->createUserGroup('teacher', 60000 - CSchool::SCHOOL_ROLE_TEACHER, 'Group for all teachers');
		$this->createUserGroup('pupils', 60000 - CSchool::SCHOOL_ROLE_PUPIL, 'Group for all pupils');
		$this->createUserGroup('schooladmin', 60000 - CSchool::SCHOOL_ROLE_SCHOOLADMIN, 'Group for all school admins');
	}





/**
**name CSchoolLDAPKerberos::moveUserToSchool($account, $newSchool)
**description Moves a posix account from the current school to another.
**parameter account: the login name
**parameter newSchool: Name of the new (existing) school.
**returns true or error message string in case of an error.
**/
	public function moveUserToSchool($account, $newSchool)
	{
		// Full DN where the account is currently stored
		$oldDN = $this->getFullDNByFdRDN('fduserrdn',"uid=$account");

		// The new parent DN (ou=people,dc=... without "uid=$account") where the account should be moved to
		$newParentDN = $this->getFullDNByFdRDN('fduserrdn', '', "dc=$newSchool");

		return($this->ldap_rename($oldDN, "uid=$account" , $newParentDN, $I18N_LDAPcouldNotMoveUser));
	}






/**
**name CSchoolLDAPKerberos::addPrinter($printerName, $ip, $mac, $location, $labeledURI)
**description Adds a printer to LDAP in FusionDirectory style.
**parameter printerName: Name of the printer
**parameter ip: IP address of the printer
**parameter mac: MAC address of the printer
**parameter description: Description for the printer
**parameter location: Location of the client
**parameter labeledURI: The printer URI.
**returns true, if the printer could be added, otherwise false.
**/
	public function addPrinter($printerName, $ip, $mac, $description, $location, $labeledURI)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$printerName = I18N_safeUTF8_encode($printerName);
		$description = I18N_safeUTF8_encode($description);
		$location = I18N_safeUTF8_encode($location);

		// Full DN where to add
		$dn = $this->getFullDNByFdRDN('fdprinterrdn',"cn=$printerName");
		
		// Ensure that the MAC parts are separated by ":"
		$mac = CLIENT_convertMac($mac, ':');

		// Build the array for creating a new group entry
		$ldapAttributes=array();
		$ldapAttributes['cn'][0] = $printerName;
		$ldapAttributes['description'][0] = $description;
		$ldapAttributes['macAddress'][0]=$mac;
		$ldapAttributes['ipHostNumber'][0]=$ip;
		$ldapAttributes['l'][0]=$location;
		$ldapAttributes['labeledURI'][0]=$labeledURI;
		$ldapAttributes['objectClass'][0]="fdPrinter";
		$ldapAttributes['objectClass'][1]="ipHost";
		$ldapAttributes['objectClass'][2]="ieee802Device";

		return($this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreatePrinter));
	}





/**
**name CSchoolLDAPKerberos::addNetDevices($deviceName, $ip, $mac, $description)
**description Adds a network device to LDAP in FusionDirectory style.
**parameter deviceName: Name of the netdevice
**parameter ip: IP address of the netdevice
**parameter mac: MAC address of the netdevice
**parameter description: Description for the netdevice
**returns true, if the netdevice could be added, otherwise false.
**/
	public function addNetDevices($deviceName, $ip, $mac, $description)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$printerName = I18N_safeUTF8_encode($deviceName);

		// Full DN where to add
		$dn = $this->getFullDNByFdRDN('fdcomponentrdn',"cn=$deviceName");
		
		// Ensure that the MAC parts are separated by ":"
		$mac = CLIENT_convertMac($mac, ':');

		// Build the array for creating a new group entry
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]="top";
		$ldapAttributes['objectClass'][1]="device";
		$ldapAttributes['objectClass'][2]="ipHost";
		$ldapAttributes['objectClass'][3]="ieee802Device";
		$ldapAttributes['cn'][0] = $deviceName;
		$ldapAttributes['description'][0] = $description;
		$ldapAttributes['ipHostNumber'][0]=$ip;
		$ldapAttributes['macAddress'][0]=$mac;

		return($this->ldap_add($dn, $ldapAttributes, $I18N_LDAPcouldNotCreateNetdevice));
	}





/**
**name CSchoolLDAPKerberos::getUsedLDAPIPs($intIPs = false)
**description Get an array with IP addresses that are used for computers and devices in the LDAP.
**parameter intIPs: Set to true, if the IPs should be converted to integer values.
**returns Array with IP addresses that are used for computers and devices in the LDAP
**/
	public function getUsedLDAPIPs($intIPs = false)
	{
		$usedIPs = $this->filterLDAPEntries($this->search('(ipHostNumber=*)', array('ipHostNumber'), ''), 'iphostnumber');
		
		if ($intIPs)
			$usedIPs = array_map('ip2longSafe', $usedIPs);
	
		return($usedIPs);
	}





/**
**name CSchoolLDAPKerberos::IPexists($ip, $addErrorMessageIfIPExists = false))
**description Checks if an IP with the selected IP exists in LDAP for the current school
**parameter ip: IP to check
**parameter addErrorMessageIfIPExists: Set to true, if an error message should be added to the message stack, if the IP exists
**returns true, if the IP exists (is used), otherwise false.
**/
// 	public function IPexists($ip, $addErrorMessageIfIPExists = false)
// 	{
// 		// Array with all used IPs
// 		$usedIPs = $this->getUsedLDAPIPs();
// 
// 		// Is the asked IP used?
// 		$isUsed = in_array($ip , $usedIPs);
// 
// 		// Add an error message to the message stack if the IP exists
// 		if ($addErrorMessageIfIPExists && $isUsed)
// 		{
// 			include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
// 			$this->addErrorMessage("$I18N_ip_exists (LDAP: $ip)");
// 		}
// 
// 		return($isUsed);
// 	}




/**
**name CSchoolLDAPKerberos::createRootUser($account,$forename,$familyname,$pwd, $cryptPw = true)
**description Adds a posix account that should act as root to the LDAP server and encrypts the password with MD5.
**parameter account: the login name
**parameter forename: the forename of the user
**parameter familyname: the familyname of the user
**parameter pwd: the unencrypted password
**parameter uid: Linux user ID
**parameter gid: Linux group ID
**parameter cryptPw: set to true, if the password should be encrypted or false, if it's already encrypted
**returns true if all is ok or false in case of an error.
**/
	public function createRootUser($account, $forename, $familyname, $pwd, $cryptPw = true)
	{
		$ret = true;

		if (!$this->createUser($account, $forename, $familyname, $pwd, $cryptPw))
		{
			$this->showMessages();
			$ret &= false;
		}

		if (!$this->addAccountToGroup('m23sudo', $account))
		{
			$this->showMessages();
			$ret &= false;
		}

		$ret = (bool)$ret;
		return($ret);
	}





/**
**name CSchoolLDAPKerberos::deleteRootUser($account)
**description Deletes a posix account that should act as root from the LDAP server
**returns true if all is ok or false in case of an error.
**/
	public function deleteRootUser($account)
	{
		$ret = true;

		if (!$this->delAccountFromGroup('m23sudo', $account))
		{
			$this->showMessages();
			$ret &= false;
		}

		if (!$this->deleteUser($account))
		{
			$this->showMessages();
			$ret &= false;
		}

		$ret = (bool)$ret;
		return($ret);
	}





/**
**name CSchoolLDAPKerberos::createMegaAdmin($loginName, $password)
**description Creates a new account, that is administrator in FusionDirectory and m23 and that can login into m23 clients and become root via sudo.
**parameter loginName: the login name
**parameter password: the unencrypted password
**returns true, if the mega administrator was created successfully, otherwise false.
**/
	public static function createMegaAdmin($loginName, $password)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		$ret = true;

		// Create a new FusionDirectory administrator
		$CLdapO = new CLdap(CLdap::get2ndLDAPServer());
		$ret &= $CLdapO->createFDAdmin("fd-$loginName", $password);
		if ($CLdapO->hasErrors())
		{
			$CLdapO->showMessages();
			$ret &= false;
		}

		// Create a new m23 administrator
		$m23AdminO = new Cm23Admin($loginName, $password);
		if ($m23AdminO->hasErrors())
		{
			$m23AdminO->showMessages();
			$ret &= false;
		}

		// Run thru all schools
		foreach (CSchool::getSchoolNameWithDescription() as $shortName => $description)
		{
			$CSchoolO = new CSchool($shortName, "doesn't work");	// <= TODO or remove?
			// Add Kerberos and posixAccount/Group with radiusProfile in LDAP for the given school
			$ret &= $CSchoolO->createRootUser($loginName, 'Admin', $loginName, $password);
// 			print("<h3>createMegaAdmin($shortName => $description): ".serialize($ret)."</h3>");
			if ($CSchoolO->hasErrors())
				$CSchoolO->showMessages();
			unset($CSchoolO);
		}

		$ret = (bool)$ret;
		if ($ret) MSG_showInfo($I18N_megaAdminCreatedSuccessfully);
		return($ret);
	}





/**
**name CSchoolLDAPKerberos::deleteMegaAdmin($loginName)
**description Deletes a mega administrator.
**parameter loginName: the login name
**returns true, if the mega administrator was deleted successfully, otherwise false.
**/
	public static function deleteMegaAdmin($loginName)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		$ret = true;

// print("<h3>CSchoolLDAPKerberos::deleteMegaAdmin($loginName)</h3>");

		// Delete a FusionDirectory administrator
		$CLdapO = new CLdap(CLdap::get2ndLDAPServer());
		$ret &= $CLdapO->deleteFDAdmin("fd-$loginName");
// print("<h3>1CSchoolLDAPKerberos::deleteMegaAdmin($loginName):".serialize($ret)."</h3>");
		if ($CLdapO->hasErrors())
		{
			$CLdapO->showMessages();
			$ret &= false;
		}
// print("<h3>1aCSchoolLDAPKerberos::deleteMegaAdmin($loginName):".serialize($ret)."</h3>");

		// Run thru all schools
		foreach (CSchool::getSchoolNameWithDescription() as $shortName => $description)
		{
			$CSchoolO = new CSchool($shortName, "doesn't work");	// <= TODO or remove?
			// Remove user from LDAP from a given school
			$ret &= $CSchoolO->deleteRootUser($loginName);
// print("<h3>2CSchool::deleteMegaAdmin($loginName):".serialize($ret)."</h3>");
			unset($CSchoolO);
		}

		// Delete an m23 administrator
		$m23AdminO = new Cm23Admin($loginName);
		$m23AdminO->delete();
		if ($m23AdminO->hasErrors())
		{
			$m23AdminO->showMessages();
			$ret &= false;
		}
// print("<h3>3CSchool::deleteMegaAdmin($loginName):".serialize($ret)."</h3>");

		$ret = (bool)$ret;
		if ($ret) MSG_showInfo($I18N_megaAdminDeletedSuccessfully);
		return($ret);
	}





/**
**name CSchoolLDAPKerberos::getMegaAdmins()
**description Generates an array with all mega administrators.
**returns Associative array with all mega administrators as key and value.
**/
	public static function getMegaAdmins()
	{
		$out = array();

		// Open LDAP connection to the 2nd LDAP server
		$CLdapO = new CLdap(CLdap::get2ndLDAPServer());

		// Run thru the list of m23 admins
		foreach (Cm23AdminLister::ListAdmins() as $nr => $adminName)
		{
			// m23 admins that have an LDAP account staring with "fd-" are detected as mega admins
			$res = $CLdapO->search("(cn=fd-$adminName)");
			if ($res['count'] > 0)
				$out[$adminName] = $adminName;
		}

		unset($CLdapO);
		return($out);
	}





/**
**name CSchoolLDAPKerberos::createHostGroup($group, $description)
**description Does nothing. Just here to be compatible with FreeIPA implementation.
**parameter group: Name of the group
**parameter description: description of the group
**returns true.
**/
	public function createHostGroup($group, $description)
	{
		return(true);
	}





/**
**name CSchoolLDAPKerberos::createUserGroup($group, $description)
**description Does nothing. Just here to be compatible with FreeIPA implementation.
**parameter group: Name of the group
**parameter description: description of the group
**returns true.
**/
	public function createUserGroup($group, $description)
	{
		return(true);
	}
}
?>