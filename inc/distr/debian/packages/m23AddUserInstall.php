<?PHP
/*
Description:Adds a new user on Debian/Ubuntu.
Priority:125
*/

include ('/m23/data+scripts/packages/m23CommonInstallRoutines.php');
include ('/m23/inc/distr/debian/clientConfigCommon.php');

function run($id)
{
	$accountInfo = unserialize(PKG_getPackageParams($id));

	$uid = !empty($accountInfo['uid']) ? $accountInfo['uid'] : '';
	$gid = !empty($accountInfo['gid']) ? $accountInfo['gid'] : '';

//m23customPatchBegin type=change id=skipCreateUserBegin
//m23customPatchEnd id=skipCreateUserBegin

if (isset($accountInfo['groupsUbuntu']))
{
	echo('
	'.BASH_ifDetectDebian.'
	then
	');
		CLCFG_addUser($accountInfo['login'], $accountInfo['firstpw'], $accountInfo['groupsDebian'], $uid, $gid);
	echo('
	fi

	'.BASH_ifDetectUbuntu.'
	then
	');
		CLCFG_addUser($accountInfo['login'], $accountInfo['firstpw'], $accountInfo['groupsUbuntu'], $uid, $gid);
	echo('
	fi
	');
}
	else
		CLCFG_addUser($accountInfo['login'], $accountInfo['firstpw'], $accountInfo['groups'], $uid, $gid);


//m23customPatchBegin type=change id=skipCreateUserEnd
//m23customPatchEnd id=skipCreateUserEnd
	/* =====> */ MSR_statusBarIncCommand(100);
	
	// DebianVersionSpecific
/*
	echo('
	# Reconfigure libpam-runtime on all distributions but Debian 11
	if [ $(grep -c "Debian GNU/Linux 11" /etc/issue) -gt 0 ]
	then
		# Check, if LDAP is used
		if [ $(debconf-get-selections | grep libpam-runtime/profiles | grep ldap -c) -gt 0 ] && [ $(debconf-get-selections | grep libpam-runtime/profiles | grep mkhomedir -c) -eq 0 ]
		then
			param="$(debconf-get-selections | grep libpam-runtime/profiles | grep ldap | tr -s "[:blank:]" | sed "s/.*multiselect[[:blank:]]//")"

			echo "libpam-runtime libpam-runtime/profiles multiselect $param, mkhomedir
libpam-runtime libpam-runtime/override boolean true
libpam-runtime libpam-runtime/conflicts note" > /pam.debconf

			debconf-set-selections /pam.debconf
			
			debconf-get-selections | grep libpam-runtime > /dpkg-reconfigure-libpam-runtime1.debconf
			dpkg-reconfigure libpam-runtime --force &> /dpkg-reconfigure-libpam-runtime1.log
			debconf-get-selections | grep libpam-runtime > /dpkg-reconfigure-libpam-runtime1b.debconf
		fi
	
	
		export DEBIAN_FRONTEND=noninteractive
		dpkg-reconfigure libpam-runtime --force &> /dpkg-reconfigure-libpam-runtime2.log
echo $? > /dpkg-reconfigure-libpam-runtime.ret
debconf-get-selections | grep libpam-runtime > /dpkg-reconfigure-libpam-runtime2.debconf
	fi
	');
*/

	CLCFG_disableAptSystemdDaily();

	sendClientStatus($id,"done");
	executeNextWork();
};
?>