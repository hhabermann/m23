<?PHP
/*
Description: Enable FreeIPA on a client.
Priority:200
*/

function run($id)
{
	include('/m23/inc/distr/debian/clientConfigCommon.php');

	CLCFG_enableFreeIPA(CLIENT_getClientName());
	MSR_statusBarIncCommand(100);

	sendClientStageStatus(STATUS_GREEN);
	sendClientStatus($id,"done");

	echo("
		reboot
		sleep 5
		reboot -f
	");

// 	executeNextWork();
}
?>