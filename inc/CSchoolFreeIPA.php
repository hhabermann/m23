<?php

class CSchoolFreeIPA extends CFreeIPA
{
	protected $schoolName = '';






/**
**name CSchoolFreeIPA::setSchool($schoolName)
**description Sets the name of the school (must be a DC in the LDAP) under the base.
**parameter schoolName: Name of the school.
**/
	public function setSchool($schoolName)
	{
		$this->schoolName = $schoolName;
	}





/**
**name CSchoolFreeIPA::deleteWorkstation($clientName)
**description Deletes a client/workstation from LDAP.
**parameter clientName: Name of the client
**returns true, if the client could be deleted, otherwise false.
**/
	public function deleteWorkstation($clientName)
	{
		return($this->delComputer($clientName));
	}


/*
#####################################
#####################################
#####################################
#####################################
#####################################
*/



/**
**name CSchoolFreeIPA::IPexists($ip, $addErrorMessageIfIPExists = false))
**description Checks if an IP with the selected IP exists in LDAP for the current school
**parameter ip: IP to check
**parameter addErrorMessageIfIPExists: Set to true, if an error message should be added to the message stack, if the IP exists
**returns true, if the IP exists (is used), otherwise false.
**/
// 	public function IPexists($ip, $addErrorMessageIfIPExists = false)
// 	{
// 		// Array with all used IPs
// 		$usedIPs = $this->getUsedFreeIPAIPs();
// 		
// 		print_r($usedIPs);
// 
// 		// Is the asked IP used?
// 		$isUsed = in_array($ip , $usedIPs);
// 
// 		// Add an error message to the message stack if the IP exists
// 		if ($addErrorMessageIfIPExists && $isUsed)
// 		{
// 			include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
// 			$this->addErrorMessage("$I18N_ip_exists (FreeIPA: $ip)");
// 		}
// 
// 		return($isUsed);
// 	}





/**
**name CSchoolFreeIPA::addWorkstation($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
**description Adds a client to LDAP as FusionDirectory workstation.
**parameter clientName: Name of the client
**parameter ip: IP address of the client
**parameter mac: MAC address of the client
**parameter serial: Serial number
**parameter description: Description for the client
**parameter location: Location of the client
**parameter pwd: the unencrypted password
**returns true, if the client was added successfully, otherwise (error) messages from the ipa tool.	!= true, if the client could be added, otherwise false.
**/
	public function addWorkstation($clientName, $ip, $mac, $serial, $description = '', $location = '', $pwd = '')
	{
		// unused: $mac, $serial
		$locality = '';
		$platform = '';
		$os = '';
		$class = '';

		return($this->addComputer($clientName, $ip, $mac, $description, $class, $locality, $location, $platform, $os));
	}





/**
**name CSchoolFreeIPA::isSchoolExisting($schoolName)
**description Checks, if a school is existing in FreeIPA.
**parameter schoolName: Name of the school.
**/
	protected function isSchoolExisting($schoolName)
	{
		return($this->existHostGroup($schoolName));
	}





/**
**name CSchoolFreeIPA::createSchoolStructure($schoolName, $description)
**description Creates the group(s) for a new school in FreeIPA.
**parameter schoolName: Short name of the school.
**parameter description: Full school name.
**/
	public function createSchoolStructure($schoolName, $description)
	{
		return(true);
/*
		// Create group for all users (teachers, pupils, schooladmin)
		$this->createUserGroup($schoolName, "$description - All users");

		// Create separate groups for all users (teachers, pupils, schooladmin) and hosts (m23 clients)
		$this->createUserGroup($schoolName.'-teacher', "$description - Teacher");
		$this->createUserGroup($schoolName.'-pupil', "$description - Pupil");
		$this->createUserGroup($schoolName.'-schooladmin', "$description - Schooladmin");

		// Create group for all hosts (m23 clients)
		$this->createHostGroup($schoolName, "$description: All computers and devices");
*/
	}

}