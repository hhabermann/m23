<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for school administration.
$*/

class CSchoolManagement extends CSchool
{


/**
**name CSchoolManagement::__construct($in)
**description Constructor for new CSchoolManagement objects.
**parameter schoolName: Name of the school.
**/
	public function __construct($schoolName)
	{
		parent::__construct($schoolName);
	}





/**
**name CSchoolManagement::GUI_showSchoolChooser($generic = true, $hideIfButtonPressed = false)
**description Shows a dialog for selecting the school.
**parameter generic: If set to true show a generic school chooser, otherwise show the school management edition.
**parameter hideIfButtonPressed: No dialog will be shown, if set to true and the save button is clicked.
**returns Short name of the selected school, if button is pressed, otherwise empty string.
**/
	public static function GUI_showSchoolChooser($generic = true, $hideIfButtonPressed = false)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
		
		$buttonPressed = false;

		if (!isset($_SESSION['schMan']['schoolName'])) $_SESSION['schMan']['schoolName'] = '';

		if (HTML_submit('BUT_updateSchoolInfoFileFromDNSZones', $I18N_updateSchoolInfoFileFromDNSZones))
		{
			CFreeIPA::updateSchoolInfoFileFromDNSZones();
		}

		$schoolName = HTML_selection('SEL_schoolName', CSchool::getSchoolNameWithDescription(), SELTYPE_selection, true, $_SESSION['schMan']['schoolName']);

		if (HTML_submit('BUT_saveSchoolName', $I18N_select))
		{
			$_SESSION['schMan']['schoolName'] = $schoolName;
			$buttonPressed = true;
		}

		if ($hideIfButtonPressed && $buttonPressed)
			return($schoolName);

		if ($generic)
		{
			HTML_showTableHeader(true);
			HTML_showTableRow($I18N_schoolName, '');
			HTML_showTableRow(SEL_schoolName, BUT_saveSchoolName);
			HTML_showTableEnd(true);

			HTML_showTableHeader(true);
			HTML_showTableRow($I18N_onlyIfNeeded);
			HTML_showTableRow(BUT_updateSchoolInfoFileFromDNSZones);
			HTML_showTableEnd(true);
		}
		else
		{
			HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
			echo("
			<table align=\"center\">
			<tr>
				<td><div class=\"subtable_shadow\">
					<table align=\"center\" class=\"subtable2\">
						<tr>
							<td class=\"numberBorder\">1</td>
							<td>$I18N_schoolName</td>
							<td>".SEL_schoolName.'</br><center>'.BUT_saveSchoolName."</center></td>
							<td>$I18N_schoolNameHelpSchoolManagement</td>
						</tr>
					</table>
				</div></td>
			<tr></table>
			");
		}

		if ($buttonPressed)
			return($schoolName);
		else
			return('');
	}





/**
**name CSchoolManagement::GUI_showSchoolUserCreator() <= PROTOTYPE
**description Shows a dialog for creating a user (admin, teacher, pupil) in the school.
**/
	public static function GUI_showSchoolUserCreator()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		$schoolName = CSchoolManagement::GUI_showSchoolChooser(false);

		HTML_showJSsanitizeASCII('ED_loginName');

		$allOk = true;

		// Alternating coloring of table rows
		$rowColor = false;

		// Role of the user (teacher, pupil)
		$userSchoolRole = HTML_selection("SEL_userSchoolRole", CSchool::getUserSchoolRoles(false), SELTYPE_selection, true, false, false, 'onChange="disableClassIfTeacher();"');


		// Add javaScript functions
		$js = "
			<script>
				// Changes the texts of the release date, author and description spans by the choosen script name
				function disableClassIfTeacher()
				{
					var sel = document.getElementById('SEL_userSchoolRole');

					if (sel.value == ".CSchool::SCHOOL_ROLE_TEACHER.")
					{
						// Teachers have no grade, but can be school admins
						document.getElementById('ED_grade').disabled = true;
						document.getElementById('CB_schoolAdmin').disabled = false;
					}
					else
					{
						// Others have a grade, but can be no school admin
						document.getElementById('ED_grade').disabled = false;
						document.getElementById('ED_grade').value = '';

						document.getElementById('CB_schoolAdmin').disabled = true;
						document.getElementById('CB_schoolAdmin').checked = false;
					}
				}

				disableClassIfTeacher();
			</script>\n";










		// School admin or not?
		$schoolAdmin = HTML_checkBox('CB_schoolAdmin', '');

		// Grade of the pupil or class of a class teacher
		$grade = HTML_input("ED_grade", false, 4, 64);
// 		HTML_selection("ED_grade", CSchool::getSchoolGrades($schoolName), SELTYPE_selection);

		// Unique ID of the user for internal use (syncing with other databases)
		$exampleAllocationNumber = 'EIM1IER2ZO5VOHP5';
		$allocationNumber = HTML_input("ED_allocationNumber", false, 20, 64);

		// Login name: 1st letter of the forename, the family name and an increasing number in case of doublets
		$exampleLoginName = 'p.mueller';
		$loginName = HTML_input("ED_loginName", false, 20, 64,INPUT_TYPE_text, false, 'onChange="sanitizeASCII();"');

		

		// Password element
		$userPassword = '';
		if (SERVER_get2xPasswordDialogEnabled())
			HTML_storable2xPassword("ED_userpassword", false, false, $userPassword, 20, 20);
		else
			HTML_storableInput("ED_userpassword", false, false, $userPassword, 20, 20);

		// Creation button
		if (HTML_submit('BUT_create', $I18N_create))
		{
			if ($userPassword === false)
			{
				MSG_showError($I18N_firstpw_passwords_dont_match);
				$allOk = false;
			}
			
		
			MSG_showInfo("
				</br>&bull; $I18N_schoolName: $schoolName
				</br>&bull; $I18N_allocationNumber: $allocationNumber
				</br>&bull; $I18N_userSchoolRole: $userSchoolRole
				</br>&bull; $I18N_schoolAdmin: ".serialize($schoolAdmin)."
				</br>&bull; $I18N_grade: $grade
				</br>&bull; $I18N_login_name: $loginName
				</br>&bull; $I18N_userpassword: $userPassword
			");
		}





		HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
		echo("
		<table align=\"center\">
		<tr>
			<td><div class=\"subtable_shadow\">
				<table align=\"center\" class=\"subtable2\">
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">2</td>
						<td>$I18N_allocationNumber</td>
						<td>".ED_allocationNumber." </br>($I18N_eg $exampleAllocationNumber)</td>
						<td>$I18N_allocationNumberHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">3</td>
						<td>$I18N_userSchoolRole</td>
						<td>".SEL_userSchoolRole." </td>
						<td>$I18N_userSchoolRoleHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">4</td>
						<td>$I18N_schoolAdmin</td>
						<td>".CB_schoolAdmin."</td>
						<td>$I18N_schoolAdminHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">5</td>
						<td>$I18N_grade</td>
						<td>".ED_grade."</td>
						<td>$I18N_gradeHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">6</td>
						<td>$I18N_login_name</td>
						<td>".ED_loginName." </br>($I18N_eg $exampleLoginName)</td>
						<td>$I18N_login_nameHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">7</td>
						<td>$I18N_userpassword*</td>
						<td>".ED_userpassword."</td>
						<td>$I18N_userpasswordHelpSchoolManagement</td>
					</tr>
					<tr>
						<td class=\"numberBorder\">8</td>
						<td colspan=\"2\" align=\"center\">".BUT_create."</td>
						<td>$I18N_createUserHelpSchoolManagement</td>
					</tr>
				</table>
			</div></td>
		<tr></table>
		$js
		");
	}





/**
**name CSchoolManagement::GUI_showCreateMegaAdmin()
**description Shows a dialog for creating a admin.
**/
	public static function GUI_showCreateMegaAdmin()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		HTML_showJSsanitizeASCII('ED_loginName');
		$exampleLoginName = 'p.mueller';
		$loginName = HTML_input("ED_loginName", false, 20, 64,INPUT_TYPE_text, false, 'onChange="sanitizeASCII();"');

		// Password element
		$password = '';
		if (SERVER_get2xPasswordDialogEnabled())
			HTML_storable2xPassword("ED_password", false, false, $password, 20, 20);
		else
			HTML_storableInput("ED_password", false, false, $password, 20, 20);

		// Creation button
		if (HTML_submit('BUT_create', $I18N_create))
		{
			if ($password === false)
			{
				MSG_showError($I18N_firstpw_passwords_dont_match);
				return(false);
			}

			$CChecksO = new CChecks();
			if (!$CChecksO->checkm23AdminName($loginName))
				$CChecksO->showMessages();
			else
				CSchool::createMegaAdmin($loginName, $password);
			unset($CChecksO);
		}

/*
Admin
-----
* OHNE SVS-ID

Angaben:
	* Login
	* Pa�wort


* LDAP
	* Benutzer anlegen, löschen
	* Schulwechsel
	* Paßwort zurücksetzen


Berechtigungen:
	* m23sudo-Gruppe in allen Schulen
	* posixAccount
	* Admin in m23-Oberfl�che
*/






		HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
		echo("
		<table align=\"center\">
		<tr>
			<td><div class=\"subtable_shadow\">
				<table align=\"center\" class=\"subtable2\">
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">1</td>
						<td>$I18N_login_name</td>
						<td>".ED_loginName." </br>($I18N_eg $exampleLoginName)</td>
						<td>$I18N_login_nameAdminHelpSchoolManagement</td>
					</tr>
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">2</td>
						<td>$I18N_password*</td>
						<td>".ED_password."</td>
						<td>$I18N_userpasswordHelpSchoolManagement</td>
					</tr>
					<tr>
						<td class=\"numberBorder\">3</td>
						<td colspan=\"2\" align=\"center\">".BUT_create."</td>
						<td>$I18N_createUserHelpSchoolManagement</td>
					</tr>
				</table>
			</div></td>
		<tr></table>
		");
	}





/**
**name CSchoolManagement::GUI_showDeleteMegaAdmin()
**description Shows a dialog for deleting a admin.
**/
	public static function GUI_showDeleteMegaAdmin()
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");


		if (HTML_submit('BUT_delete', $I18N_delete))
		{
			$adminName = HTML_getElementValue('SEL_adminName', 'SEL_adminName', '');
			CSchool::deleteMegaAdmin($adminName);
		}

		$adminName = HTML_selection('SEL_adminName', CSchool::getMegaAdmins(), SELTYPE_selection);


		HTML_showTableHeader(true, 'subtable2', 'width="100%" cellspacing=10');
		echo("
		<table align=\"center\">
		<tr>
			<td><div class=\"subtable_shadow\">
				<table align=\"center\" class=\"subtable2\">
					<tr ".HTML_rowColor($rowColor).">
						<td class=\"numberBorder\">1</td>
						<td>$I18N_login_name</td>
						<td>".SEL_adminName."</td>
						<td>$I18N_GUI_showDeleteMegaSEL_adminNameAdminHelpSchoolManagement</td>
					</tr>
					<tr>
						<td class=\"numberBorder\">2</td>
						<td colspan=\"2\" align=\"center\">".BUT_delete."</td>
						<td>$I18N_GUI_showDeleteMegaBUT_deleteAdminHelpSchoolManagement</td>
					</tr>
				</table>
			</div></td>
		<tr></table>
		");
	}
}
?>