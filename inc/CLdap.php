<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for LDAP access.
$*/

class CLdap extends CChecks
{
	const FD_CONFIG_CACHE_FILE = '/m23/var/fusionDirectoryConfigCache.php',
		LDAP_TRIM = " \n\r\t\v\0,";

	protected $serverInfo = NULL,
			$serverName = NULL,
			$ds = NULL,
			$RDN = '',
			$fdConfig = array();





/**
**name CLdap::__construct($serverName)
**description Constructor for new CLdap objects.
**parameter serverName: Name of the LDAP server
**/
	public function __construct($serverName)
	{
		$this->ds = LDAP_connectServer($serverName, $this->serverInfo);
		
		if ($this->ds === false)
		{
			include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");
			$this->addErrorMessage($I18N_LDAPServerConnectionRefused);
			exit(1);
		}
		
		$this->serverName = $serverName;
		$this->loadFusionDirectoryConfigFile();
// 		print_r($this->serverInfo);
// 		print_r($this->fdConfig);
	}





/**
**name CLdap::getFDAdminACLAttributes($login, &$dnUser, &$dnACL, &$aclAttributes)
**description Get information (LDAP attribute, DN for user account and ACL) for creation and deletion of FusionDirectory administrators.
**parameter login: the login name
**parameter dnUser: DN where to store the FD admin account in the LDAP. (DN is written into this variable)
**parameter dnACL: DN where to store the FD admin role in the LDAP. (DN is written into this variable)
**parameter aclAttributes: ACL attribute that encode the admin role. (attribute is written into this variable)
**/
	public function getFDAdminACLAttributes($login, &$dnUser, &$dnACL, &$aclAttributes)
	{
		$dnUser = "uid=$login,ou=people,".$this->getStartRDN();
		$dnACL = "cn=admin,ou=aclroles,".$this->getStartRDN();
		$b64A = LDAP_getBase64UTF8($dnACL, true);
		$b64B = LDAP_getBase64UTF8($dnUser, true);
		$aclAttributes=array();
		$aclAttributes['gosaAclEntry'] = "0:subtree$b64A$b64B";
	}





/**
**name CLdap::createFDAdmin($login, $pwd, $cryptPw = true)
**description Creates a new FusionDirectory administrator.
**parameter login: the login name
**parameter pwd: the unencrypted password
**parameter cryptPw: set to true, if the password should be encrypted or false, if it's already encrypted.
**returns true, if the administrator was created successfully, otherwise false.
**/
	public function createFDAdmin($login, $pwd, $cryptPw = true)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		// Hash the password if wanted
		if ($cryptPw) $pwd = $this->getPasswordHash($pwd);

		// Set the variables $dnUser, $dnACL and $aclAttributes
		$this->getFDAdminACLAttributes($login, $dnUser, $dnACL, $aclAttributes);

		// Create a new user in LDAP
		$ldapAttributes=array();
		$ldapAttributes['objectClass'][0]='inetOrgPerson';
		$ldapAttributes['objectClass'][1]='organizationalPerson';
		$ldapAttributes['objectClass'][2]='person';
		$ldapAttributes['userPassword'][0]=$pwd;
		$ldapAttributes['cn'][0]=$login;
		$ldapAttributes['sn'][0]=$login;
		$ldapAttributes['uid'][0]=$login;
		$ldapAttributes['givenName'][0]="Admin-$login";
		if ($this->ldap_add($dnUser, $ldapAttributes, $I18N_LDAPcouldNotCreateFDAdmin) === false)
			return(false);

		// Add FD admin role to the user
		if ($this->ldap_mod_add($this->getStartRDN(), $aclAttributes, "$I18N_LDAPcouldNotAddACLToFDAdmin: $login") === false)
			return(false);

		MSG_showInfo($I18N_FDAdminCreatedSuccessfully);
		return(true);
	}





/**
**name CLdap::deleteFDAdmin($login)
**description Deletes a FusionDirectory administrator.
**parameter login: the login name
**returns true, if the administrator was deleted successfully, otherwise false.
**/
	public function deleteFDAdmin($login)
	{
		include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

		// Set the variables $dnUser, $dnACL and $aclAttributes
		$this->getFDAdminACLAttributes($login, $dnUser, $dnACL, $aclAttributes);

		// Remove FD admin role from the user
		if ($this->ldap_mod_del($this->getStartRDN(), $aclAttributes, "$I18N_LDAPcouldNotDeleteACLFromFDAdmin: $login") === false)
			return(false);

		// Delete the user from LDAP
		if ($this->ldap_delete($dnUser, $I18N_LDAPcouldNotDeleteFDAdmin) === false)
			return(false);

		MSG_showInfo($I18N_FDAdminDeletedSuccessfully);
		return(true);
	}





/**
**name CLdap::setStartRDN($rdn)
**description Sets the starting container (eg for searches or inserts).
**parameter rdn: Relative DN where to start searching or inserting (eg. ou=people).
**/
	public function setStartRDN($rdn)
	{
		$this->RDN = trim($rdn,CLdap::LDAP_TRIM).',';
	}





/**
**name CLdap::getLDAPServer()
**description Gets the name of the LDAP server.
**returns Name of the used LDAP server.
**/
	public function getLDAPServer()
	{
		return($this->serverName);
	}





/**
**name CLdap::clearStartRDN()
**description Clears the starting container RDN.
**/
	public function clearStartRDN()
	{
		$this->RDN = '';
	}





/**
**name CLdap::getStartRDN($rdn = NULL)
**description Gets the starting container (eg for searches or inserts).
**parameter rdn: Set the RDN to something else but global $this->RDN.
**returns: Relative DN where to start searching or inserting (eg. ou=people).
**/
	protected function getStartRDN($rdn = NULL)
	{
		if (is_null($rdn))
			$rdn = $this->RDN;
		else
			$rdn = trim($rdn,CLdap::LDAP_TRIM).',';
	
		return($rdn.$this->serverInfo['base']);
	}





/**
**name CLdap::search($filter, $attributes = array("*"), $container = '')
**description Searches the LDAP.
**parameter filter: Filter for only handling entries that match.
**parameter attributes: Array with attributes to fetch.
**parameter container: Container (subtree) in the DIT to search for the IDs. If empty the whole database will be searched.
**returns Associative array with the LDAP search result or false in case of an error.
**/
	public function search($filter = '(cn=*)', $attributes = array("*"), $container = '')
	{
		// Add comma, if a container is given to seprarate it from the full DN
		if (isset($container[0])) $container .= ',';

		// Start search
		$sr = @ldap_search($this->ds, "$container".$this->getStartRDN(), $filter, $attributes);

		if (false === $sr) return(false);

		// Get results as associative array
		return(ldap_get_entries($this->ds, $sr));
	}





/**
**name CLdap::dumpContainer($container)
**description Gets the contents of a LDAP container.
**parameter container: Container (subtree) in the DIT to search for the IDs. If empty the whole database will be searched.
**returns Associative array with the dump.
**/
	public function dumpContainer($container)
	{
		$container = trim($container, CLdap::LDAP_TRIM);
	
		return($this->search('(cn=*)', array("*"),$container));
	}





/**
**name CLdap::getNextID($attribute, $container, $filter, $serverMinimumSettingsVar, $amount = 1, $minimumID = NULL)
**description Returns an array with free (posix user or group) IDs or a single free ID from the LDAP database.
**parameter attribute: Name ID attribute (eg. uidnumber)
**parameter container: Container (subtree) in the DIT to search for the IDs. If empty the whole database will be searched.
**parameter filter: Filter for only handling entries that match.
**parameter serverMinimumSettingsVar: The name of the variable that holds the minimum allowed ID value.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free IDs in the given amount or a single free ID as int.
**/
	private function getNextID($attribute, $container, $filter, $serverMinimumSettingsVar, $amount = 1, $minimumID = NULL)
	{
		$freeIDs = array();
	
		// Overwrite the start ID, if parameter was not given?
		if (is_null($minimumID)) $minimumID = $this->serverInfo[$serverMinimumSettingsVar];
		
		// All free IDs with maximum allowed (g/u)ID for Linux: 60000
		for ($i = $minimumID; $i <= 60000; $i++)
			$freeIDs[$i] = $i;
	
		// Search the LDAP
		$info = $this->search($filter, array($attribute), $container);
	
		// Amount of found IDs in LDAP
		$idAmount = $info['count'];
	
		// Get the amount of results
		if ($idAmount > 0)
		{
			// Remove found IDs from the list of free IDs
			for ($i = 0; $i < $idAmount; $i++)
				if (isset($info[$i][$attribute][0]))
					unset($freeIDs[$info[$i][$attribute][0]]);
		}
	
		// Multiple IDs or single ID?
		if ($amount > 1)
			// Give out the free IDs in the needed amount (counting starts from 0 and NOT from the key that is $minimumID)
			return(array_slice($freeIDs, 0, $amount));
		else
			// Return the first ID from the array (key won't be 0, so reset gives the first element)
			return(reset($freeIDs));
	}





/**
**name CLdap::getFreeUserIDs($amount = 1, $minimumID = NULL)
**description Returns an array with free posix group IDs or a single free ID from the LDAP database.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free user IDs in the given amount or a single free user ID as int.
**/
	public function getFreeUserIDs($amount = 1, $minimumID = NULL)
	{
		return($this->getNextID('uidnumber', '' /*'ou=people,'*/, '(uid=*)', 'uidMin', $amount, $minimumID));
	}





/**
**name CLdap::getFreeGroupIDs($amount = 1, $minimumID = NULL)
**description Returns an array with free posix group IDs or a single free ID from the LDAP database.
**parameter amount: Amount of IDs to generate.
**parameter minimumID: If set, this ID will be used as start ID instead of the ID set by server information.
**returns Array with free group IDs in the given amount or a single free group ID as int.
**/
	public function getFreeGroupIDs($amount = 1, $minimumID = NULL)
	{
		return($this->getNextID('gidnumber', '' /*'ou=Group,'*/, '(cn=*)', 'gidMin', $amount, $minimumID));
	}





/**
**name CLdap::simplifyLDAPEntries($info)
**description Simplifies the output of ldap_get_entries.
**parameter info: Output of ldap_get_entries.
**returns Array with simplified output of ldap_get_entries.
**/
	public function simplifyLDAPEntries($info)
	{
		$out = array();
		
		for ($a = 0; $a < $info['count']; $a++)
			for ($b = 0; $b < $info[$a]['count']; $b++)
			{
				$key = $info[$a][$b];
				$val = $info[$a][$key][0];

				// Single value or array?
				if ($info[$a][$key]['count'] == 1)
					$out[$key] = $info[$a][$key][0];
				else
					for ($c = 0; $c < $info[$a][$key]['count']; $c++)
						$out[$key][$c] = $info[$a][$key][$c];
			}

		return($out);
	}





/**
**name CLdap::filterLDAPEntries($info, $filter)
**description Filters the output of ldap_get_entries.
**parameter info: Output of ldap_get_entries.
**parameter filter: Only keys that match the filter will be present in the output.
**returns Nummeric array with entries there the keys from ldap_get_entries match the filter.
**/
	public function filterLDAPEntries($info, $filter)
	{
		$out = array();
		$i = 0;

		for ($a = 0; $a < $info['count']; $a++)
			for ($b = 0; $b < $info[$a]['count']; $b++)
			{
				$key = $info[$a][$b];
				
				// Check, if the key matches the filter
				if ($key != $filter) continue;
				
				$val = $info[$a][$key][0];

				// Single value or array?
				if ($info[$a][$key]['count'] == 1)
					$out[$i++] = $info[$a][$key][0];
				else
					for ($c = 0; $c < $info[$a][$key]['count']; $c++)
						$out[$i++] = $info[$a][$key][$c];
			}

		return($out);
	}





/**
**name CLdap::generateConfigCacheFileFromFusionDirectory()
**description Reads the FusionDirectory configuration from cn=config,ou=fusiondirectory,... and stores it into CLdap::FD_CONFIG_CACHE_FILE.
**/
	private function generateConfigCacheFileFromFusionDirectory()
	{
		$this->fdConfig = $this->dumpContainer('cn=config,ou=fusiondirectory');
		file_put_contents(CLdap::FD_CONFIG_CACHE_FILE, serialize($this->simplifyLDAPEntries($this->fdConfig)));
	}





/**
**name CLdap::loadFusionDirectoryConfigFile()
**description Loads the FusionDirectory configuration from CLdap::FD_CONFIG_CACHE_FILE.
**/
	private function loadFusionDirectoryConfigFile()
	{
		if (file_exists(CLdap::FD_CONFIG_CACHE_FILE))
			$this->fdConfig = (unserialize(file_get_contents(CLdap::FD_CONFIG_CACHE_FILE)));

		if (!is_array($this->fdConfig) || (count($this->fdConfig) < 10))
			$this->generateConfigCacheFileFromFusionDirectory();
	}





/**
**name CLdap::getFullDNByFdRDN($fdConfigVar, $prefix = '', $rdn = NULL)
**description Get a full DN by LDAP's base and a FusionDirectory RDN.
**parameter fdConfigVar: Name of the FusionDirectory config variable holding the RDN.
**parameter prefix: Prefix to add before the DN.
**parameter rdn: Set the RDN to something else but global $this->RDN.
**returns Full DN or false, if the FusionDirectory config variable isn't set.
**/
	protected function getFullDNByFdRDN($fdConfigVar, $prefix = '', $rdn = NULL)
	{
		if (isset($prefix[0]))
			$prefix = trim($prefix, CLdap::LDAP_TRIM).',';
	
		if (isset($this->fdConfig[$fdConfigVar]))
			return($prefix.
				trim($this->fdConfig[$fdConfigVar], CLdap::LDAP_TRIM).','.
				$this->getStartRDN($rdn));
		else
			return(false);
	}





/**
**name CLdap::getKRB_realm()
**description Gets the Kerberos realm.
**returns Kerberos realm or false, if not given.
**/
	protected function getKRB_realm()
	{
		$var = 'KRB_realm';
		return (isset($this->serverInfo[$var]) ? $this->serverInfo[$var] : false);
	}





/**
**name CLdap::getKRB_server()
**description Gets the Kerberos server.
**returns Kerberos server or false, if not given.
**/
	protected function getKRB_server()
	{
		$var = 'KRB_server';
		return (isset($this->serverInfo[$var]) ? $this->serverInfo[$var] : false);
	}





/**
**name CLdap::getKRB_adminServer()
**description Gets the Kerberos admin server.
**returns Kerberos admin server or false, if not given.
**/
	protected function getKRB_adminServer()
	{
		$var = 'KRB_adminServer';
		return (isset($this->serverInfo[$var]) ? $this->serverInfo[$var] : false);
	}





/**
**name CLdap::getKRB_defaultDomain()
**description Gets the Kerberos default domain.
**returns Kerberos default domain or false, if not given.
**/
	protected function getKRB_defaultDomain()
	{
		$var = 'KRB_defaultDomain';
		return (isset($this->serverInfo[$var]) ? $this->serverInfo[$var] : false);
		
		/*
			Also possible variables:

			kdcServicePass
			kadminServicePass
		*/
	}





/**
**name CLdap::getPasswordHash($pwd)
**description Hashes a password depending on FusionDirectory's default hash setting via MD5 or SSHA as LDAP/PAM expects it.
**parameter pwd: Password to hash.
**returns Hashed password.
**/
	protected function getPasswordHash($pwd)
	{
		if (isset($this->serverInfo['passwordHashAlgorithm']) && 'ssha' == $this->serverInfo['passwordHashAlgorithm'])
			return(LDAP_getSSHA($pwd));
		else
			return(LDAP_getMD5($pwd));
	}





/**
**name CLdap::ldap_add($dn, $ldapAttributes, $message)
**description Adds an object in the DIT.
**parameter dn: Full DN where to add
**parameter ldapAttributes: Attributes for LDAP object.
**parameter errorMessage: Message to add in case of an error.
**returns Output of ldap_add.
**/
	protected function ldap_add($dn, $ldapAttributes, $errorMessage)
	{
		$ret = @ldap_add($this->ds, $dn, $ldapAttributes);

		if ($ret === false)
			$this->addErrorMessage("$errorMessage ($dn): ".ldap_error($this->ds).'/'.ldap_errno($this->ds).'<br>
			<pre>'.print_r($ldapAttributes,true).'</pre>');

		return($ret);
	}





/**
**name CLdap::ldap_mod_add($dn, $ldapAttributes, $errorMessage)
**description Adds an attributes to an existing object.
**parameter dn: Full DN where to add attributes
**parameter ldapAttributes: Attributes to add to an LDAP object.
**parameter errorMessage: Message to add in case of an error.
**returns Output of ldap_mod_add.
**/
	protected function ldap_mod_add($dn, $ldapAttributes, $errorMessage)
	{
		$ret = @ldap_mod_add($this->ds, $dn, $ldapAttributes);

		if ($ret === false)
			$this->addErrorMessage("$errorMessage ($dn): ".ldap_error($this->ds).'/'.ldap_errno($this->ds).'<br>
			<pre>'.print_r($ldapAttributes,true).'</pre>');

		return($ret);
	}





/**
**name CLdap::ldap_mod_del($dn, $ldapAttributes, $errorMessage)
**description Removes attribute(s) from an existing object.
**parameter dn: Full DN where to add attributes
**parameter ldapAttributes: Attributes to remove from an LDAP object.
**parameter errorMessage: Message to add in case of an error.
**returns Output of ldap_mod_del.
**/
	protected function ldap_mod_del($dn, $ldapAttributes, $errorMessage)
	{
		$ret = @ldap_mod_del($this->ds, $dn, $ldapAttributes);

		if ($ret === false)
			$this->addErrorMessage("$errorMessage ($dn): ".ldap_error($this->ds).'/'.ldap_errno($this->ds).'<br>
			<pre>'.print_r($ldapAttributes,true).'</pre>');

		return($ret);
	}





/**
**name CLdap::ldap_delete($dn, $errorMessage)
**description Removes an object from the DIT.
**parameter dn: Full DN where to add attributes
**parameter errorMessage: Message to add in case of an error.
**returns Output of ldap_delete.
**/
	protected function ldap_delete($dn, $errorMessage)
	{
		$ret = @ldap_delete($this->ds, $dn);

		if ($ret === false)
			$this->addErrorMessage("$errorMessage ($dn): ".ldap_error($this->ds).'/'.ldap_errno($this->ds));

		return($ret);
	}





/**
**name CLdap::ldap_rename($oldDN, $newRND , $newParent, $errorMessage)
**description Renames or moves a leaf in the DIT.
**parameter oldDN: Full DN where the leaf is currently stored
**parameter newRND: New (or same) RDN where the leave should be moved/renamed to.
**parameter newParent: New parent DN (eg ou=people,dc=... without "uid=$account") where the leave should be moved to.
**parameter errorMessage: Message to add in case of an error.
**returns Output of ldap_delete.
**/
	protected function ldap_rename($oldDN, $newRND, $newParent, $errorMessage)
	{
		$ret = @ldap_rename ($this->ds, $oldDN, $newRND , $newParent, true);

		if ($ret === false)
			$this->addErrorMessage("$errorMessage ($oldDN => $newRND,$newParent): ".ldap_error($this->ds).'/'.ldap_errno($this->ds));

		return($ret);
	}




/**
**name CLdap::get2ndLDAPServer()
**description Gets the name of the school's LDAP server.
**returns Name of the used school LDAP server.
**/
	public static function get2ndLDAPServer()
	{
		$ldapServers = LDAP_listServers();
		unset($ldapServers['m23-LDAP']);
		return(reset($ldapServers));
	}





/**
**name CLdap::__destruct()
**description Destructor for a CLdap object.
**/
	function __destruct()
	{
		// Show messages before destructing the object
		$this->showMessages();

		if (!is_bool($this->ds))
			ldap_close($this->ds);
	}
}

?>
