<?php

/*$mdocInfo
 Author: Hauke Goos-Habermann (HHabermann@pc-kiel.de)
 Description: Class for visualisation of partitioning and formating.
$*/

class CFDiscAutomatic extends CFDiskGUI
{



/**
**name CFDiscAutomatic::__construct($in)
**description Constructor for new CFDiscAutomatic objects. The object holds all information about the partitioning (of a client and loads the values from the DB).
**parameter in: Name of an existing client (to load) or data of an empty disk.
**/
	public function __construct($in)
	{
		parent::__construct($in);
	}





/**
**name CFDiscAutomatic::__destruct()
**description Destructor for a CFDiscAutomatic object. Before the object is removed from the RAM, all settings are written to the DB.
**/
	function __destruct()
	{
		parent::__destruct();
	}





/**
**name CFDiscAutomatic::getBiggestDrive()
**description Get the drive with the highest capacity.
**returns Name of the drive with the highest capacity.
**/
	protected function getBiggestDrive()
	{
		$list = array();

		// Get a list with all drives as keys and their sizes as values
		for ($vDisk=0; $vDisk < $this->getDiskAmount(); $vDisk++)
		{
			// Get the device string of the current disk
			$diskDev = $this->getDiskDev($vDisk);

			// Add the disk (e.g. /dev/hda) and add size (if choosen only)
			$list[$diskDev] = $this->getDiskSize($vDisk);
		}

		// Sort for size (biggest first)
		arsort($list);

		// Return the drive with the biggest size
		return(array_key_first($list));
	}





/**
**name CFDiscAutomatic::createAutoPartitionFormatJob()
**description Generates a automatic partition and formating job for the drive with the highest capacity.
**/
	public function createAutoPartitionFormatJob()
	{
		$instDisk = $this->getBiggestDrive();
	
		$this->autoPartitionDisk($instDisk);
		$this->setMBRPartDev($instDisk);

		// Save eg. instPart and swapPart to DB
		$this->save();
		$this->finalChecksAndRealPartitionAndFormatStart();
	}
}

?>