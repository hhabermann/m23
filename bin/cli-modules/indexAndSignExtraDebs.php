<?php
/*
Description: Create Packages* and signs the Release file in the extraDebs directory as Release.gpg and InRelease.
*/

function run($argc, $argv)
{
	// Create Packages*
	PKGBUILDER_tar2deb(false);
	// Signs the Release file in the extraDebs directory as Release.gpg and InRelease.
	PKGBUILDER_signExtraDebsRelease();
}
?>

