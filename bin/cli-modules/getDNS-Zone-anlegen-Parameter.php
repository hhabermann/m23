<?php
/*
Description: Get the parameters for DNS-Zone-anlegen.sh
*/

function run($argc, $argv)
{
	foreach (CSchool::readSchoolInfoFile(NULL) as $school => $info)
	{
// 		print_r2($info);

		$ranges = array('LAN_MNT', 'LAN_EDU', 'WLAN_EDU');

		$description = I18N_replaceUmlaute($info['description']);

		$cmd = "\n./DNS-Zone-anlegen.sh \"$description\" \"$school\" ";

		foreach($ranges as $range)
		{
		
			$min = $info[$range]['min'];
			$max = $info[$range]['max'];
			$netmask = $info[$range]['netmask'];
			$gateway = $info[$range]['gateway'];
			$dns1 = $info[$range]['dns1'];
			$dns2 = $info[$range]['dns2'];

			$cmd .= " \"$min\" \"$max\" \"$netmask\" \"$gateway\" \"$dns1\"  \"$dns2\"";
		}

		$cmd .= "\n";
		echo("$cmd");
	}
}
?>
