<?php
/*
Description: Adds a LDAP server to the phpLDAPAdmin configuration
Parameter: How the LDAP server should be called
Parameter: IP or hostname of the LDAP server
Parameter: Base DN (e.g. dc=m23, dc=de)
Parameter: Unencrypted password for the admin
Parameter: Port of the LDAP server
Parameter: minUidNumber: Minimum user ID.
Parameter: minGidNumber: Minimum group ID.

Parameter (optional): FusionDirectory admin password

If given, all of these must be set
	Parameter (optional): Password for the kerberos service account.
	Parameter (optional): Password for the kerberos admin account.
	Parameter (optional): Kerberos realm.
	Parameter (optional): Kerberos server.
	Parameter (optional): Kerberos admin server.
	Parameter (optional): Kerberos default domain.
**/

function run($argc, $argv)
{
	// Array with additional variables and values
	$additionalA = array();

	$name = $argv[2];
	$host = $argv[3];
	$base = $argv[4];
	$pwd = $argv[5];
	$port = $argv[6];


	// Password for FusionDirectory
	if (isset($argv[7]))
		SERVER_setFusionDirectoryPassword($argv[7]);


	// Kerberos parameter
	if (isset($argv[13]))
	{
		$additionalA = array('kdcServicePass' => $argv[8], 'kadminServicePass' => $argv[9], 'KRB_realm' => $argv[10], 'KRB_server' => $argv[11], 'KRB_adminServer' => $argv[12], 'KRB_defaultDomain' => $argv[13]);
	}

	LDAP_addServerTophpLdapAdmin($name,$host,$base,$pwd,$port,1000,1000,'ssha',$additionalA);
}

?>