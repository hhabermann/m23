<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("ddclient");

$elem["ddclient/service"]["type"]="select";
$elem["ddclient/service"]["choices"][1]="${choices}";
$elem["ddclient/service"]["choicesde"][1]="${choices}";
$elem["ddclient/service"]["choicesfr"][1]="${choices}";
$elem["ddclient/service"]["description"]="Dynamic DNS service provider:
 Dynamic DNS service you are using. If the service you use is not
 listed, choose \"other\".
";
$elem["ddclient/service"]["descriptionde"]="Anbieter des dynamischen DNS-Dienstes:
 Dynamischer DNS-Dienst, den Sie verwenden. Falls der von Ihnen verwandte Dienst nicht aufgeführt ist, wählen Sie »anderer«.
";
$elem["ddclient/service"]["descriptionfr"]="Fournisseur de service de DNS dynamique :
 Service de DNS dynamique que vous utilisez. Si celui-ci n'est pas affiché ici, veuillez choisir « Autre ».
";
$elem["ddclient/service"]["default"]="no-ip.com";
$elem["ddclient/server"]["type"]="string";
$elem["ddclient/server"]["description"]="Dynamic DNS server (blank for default):
 Server providing the dynamic DNS service (example:
 members.dyndns.org). Leave blank to use the default for the
 \"${protocol}\" protocol.
";
$elem["ddclient/server"]["descriptionde"]="Dynamischer DNS-Server (leer lassen für Vorgabe):
 Server, der den dynamischen DNS-Dienst bereitstellt (Beispiel: members.dyndns.org). Lassen Sie dies leer, um die Vorgabe für das Protokoll »${protocol}« zu verwenden.
";
$elem["ddclient/server"]["descriptionfr"]="Serveur de DNS dynamique (vide pour le choix par défaut) :
 Serveur qui fournit le DNS dynamique (par exemple : members.dyndns.org). Veuillez laisser vide pour utiliser le protocole « ${protocol} » par défaut.
";
$elem["ddclient/server"]["default"]="";
$elem["ddclient/protocol"]["type"]="select";
$elem["ddclient/protocol"]["choices"][1]="changeip";
$elem["ddclient/protocol"]["choices"][2]="cloudflare";
$elem["ddclient/protocol"]["choices"][3]="concont";
$elem["ddclient/protocol"]["choices"][4]="dnsmadeeasy";
$elem["ddclient/protocol"]["choices"][5]="dnspark";
$elem["ddclient/protocol"]["choices"][6]="dondominio";
$elem["ddclient/protocol"]["choices"][7]="dslreports1";
$elem["ddclient/protocol"]["choices"][8]="dtdns";
$elem["ddclient/protocol"]["choices"][9]="duckdns";
$elem["ddclient/protocol"]["choices"][10]="dyndns1";
$elem["ddclient/protocol"]["choices"][11]="dyndns2";
$elem["ddclient/protocol"]["choices"][12]="easydns";
$elem["ddclient/protocol"]["choices"][13]="freedns";
$elem["ddclient/protocol"]["choices"][14]="freemyip";
$elem["ddclient/protocol"]["choices"][15]="googledomains";
$elem["ddclient/protocol"]["choices"][16]="hammernode1";
$elem["ddclient/protocol"]["choices"][17]="namecheap";
$elem["ddclient/protocol"]["choices"][18]="nfsn";
$elem["ddclient/protocol"]["choices"][19]="noip";
$elem["ddclient/protocol"]["choices"][20]="nsupdate";
$elem["ddclient/protocol"]["choices"][21]="sitelutions";
$elem["ddclient/protocol"]["choices"][22]="woima";
$elem["ddclient/protocol"]["choices"][23]="yandex";
$elem["ddclient/protocol"]["choices"][24]="zoneedit1";
$elem["ddclient/protocol"]["choicesde"][1]="changeip";
$elem["ddclient/protocol"]["choicesde"][2]="cloudflare";
$elem["ddclient/protocol"]["choicesde"][3]="concont";
$elem["ddclient/protocol"]["choicesde"][4]="dnsmadeeasy";
$elem["ddclient/protocol"]["choicesde"][5]="dnspark";
$elem["ddclient/protocol"]["choicesde"][6]="dondominio";
$elem["ddclient/protocol"]["choicesde"][7]="dslreports1";
$elem["ddclient/protocol"]["choicesde"][8]="dtdns";
$elem["ddclient/protocol"]["choicesde"][9]="duckdns";
$elem["ddclient/protocol"]["choicesde"][10]="dyndns1";
$elem["ddclient/protocol"]["choicesde"][11]="dyndns2";
$elem["ddclient/protocol"]["choicesde"][12]="easydns";
$elem["ddclient/protocol"]["choicesde"][13]="freedns";
$elem["ddclient/protocol"]["choicesde"][14]="freemyip";
$elem["ddclient/protocol"]["choicesde"][15]="googledomains";
$elem["ddclient/protocol"]["choicesde"][16]="hammernode1";
$elem["ddclient/protocol"]["choicesde"][17]="namecheap";
$elem["ddclient/protocol"]["choicesde"][18]="nfsn";
$elem["ddclient/protocol"]["choicesde"][19]="noip";
$elem["ddclient/protocol"]["choicesde"][20]="nsupdate";
$elem["ddclient/protocol"]["choicesde"][21]="sitelutions";
$elem["ddclient/protocol"]["choicesde"][22]="woima";
$elem["ddclient/protocol"]["choicesde"][23]="yandex";
$elem["ddclient/protocol"]["choicesde"][24]="zoneedit1";
$elem["ddclient/protocol"]["choicesfr"][1]="changeip";
$elem["ddclient/protocol"]["choicesfr"][2]="cloudflare";
$elem["ddclient/protocol"]["choicesfr"][3]="concont";
$elem["ddclient/protocol"]["choicesfr"][4]="dnsmadeeasy";
$elem["ddclient/protocol"]["choicesfr"][5]="dnspark";
$elem["ddclient/protocol"]["choicesfr"][6]="dondominio";
$elem["ddclient/protocol"]["choicesfr"][7]="dslreports1";
$elem["ddclient/protocol"]["choicesfr"][8]="dtdns";
$elem["ddclient/protocol"]["choicesfr"][9]="duckdns";
$elem["ddclient/protocol"]["choicesfr"][10]="dyndns1";
$elem["ddclient/protocol"]["choicesfr"][11]="dyndns2";
$elem["ddclient/protocol"]["choicesfr"][12]="easydns";
$elem["ddclient/protocol"]["choicesfr"][13]="freedns";
$elem["ddclient/protocol"]["choicesfr"][14]="freemyip";
$elem["ddclient/protocol"]["choicesfr"][15]="googledomains";
$elem["ddclient/protocol"]["choicesfr"][16]="hammernode1";
$elem["ddclient/protocol"]["choicesfr"][17]="namecheap";
$elem["ddclient/protocol"]["choicesfr"][18]="nfsn";
$elem["ddclient/protocol"]["choicesfr"][19]="noip";
$elem["ddclient/protocol"]["choicesfr"][20]="nsupdate";
$elem["ddclient/protocol"]["choicesfr"][21]="sitelutions";
$elem["ddclient/protocol"]["choicesfr"][22]="woima";
$elem["ddclient/protocol"]["choicesfr"][23]="yandex";
$elem["ddclient/protocol"]["choicesfr"][24]="zoneedit1";
$elem["ddclient/protocol"]["description"]="Dynamic DNS update protocol:
 Dynamic DNS update protocol used by your dynamic DNS service
 provider. If the protocol your service uses is not listed, select
 \"other\".
";
$elem["ddclient/protocol"]["descriptionde"]="Protokoll für die dynamische DNS-Aktualisierung:
 Protokoll für die dynamische DNS-Aktualisierung, das von Ihrem Anbieter für dynamischen DNS-Dienst verwandt wird. Falls das von Ihrem Dienst verwandte Protokoll nicht aufgeführt ist, wählen Sie »anderer«.
";
$elem["ddclient/protocol"]["descriptionfr"]="Protocole de mise à jour du DNS dynamique :
 Protocole de mise à jour du DNS dynamique utilisé par le fournisseur du service. Si le protocole utilisé par votre service n'est pas affiché, veuillez sélectionner « Autre ».
";
$elem["ddclient/protocol"]["default"]="dyndns2";
$elem["ddclient/protocol-other"]["type"]="string";
$elem["ddclient/protocol-other"]["description"]="Dynamic DNS update protocol:
 The name of the dynamic DNS update protocol used by your dynamic DNS
 service provider.
";
$elem["ddclient/protocol-other"]["descriptionde"]="Protokoll für die dynamische DNS-Aktualisierung:
 Der Name des Protokolls für die dynamische DNS-Aktualisierung, das vom Anbieter Ihres des dynamischen DNS-Dienstes verwandt wird.
";
$elem["ddclient/protocol-other"]["descriptionfr"]="Protocole de mise à jour du DNS dynamique :
 Nom du protocole de mise à jour du DNS dynamique utilisé par le fournisseur du service.
";
$elem["ddclient/protocol-other"]["default"]="";
$elem["ddclient/proxy"]["type"]="string";
$elem["ddclient/proxy"]["description"]="Optional HTTP proxy:
 HTTP proxy in the form http://proxy.example.com or
 https://proxy.example.com. Proxy authentication is not supported.
 Leave blank if you do not use an HTTP proxy.
";
$elem["ddclient/proxy"]["descriptionde"]="Optionaler HTTP-Proxy:
 HTTP-Proxy in der Form http://proxy.example.com oder https://proxy.example.com. Proxy-Authentifizierung wird nicht unterstützt. Lassen Sie dies leer, falls Sie keinen HTTP-Proxy verwenden.
";
$elem["ddclient/proxy"]["descriptionfr"]="Mandataire HTTP facultatif :
 Mandataire HTTP sous la forme http://proxy.example.com ou https://proxy.example.com. L'authentification du mandataire n'est pas prise en charge. Veuillez laisser vide si vous n'utilisez pas de mandataire HTTP.
";
$elem["ddclient/proxy"]["default"]="";
$elem["ddclient/names"]["type"]="string";
$elem["ddclient/names"]["description"]="Hosts to update (comma-separated):
 Comma-separated list of fully qualified domain names to update (for
 instance, \"myname.dyndns.org\" with only one host, or
 \"myname1.dyndns.org,myname2.dyndns.org\" for two hosts).
";
$elem["ddclient/names"]["descriptionde"]="Zu aktualisierende Rechner (Kommata-getrennt):
 Kommata-getrennte Liste der zu aktualisierenden, vollständigen Domainnamen (zum Beispiel »meinname.dyndns.org« mit nur einem Rechner oder »meinname1.dyndns.org,meinname2.dyndns.org« für zwei Rechner).
";
$elem["ddclient/names"]["descriptionfr"]="Hôtes à mettre à jour (séparés par des virgules) :
 Liste, séparée par des virgules, des noms de domaine complètement qualifiés à mettre à jour (par exemple « myname.dyndns.org » pour un nom unique ou « myname1.dyndns.org,myname2.dyndns.org » pour deux noms).
";
$elem["ddclient/names"]["default"]="";
$elem["ddclient/username"]["type"]="string";
$elem["ddclient/username"]["description"]="Username:
 Username (or other type of account identifer) to use with the dynamic
 DNS service.
";
$elem["ddclient/username"]["descriptionde"]="Benutzername:
 Benutzername (oder eine andere Art von Kontokennzeichner), der mit dem dynamischen DNS-Dienst verwandt werden soll.
";
$elem["ddclient/username"]["descriptionfr"]="Identifiant :
 Nom d'utilisateur (ou tout type d'identifiant de compte) qui sera utilisé pour la connexion au serveur de DNS dynamique.
";
$elem["ddclient/username"]["default"]="";
$elem["ddclient/password"]["type"]="password";
$elem["ddclient/password"]["description"]="Password:
 Password, API key, or token to use with the dynamic DNS service.
";
$elem["ddclient/password"]["descriptionde"]="Passwort:
 Passwort, API-Schlüssel oder Token, der mit dem dynamischen DNS-Dienst verwandt werden soll.
";
$elem["ddclient/password"]["descriptionfr"]="Mot de passe :
 Mot de passe, clé API ou jeton qui sera utilisé pour la connexion au serveur de DNS dynamique.
";
$elem["ddclient/password"]["default"]="";
$elem["ddclient/password-repeat"]["type"]="password";
$elem["ddclient/password-repeat"]["description"]="Re-enter password:
 Password, API key, or token entered again to ensure it was entered
 correctly.
";
$elem["ddclient/password-repeat"]["descriptionde"]="Passwortwiederholung:
 Erneute Eingabe von Passwort, API-Schlüssel oder Token, um sicherzustellen, dass die Eingabe korrekt war.
";
$elem["ddclient/password-repeat"]["descriptionfr"]="Confirmation du mot de passe :
 Veuillez entrer une seconde fois le mot de passe, la clé de l'API ou le jeton pour s'assurer qu'il a été saisi correctement.
";
$elem["ddclient/password-repeat"]["default"]="";
$elem["ddclient/password-mismatch"]["type"]="error";
$elem["ddclient/password-mismatch"]["description"]="Passwords do not match
 The two passwords you entered were not the same. Please try again.
";
$elem["ddclient/password-mismatch"]["descriptionde"]="Passwörter stimmen nicht überein
 Sie haben zwei verschiedene Passwörter eingegeben. Bitte versuchen Sie es noch einmal.
";
$elem["ddclient/password-mismatch"]["descriptionfr"]="Mots de passe différents
 Le mot de passe et sa confirmation ne sont pas identiques. Veuillez recommencer.
";
$elem["ddclient/password-mismatch"]["default"]="";
$elem["ddclient/method"]["type"]="select";
$elem["ddclient/method"]["choices"][1]="Web-based IP discovery service";
$elem["ddclient/method"]["choicesde"][1]="Web-basierter IP-Ermittlungsdienst";
$elem["ddclient/method"]["choicesfr"][1]="Service de découverte d'IP basée sur le web";
$elem["ddclient/method"]["description"]="IP address discovery method:
 The method ddclient uses to determine your current IP address.
 Your options:
 .
 Web-based IP discovery service: Periodically visit a web page that
 shows your IP address. You probably want this option if your computer
 is connected to the Internet via a Network Address Translation (NAT)
 device such as a typical consumer router.
 .
 Network interface: Use the IP address assigned to your computer's
 network interface (such as an Ethernet adapter or PPP connection).
 You probably want this option if your computer connects directly to
 the Internet (your connection does not go through a NAT device).
";
$elem["ddclient/method"]["descriptionde"]="IP-Adressen-Ermittlungsmethode:
 Die Methode, die Ddclient zur Ermittlung Ihrer aktuellen IP-Adresse verwendet. Ihre Optionen:
 .
 Web-basierter IP-Ermittlungsdienst: Besucht periodisch eine Webseite, die Ihre IP-Adresse anzeigt. Dieser Option ist wahrscheinlich die richtige, wenn Ihr Computer über ein Gerät zur Netzwerk-Adressen-Übersetzung (NAT) mit dem Internet verbunden ist, wie bei einem typischen Endverbraucher-Router.
 .
 Netzwerkschnittstelle: Verwendet die IP-Adresse, die der Netzwerkschnittstelle (wie einem Ethernet-Adapter oder einer PPP-Verbindung) Ihres Computers zugewiesen ist. Diese Option ist wahrscheinlich die richtige, falls sich Ihr Computer direkt mit dem Internet verbindet (Ihre Verbindung läuft nicht über ein NAT-Gerät).
";
$elem["ddclient/method"]["descriptionfr"]="Méthode de découverte d'adresse IP :
 Méthode utilisée par ddclient pour déterminer votre adresse IP actuelle. Vos options sont :
 .
 Service de découverte d'IP basée sur le web : visite régulière d'une page web qui affiche votre adresse IP. Ce sera probablement votre choix si votre ordinateur est connecté à Internet par un périphérique de traduction d'adresse réseau (NAT) tel qu'un routeur de type grand public.
 .
 Interface réseau : utilisation de l'adresse IP assignée à l'interface réseau de votre ordinateur (tel qu'un adaptateur Ethernet ou une connexion PPP). Ce sera probablement votre choix si votre ordinateur se connecte directement à Internet (votre connexion ne passe pas par un périphérique NAT).
";
$elem["ddclient/method"]["default"]="Web-based IP discovery service";
$elem["ddclient/web"]["type"]="select";
$elem["ddclient/web"]["choices"][1]="${choices}";
$elem["ddclient/web"]["choicesde"][1]="${choices}";
$elem["ddclient/web"]["choicesfr"][1]="${choices}";
$elem["ddclient/web"]["description"]="IP discovery service:
 The web-based IP discovery service you would like ddclient to use to
 determine your current IP address.
";
$elem["ddclient/web"]["descriptionde"]="IP-Ermittlungsdienst:
 Der Web-basierte IP-Ermittlungsdienst, den Ddclient verwenden soll, um Ihre aktuelle IP-Adresse zu ermitteln.
";
$elem["ddclient/web"]["descriptionfr"]="Service de découverte d'IP :
 Service de découverte d'IP dont vous souhaitez que ddclient se serve pour déterminer votre adresse IP actuelle.
";
$elem["ddclient/web"]["default"]="";
$elem["ddclient/web-url"]["type"]="string";
$elem["ddclient/web-url"]["description"]="IP discovery service URL:
 URL to a web page that returns your IP address.
";
$elem["ddclient/web-url"]["descriptionde"]="URL des IP-Ermittlungsdienstes:
 URL zu einer Web-Seite, die Ihre IP-Adresse zurückliefert.
";
$elem["ddclient/web-url"]["descriptionfr"]="URL du service découverte d'IP :
 URL de la page web qui renvoie votre adresse IP.
";
$elem["ddclient/web-url"]["default"]="";
$elem["ddclient/interface"]["type"]="string";
$elem["ddclient/interface"]["description"]="Network interface:
 The name of the network interface (e.g., eth0, wlan0, ppp0) that
 ddclient will look at to determine the current IP address.
";
$elem["ddclient/interface"]["descriptionde"]="Netzwerkschnittstelle:
 Der Name der Netzwerkschnittstelle (z.B. eth0, wlan0, ppp0), die Ddclient sich anschauen wird, um die aktuelle IP-Adresse zu ermitteln.
";
$elem["ddclient/interface"]["descriptionfr"]="Interface réseau :
 Nom de l'interface réseau (par exemple eth0, wlan0, ppp0) que ddclient examinera pour déterminer l'adresse IP en cours.
";
$elem["ddclient/interface"]["default"]="";
$elem["ddclient/run_mode"]["type"]="select";
$elem["ddclient/run_mode"]["choices"][1]="As a daemon";
$elem["ddclient/run_mode"]["choicesde"][1]="Als Daemon";
$elem["ddclient/run_mode"]["choicesfr"][1]="En tant que démon";
$elem["ddclient/run_mode"]["description"]="How to run ddclient:
 The ddclient run mode. Your options:
 .
 As a daemon: ddclient runs in the background periodically checking to
 see if the IP address has changed.
 .
 On PPP connect: Each time you connect via PPP ddclient will start,
 update the IP address, and exit.
";
$elem["ddclient/run_mode"]["descriptionde"]="Wie Ddclient ausgeführt werden soll:
 Der Ddclient-Ausführungsmodus. Ihre Optionen:
 .
 Als Daemon: Ddclient läuft im Hintergrund und überprüft periodisch, ob sich die IP-Adresse geändert hat.
 .
 Bei PPP-Aufbau: Jedes Mal, wenn Sie sich über PPP verbinden, wird DDclient starten, die IP-Adresse aktualisieren und sich beenden.
";
$elem["ddclient/run_mode"]["descriptionfr"]="Comment lancer ddclient :
 Mode de lancement de ddclient. Vos options :
 .
 En tant que démon : ddclient est lancé périodiquement en tâche de fond pour vérifier si l'adresse IP a changé.
 .
 Lors de connexions PPP : chaque fois que vous vous connectez par PPP, ddclient démarre, met à jour l'adresse IP et quitte.
";
$elem["ddclient/run_mode"]["default"]="As a daemon";
$elem["ddclient/daemon_interval"]["type"]="string";
$elem["ddclient/daemon_interval"]["description"]="Time between address checks:
 How long ddclient should wait between IP address checks. Values may
 be given in seconds (e.g., \"300s\"), in minutes (e.g., \"5m\"), in hours
 (e.g., \"7h\") or in days (e.g., \"1d\").
";
$elem["ddclient/daemon_interval"]["descriptionde"]="Zeitdauer zwischen Adressüberprüfungen:
 Die Wartezeit zwischen IP-Adressüberprüfungen. Werte können in Sekunden (z.B. »300s«), Minuten (z.B. »5m«), Stunden (z.B. »7h«) oder Tagen (z.B. »1d«) angegeben werden.
";
$elem["ddclient/daemon_interval"]["descriptionfr"]="Intervalle entre deux vérifications d'adresse :
 Intervalle entre deux vérifications par ddclient de l'adresse IP. Les valeurs peuvent être indiquées en seconde (par exemple « 300s »), en minute (par exemple « 5m »), en heure (par exemple « 7h ») ou en jour (par exemple « 1d »). Utilisateurs francophones, attention à l'abréviation utilisée pour les jours.
";
$elem["ddclient/daemon_interval"]["default"]="5m";
$elem["ddclient/fetchhosts"]["type"]="select";
$elem["ddclient/fetchhosts"]["choices"][1]="From list";
$elem["ddclient/fetchhosts"]["choicesde"][1]="aus der Liste";
$elem["ddclient/fetchhosts"]["choicesfr"][1]="À partit d'une liste";
$elem["ddclient/fetchhosts"]["description"]="How to enter host names:
 How to prompt you for the host name(s) that ddclient will update.
 .
 If you choose \"From list\", this program will attempt to look up the
 host names that are registered with your DynDNS account. You will
 then select hosts from that list.
 .
 If you choose \"Manually\", you will have to type in the host name(s).
";
$elem["ddclient/fetchhosts"]["descriptionde"]="Wie der Rechnername eingegeben werden soll:
 Wie sollen Sie nach dem oder den Rechername(n) gefragt werden, die Ddclient aktualisieren wird.
 .
 Falls Sie »aus der Liste« auswählen, wird dieses Programm versuchen, die Rechnernamen nachzuschlagen, die mit Ihrem DynDNS-Konto registriert sind. Sie können dann die Rechner aus der Liste auswählen.
 .
 Falls Sie »manuell« auswählen, müssen Sie den oder die Rechnername(n) eingeben.
";
$elem["ddclient/fetchhosts"]["descriptionfr"]="Comment entrer les noms d'hôte :
 Comment demander le(s) nom(s) d'hôte que ddclient mettra à jour.
 .
 Si vous choisissez « À partir d'une liste », ce programme tentera de consulter les noms d'hôte qui sont enregistrés avec votre compte DynDNS. Vous choisirez alors les hôtes à partir de cette liste.
 .
 Si vous choisissez « Manuellement », vous devrez entrer le(s) nom(s) d'hôte.
";
$elem["ddclient/fetchhosts"]["default"]="From list";
$elem["ddclient/hostslist"]["type"]="multiselect";
$elem["ddclient/hostslist"]["description"]="Hosts to update:
 The host name(s) to keep updated with your current IP address. (This
 list of host names was downloaded from your DynDNS account.)
";
$elem["ddclient/hostslist"]["descriptionde"]="Zu aktualisierende Rechnernamen:
 Den oder die Rechnername(n), die mit Ihrer aktuellen IP-Adresse aktuell zu halten sind. (Diese Liste von Rechnernamen wurde von Ihrem DynDNS-Konto heruntergeladen.)
";
$elem["ddclient/hostslist"]["descriptionfr"]="Hôtes à tenir à jour :
 Nom(s) d'hôte à tenir à jour avec votre adresse IP actuelle. (Cette liste de noms d'hôte est téléchargée à partir de votre compte DynDNS.)
";
$elem["ddclient/hostslist"]["default"]="";
$elem["ddclient/blankhostslist"]["type"]="error";
$elem["ddclient/blankhostslist"]["description"]="Empty host list
 The list of host names managed under your account is empty when retrieved
 from the dynamic DNS service website.
 .
 You may have provided an incorrect username or password, or the online account
 may have no host names configured.
 .
 Please check your account to be sure you have host names configured, then run
 \"dpkg-reconfigure ddclient\" to input your username and password again.
";
$elem["ddclient/blankhostslist"]["descriptionde"]="Leere Rechnerliste
 Die Liste der mit Ihrem Konto verwalteten Rechnernamen ist leer, wenn sie von der Website des dynamischen DNS-Dienstes heruntergeladen wird.
 .
 Es könnte sein, dass Sie keinen korrekten Benutzernamen oder kein korrektes Passwort angegeben haben oder im Online-Konto keine Rechnernamen konfiguriert worden sind.
 .
 Bitte überprüfen Sie Ihr Konto, um sicherzustellen, dass Sie Rechnernamen konfiguriert haben. Führen Sie danach »dpkg-reconfigure ddclient« aus, um erneut Ihren Benutzernamen und Ihr Passwort einzugeben.
";
$elem["ddclient/blankhostslist"]["descriptionfr"]="Liste d'hôtes vide
 La liste des noms d'hôte gérés avec votre compte, téléchargée sur le site web du service de DNS dynamique, est vide.
 .
 Vous avez peut-être indiqué un identifiant ou un mot de passe non valable, ou bien le compte en ligne n'a peut-être aucun nom d'hôte configuré.
 .
 Veuillez vérifier si des noms d'hôte sont définis avec le compte utilisé, puis utiliser la commande « dpkg-reconfigure ddclient » pour indiquer à nouveau l'identifiant et le mot de passe à utiliser.
";
$elem["ddclient/blankhostslist"]["default"]="";
PKG_OptionPageTail2($elem);
?>
