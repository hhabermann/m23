<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("wgaelic");

$elem["shared/packages-wordlist"]["type"]="text";
$elem["shared/packages-wordlist"]["description"]="Do not translate, for internal use.
 Intended to get a list of owners of this shared template.

";
$elem["shared/packages-wordlist"]["descriptionde"]="";
$elem["shared/packages-wordlist"]["descriptionfr"]="";
$elem["shared/packages-wordlist"]["default"]="";
$elem["wgaelic/languages"]["type"]="text";
$elem["wgaelic/languages"]["description"]="Do not translate, for internal use.
 Intended to contain a list of language keys provided by this
 package. These are the internal untranslated keys.

";
$elem["wgaelic/languages"]["descriptionde"]="";
$elem["wgaelic/languages"]["descriptionfr"]="";
$elem["wgaelic/languages"]["default"]="Gaidhlig (Scots Gaelic)";
$elem["shared/packages-wordlist"]["type"]="text";
$elem["shared/packages-wordlist"]["description"]="Description:

";
$elem["shared/packages-wordlist"]["descriptionde"]="";
$elem["shared/packages-wordlist"]["descriptionfr"]="";
$elem["shared/packages-wordlist"]["default"]="";
$elem["wgaelic/languages"]["type"]="text";
$elem["wgaelic/languages"]["description"]="Description:
";
$elem["wgaelic/languages"]["descriptionde"]="";
$elem["wgaelic/languages"]["descriptionfr"]="";
$elem["wgaelic/languages"]["default"]="Gaidhlig (Scots Gaelic)";
PKG_OptionPageTail2($elem);
?>
