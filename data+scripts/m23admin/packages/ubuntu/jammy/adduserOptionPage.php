<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("adduser");

$elem["adduser/title"]["type"]="title";
$elem["adduser/title"]["description"]="Adduser
";
$elem["adduser/title"]["descriptionde"]="Adduser
";
$elem["adduser/title"]["descriptionfr"]="Adduser
";
$elem["adduser/title"]["default"]="";
$elem["adduser/homedir-permission"]["type"]="boolean";
$elem["adduser/homedir-permission"]["description"]="Do you want system-wide readable home directories?
 By default, users' home directories are readable only by their owners.
 To allow files to be easily shared with other users you might want to
 make them readable by all other users.
 .
 This will only affect home directories of users added from now
 on with the adduser command.
";
$elem["adduser/homedir-permission"]["descriptionde"]="";
$elem["adduser/homedir-permission"]["descriptionfr"]="";
$elem["adduser/homedir-permission"]["default"]="false";
PKG_OptionPageTail2($elem);
?>
