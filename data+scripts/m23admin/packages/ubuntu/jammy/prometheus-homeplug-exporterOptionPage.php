<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("prometheus-homeplug-exporter");

$elem["prometheus-homeplug-exporter/want_cap_net_raw"]["type"]="boolean";
$elem["prometheus-homeplug-exporter/want_cap_net_raw"]["description"]="Enable additional network privileges for PLC communication?
 prometheus-homeplug-exporter requires CAP_NET_RAW capabilities to be able to
 send out raw Ethernet packets to the HomePlug/PLC devices.
 .
 prometheus-homeplug-exporter will not work at all unless this option is
 enabled, or it runs as root.
";
$elem["prometheus-homeplug-exporter/want_cap_net_raw"]["descriptionde"]="Zusätzliche Netzwerkprivilegien für PLC-Kommunikation aktivieren?
 Prometheus-homeplug-exporter benötigt die Capability CAP_NET_RAW, um rohe Ethernet-Pakete an die HomePlug/PLC-Geräte zu senden.
 .
 Prometheus-homeplug-exporter wird nur dann funktionieren, wenn diese Option aktiviert ist oder es unter root ausgeführt wird.
";
$elem["prometheus-homeplug-exporter/want_cap_net_raw"]["descriptionfr"]="Faut-il activer des privilèges réseau supplémentaires pour la communication par CPL ?
 prometheus-homeplug-exporter a besoin de la capacité CAP_NET_RAW pour pouvoir envoyer des paquets Ethernet bruts aux périphériques HomePlug/CPL.
 .
 prometheus-homeplug-exporter ne fonctionnera pas du tout à moins que cette option ne soit activée, ou alors il ne sera exécuté qu'avec les droits du superutilisateur.
";
$elem["prometheus-homeplug-exporter/want_cap_net_raw"]["default"]="true";
PKG_OptionPageTail2($elem);
?>
