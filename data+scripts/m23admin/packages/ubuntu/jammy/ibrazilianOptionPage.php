<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("ibrazilian");

$elem["shared/packages-ispell"]["type"]="text";
$elem["shared/packages-ispell"]["description"]="Do not translate, for internal use.
 Intended to get a list of owners of this shared template.

";
$elem["shared/packages-ispell"]["descriptionde"]="";
$elem["shared/packages-ispell"]["descriptionfr"]="";
$elem["shared/packages-ispell"]["default"]="";
$elem["ibrazilian/languages"]["type"]="text";
$elem["ibrazilian/languages"]["description"]="Do not translate, for internal use.
 Intended to contain a list of language keys provided by this
 package. These are the internal untranslated keys.
";
$elem["ibrazilian/languages"]["descriptionde"]="";
$elem["ibrazilian/languages"]["descriptionfr"]="";
$elem["ibrazilian/languages"]["default"]="portugues brasileiro (Brazilian Portuguese)";
PKG_OptionPageTail2($elem);
?>
