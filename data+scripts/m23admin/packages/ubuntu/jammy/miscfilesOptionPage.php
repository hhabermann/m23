<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("miscfiles");

$elem["shared/packages-wordlist"]["type"]="text";
$elem["shared/packages-wordlist"]["description"]="Do not translate, for internal use.
 Intended to get a list of owners of this shared template.

";
$elem["shared/packages-wordlist"]["descriptionde"]="";
$elem["shared/packages-wordlist"]["descriptionfr"]="";
$elem["shared/packages-wordlist"]["default"]="";
$elem["miscfiles/languages"]["type"]="text";
$elem["miscfiles/languages"]["description"]="Do not translate, for internal use.
 Intended to contain a list of language keys provided by this
 package. These are the internal untranslated keys.
";
$elem["miscfiles/languages"]["descriptionde"]="";
$elem["miscfiles/languages"]["descriptionfr"]="";
$elem["miscfiles/languages"]["default"]="english (Webster's Second International English wordlist)";
PKG_OptionPageTail2($elem);
?>
