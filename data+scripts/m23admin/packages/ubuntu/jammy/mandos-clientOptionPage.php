<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("mandos-client");

$elem["mandos-client/key_id"]["type"]="note";
$elem["mandos-client/key_id"]["description"]="New client option \"${key_id}\" is REQUIRED on server
 A new \"key_id\" client option is REQUIRED in the server's clients.conf
 file, otherwise this computer most likely will not reboot unattended. 
 This option:
 .
  ${key_id}
 .
 must be added (all on one line!) on the Mandos server host, in the file
 /etc/mandos/clients.conf, right before the \"fingerprint\" option for this
 Mandos client.  You must edit that file on that server and add this
 option.
 .
 With GnuTLS 3.6.6, Mandos has been forced to stop using OpenPGP keys as
 TLS session keys.  A new TLS key pair has been generated and will be used
 as identification, but the key ID of the public key needs to be added to
 the server, since this will now be used to identify the client to the
 server.
";
$elem["mandos-client/key_id"]["descriptionde"]="Auf dem Server ist die neue Client-Option »${key_id}« ERFORDERLICH.
 In der Datei clients.conf des Servers ist eine neue  Client-Option »key_id« ERFORDERLICH, andernfalls wird dieser Rechner höchstwahrscheinlich nicht unbeaufsichtigt neu starten. Die Option 
 .
  ${key_id}
 .
 muss (in einer einzigen Zeile!) der Datei /etc/mandos/clients.conf auf dem Mandos-Server kurz vor der Option »fingerprint« für diesen Mandos-Client hinzugefügt werden. Sie müssen diese Datei auf diesem Server bearbeiten und diese Option hinzufügen.
 .
 Mit GnuTLS 3.6.6 wurde erzwungen, dass Mandos die Benutzung von OpenPGP als TLS-Sitzungsschlüssel stoppt. Ein neues TLS-Schlüsselpaar wurde erzeugt und wird zur Identifizierung benutzt, aber die Schlüsselkennung des öffentlichen Schlüssels muss auf diesem Server hinzugefügt werden, da dies nun zur Identifizierung des Clients auf dem Server verwendet wird.
";
$elem["mandos-client/key_id"]["descriptionfr"]="";
$elem["mandos-client/key_id"]["default"]="";
PKG_OptionPageTail2($elem);
?>
