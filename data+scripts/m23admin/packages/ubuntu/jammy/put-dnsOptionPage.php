<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("put-dns");

$elem["put-dns/update-domain"]["type"]="boolean";
$elem["put-dns/update-domain"]["description"]="Automatically update domain in put-dns.conf?
 If enabled put-dns will, on configuration, try to detect a
 valid external domain name on the current system and update
 the put-dns.conf configuration file to use it. If disabled
 then the configuration will not be automatically updated.
";
$elem["put-dns/update-domain"]["descriptionde"]="Domain in put-dns.conf automatisch aktualisieren?
 Falls aktiviert, wird Put-dns während seiner Konfiguration versuchen, einen gültigen externen Domain-Namen auf dem aktuellen System zu erkennen und die Konfigurationsdatei put-dns.conf aktualisieren, diesen zu verwenden. Falls deaktiviert, wird die Konfiguration nicht automatisch aktualisiert.
";
$elem["put-dns/update-domain"]["descriptionfr"]="";
$elem["put-dns/update-domain"]["default"]="false";
PKG_OptionPageTail2($elem);
?>
