<?php
include ("/m23/inc/packages.php");
include ("/m23/inc/checks.php");
include ("/m23/inc/client.php");
include ("/m23/inc/capture.php");

$params = PKG_OptionPageHeader2("phpldapadmin");

$elem["phpldapadmin/ldap-server"]["type"]="string";
$elem["phpldapadmin/ldap-server"]["description"]="LDAP server host address:
 Please enter the host name or the address of the LDAP server you want to
 connect to.
";
$elem["phpldapadmin/ldap-server"]["descriptionde"]="Adresse des LDAP-Servers
 Bitte geben Sie den Namen oder die Adresse des LDAP-Servers ein, mit dem Sie sich verbinden wollen.
";
$elem["phpldapadmin/ldap-server"]["descriptionfr"]="Adresse du serveur LDAP :
 Veuillez indiquer le nom d'hôte ou l'adresse du serveur LDAP auquel vous souhaitez vous connecter.
";
$elem["phpldapadmin/ldap-server"]["default"]="127.0.0.1";
$elem["phpldapadmin/ldap-tls"]["type"]="boolean";
$elem["phpldapadmin/ldap-tls"]["description"]="Enable support for ldaps protocol?
 If your LDAP server supports TLS (Transport Security Layer), you can use
 the ldaps protocol to connect to it.
";
$elem["phpldapadmin/ldap-tls"]["descriptionde"]="Unterstützung für das ldaps-Protokoll aktivieren?
 Falls Ihr LDAP-Server TLS (Transport Security Layer - Transport-Sicherheits-Schicht) unterstützt, können Sie das ldaps-Protokoll verwenden, um sich mit ihm zu verbinden.
";
$elem["phpldapadmin/ldap-tls"]["descriptionfr"]="Faut-il gérer le protocole LDAPS ?
 Si votre serveur LDAP gère la couche de sécurité de transport (TLS : « Transport Security Layer »), vous pouvez utiliser le protocole ldaps pour vous y connecter.
";
$elem["phpldapadmin/ldap-tls"]["default"]="false";
$elem["phpldapadmin/ldap-basedn"]["type"]="string";
$elem["phpldapadmin/ldap-basedn"]["description"]="Distinguished name of the search base:
 Please enter the distinguished name of the LDAP search base. Many sites
 use the components of their domain names for this purpose. For example,
 the domain \"example.com\" would use \"dc=example,dc=com\" as the
 distinguished name of the search base.
";
$elem["phpldapadmin/ldap-basedn"]["descriptionde"]="Eindeutiger Name der Suchbasis:
 Bitte geben Sie den eindeutigen Namen (distinguished name, dn) der Suchbasis ein. Viele Installationen verwenden zu diesem Zweck die Komponenten ihres Domain-Namens. Zum Beispiel würde die Domain »example.com« als eindeutigen Namen der Suchbasis »dc=example,dc=com« verwenden.
";
$elem["phpldapadmin/ldap-basedn"]["descriptionfr"]="Nom distinctif de la base de recherche :
 Veuillez indiquer le nom distinctif de la base de recherche LDAP. De nombreux sites utilisent les parties de leur nom de domaine pour cela. Par exemple, le domaine « exemple.com » pourra utiliser « dc=exemple,dc=com » comme nom distinctif de la base de recherche.
";
$elem["phpldapadmin/ldap-basedn"]["default"]="dc=example,dc=com";
$elem["phpldapadmin/ldap-authtype"]["type"]="select";
$elem["phpldapadmin/ldap-authtype"]["choices"][1]="session";
$elem["phpldapadmin/ldap-authtype"]["choices"][2]="cookie";
$elem["phpldapadmin/ldap-authtype"]["choicesde"][1]="Sitzung";
$elem["phpldapadmin/ldap-authtype"]["choicesde"][2]="Cookie";
$elem["phpldapadmin/ldap-authtype"]["choicesfr"][1]="session";
$elem["phpldapadmin/ldap-authtype"]["choicesfr"][2]="cookie";
$elem["phpldapadmin/ldap-authtype"]["description"]="Type of authentication
 session : You will be prompted for a login dn and a password everytime
           you connect to phpLDAPadmin, and a session variable on the 
           web server will store them. It is more secure so this is the
           default.
 .
 cookie :  You will be prompted for a login dn and a password everytime
           you connect to phpLDAPadmin, and a cookie on your client will
           store them.
 .
 config  : login dn and password are stored in the configuration file,
           so you have not to specify them when you connect to 
           phpLDAPadmin.
";
$elem["phpldapadmin/ldap-authtype"]["descriptionde"]="Art der Authentifizierung
 Sitzung:       Jedes Mal, wenn Sie sich mit phpLDAPadmin verbinden, werden
                Sie nach einem Login-dn und einem Passwort gefragt. Diese
                werden in einer Sitzungsvariablen im Webserver gespeichert.
                Da dies sicherer ist, ist es die Voreinstellung.
 .
 Cookie:        Jedes Mal, wenn Sie sich mit phpLDAPadmin verbinden, werden
                Sie nach einem Login-dn und einem Passwort gefragt. Diese
                werden in einem Cookie in Ihrem Client gespeichert.
 .
 Konfiguration: Login-dn und Passwort sind in der Konfigurationsdatei
                gespeichert. Sie müssen sie nicht angeben, wenn Sie sich
                mit phpLDAPadmin verbinden.
";
$elem["phpldapadmin/ldap-authtype"]["descriptionfr"]="Type d'authentification :
 session : un identifiant dn de connexion et un mot de passe vous seront
           demandés à chaque connexion à phpLDAPadmin et une variable de
           session sera utilisée pour les conserver sur le serveur web.
           Ce choix est le plus sûr et sera donc utilisé par défaut ;
 .
 cookie  : un identifiant dn de connexion et un mot de passe vous
           seront demandés à chaque connexion à phpLDAPadmin. Ils
           seront conservés dans un cookie sur votre navigateur client ;
 .
 config  : l'identifiant dn de connexion et le mot de passe seront
           conservés dans le fichier de configuration afin de ne pas
           avoir à les indiquer pour chaque connexion à phpLDAPadmin.
";
$elem["phpldapadmin/ldap-authtype"]["default"]="session";
$elem["phpldapadmin/ldap-binddn"]["type"]="string";
$elem["phpldapadmin/ldap-binddn"]["description"]="Login dn for the LDAP server:
 Enter the name of the account that will be used to log in to the LDAP
 server. If you chose a form based authentication this will be the
 default login dn. In this case you can also leave it empty, if you do 
 not want a default one.
";
$elem["phpldapadmin/ldap-binddn"]["descriptionde"]="Login-dn für den LDAP-Server:
 Geben Sie den Namen des Kontos ein, das zur Anmeldung an den LDAP-Server verwendet wird. Falls Sie eine formularbasierte Authentifizierung wählten, wird dies der voreingestellte Login-dn sein. In diesem Fall können Sie ihn auch leer lassen, wenn Sie keine Voreinstellung möchten.
";
$elem["phpldapadmin/ldap-binddn"]["descriptionfr"]="Identifiant dn de connexion au serveur LDAP :
 Veuillez indiquer l'identifiant que sera utilisé pour la connexion au serveur LDAP. Si vous choisissez une authentification par formulaires, cet identifiant sera alors la valeur par défaut. Vous pouvez, dans ce cas, également laisser ce champ vide afin qu'aucune valeur par défaut ne soit utilisée.
";
$elem["phpldapadmin/ldap-binddn"]["default"]="cn=admin,dc=example,dc=com";
$elem["phpldapadmin/ldap-bindpw"]["type"]="string";
$elem["phpldapadmin/ldap-bindpw"]["description"]="Login password for the LDAP server:
 Enter the password that will be used to log in to the LDAP server. Note:
 the password will be stored in clear text in config.php, which is not
 world-readable.
";
$elem["phpldapadmin/ldap-bindpw"]["descriptionde"]="Login-Passwort für den LDAP-Server:
 Geben Sie das Passwort zur Anmeldung an den LDAP-Server ein. Hinweis: Das Passwort wird im Klartext in der Datei config.php gespeichert. Diese ist nicht für jeden lesbar.
";
$elem["phpldapadmin/ldap-bindpw"]["descriptionfr"]="Mot de passe pour la connexion au serveur LDAP :
 Veuillez indiquer le mot de passe qui sera utilisé pour la connexion au serveur LDAP. Veuillez noter que ce mot de passe sera conservé en clair dans le fichier config.php, qui ne peut pas être lu par les utilisateurs non privilégiés.
";
$elem["phpldapadmin/ldap-bindpw"]["default"]="secret";
$elem["phpldapadmin/reconfigure-webserver"]["type"]="multiselect";
$elem["phpldapadmin/reconfigure-webserver"]["description"]="Web server(s) which will be reconfigured automatically:
 phpLDAPadmin supports any web server that PHP does, but this automatic
 configuration process only supports Apache2.
";
$elem["phpldapadmin/reconfigure-webserver"]["descriptionde"]="Webserver, der oder die automatisch eingerichtet werden:
 phpLDAPadmin unterstützt jeden Webserver, der von PHP unterstützt wird. Dieser automatische Konfigurationsprozess funktioniert jedoch nur mit Apache2.
";
$elem["phpldapadmin/reconfigure-webserver"]["descriptionfr"]="Serveur web à reconfigurer automatiquement :
 PhpLDAPadmin gère tout serveur web géré par PHP4, mais cette procédure de configuration automatique ne gère actuellement qu'Apache.
";
$elem["phpldapadmin/reconfigure-webserver"]["default"]="apache2";
$elem["phpldapadmin/restart-webserver"]["type"]="boolean";
$elem["phpldapadmin/restart-webserver"]["description"]="Should your webserver(s) be restarted?
 Remember that in order to apply the changes your webserver(s) has/have to
 be restarted. 
";
$elem["phpldapadmin/restart-webserver"]["descriptionde"]="Webserver neu startetn?
 Denken Sie daran, dass Ihre Webserver neu gestartet werden müssen, um die Änderungen wirksam werden zu lassen.
";
$elem["phpldapadmin/restart-webserver"]["descriptionfr"]="Faut-il redémarrer le serveur web ?
 Veuillez noter que pour que les modifications soient prises en compte, votre(vos) serveur(s) web doi(ven)t être redémarré(s).
";
$elem["phpldapadmin/restart-webserver"]["default"]="true";
PKG_OptionPageTail2($elem);
?>
