<?PHP

	// Reset the session, if a mode is set by URL
	if (isset($_GET['mode']))
	{
		// Save last school name from clearing the session.
		$lastSchool = isset($_SESSION['schMan']['schoolName']) ? $_SESSION['schMan']['schoolName'] : false;
		
// 		var_dump($lastSchool);
		
		$_SESSION = array();
		$_SESSION['schMan']['mode'] = (isset($_GET['mode']) ? $_GET['mode'] : $_POST['mode']);
		$_SESSION['schMan']['schoolName'] = $lastSchool;
	}


	// Behaviour by mode
	switch ($_SESSION['schMan']['mode'])
	{
		case 'accountOverview':
			$modeI18N = $I18N_accountOverview;
		break;

		case 'createAccount':
			$modeI18N = $I18N_createAccount;
			$dialogFunction = 'CSchoolManagement::GUI_showSchoolUserCreator';
		break;

		case 'editAccount':
			$modeI18N = $I18N_editAccount;
		break;

		case 'deleteAccount':
			$modeI18N = $I18N_deleteAccount;
		break;

		case 'createMegaAdmin':
			$modeI18N = $I18N_createMegaAdmin;
			$dialogFunction = 'CSchoolManagement::GUI_showCreateMegaAdmin';
		break;

		case 'deleteMegaAdmin':
			$modeI18N = $I18N_deleteMegaAdmin;
			$dialogFunction = 'CSchoolManagement::GUI_showDeleteMegaAdmin';
		break;
		
		
		
		
		
	}

	// Show combined title
	echo("<span class=\"title\">$I18N_schoolManagement ($modeI18N)</span><br><br>");

	HTML_showFormHeader();

	$dialogFunction();

	HTML_setPage('schoolManagement');
	HTML_showFormEnd(false);
?>