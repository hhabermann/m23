<?php
	header('Content-Type: application/json');

	include('/m23/inc/db.php');
	include('/m23/inc/capture.php');
	include('/m23/inc/checks.php');
	include('/m23/inc/html.php');
	include("/m23/inc/sourceslist.php");
	dbConnect();

	$desktops = SRCLST_getDesktopList($_GET['sourcename']);
	sort($desktops);

	echo(json_encode($desktops));
?>