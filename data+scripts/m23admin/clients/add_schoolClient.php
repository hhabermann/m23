<?php HTML_showFormHeader(); 

?>

<span class="title">
<?PHP
	require_once('/m23/inc/schoolConfig.php');

	if (isset($_GET['clearSession']) && ($_GET['clearSession'] == 1))
	{
		$_SESSION = array();
		unset($_SESSION['FDISK_showDiskDefine']);
		$_SESSION['aSC']['step'] = 0;
		unset($_SESSION['aSC']['schoolName']);
		unset($_SESSION['schMan']['schoolName']);
	}
	
	echo($I18N_addSchoolClient);
	$helpPage = 'addSchoolClient';
?>
</span><br><br>



<?php



function showBasicSchoolClient()
{
/*print('<h3>debug</h3>');
	print_r($_SESSION['aSC']);

print('<h3>schoolName2: '.$_SESSION['aSC']['schoolName'].'</h3>');*/
	
	include("/m23/inc/i18n/".$GLOBALS["m23_language"]."/m23base.php");

	$err = '';
	$rowColor = '';

	$_SESSION['preferenceName'] = 'DefaultAddSchoolClientSetting';
	PREF_loadAllPreferenceValues();

// Use network deployment school to figure out the network settings
	// Exit function, if the object could not be created
	try { $CFreeIPAO = new CFreeIPA(); } catch (Exception $e) { return(false); }



// Clientname

	$installParams['serial'] = $installParams['client'] = strtolower(HTML_input("ED_client", false, 20, 64));
	// 	$installParams['serial'] = HTML_input("ED_serial", false, 16);
	$defaultExampleClientName = SCHOOL_CONFIG_defaultExampleClientName;
	$domainList = $CFreeIPAO->getSubdomains($_SESSION['aSC']['schoolName']);

	$_SESSION['aSC']['domain'] = $domain = HTML_selection("SEL_domain", $domainList, SELTYPE_selection);
	$installParams['client'].= '.'.$domain;


// CSchool object
	// Exit function, if the object could not be created
	try { $CDeployNetworkSchoolO = new CSchool(SCHOOL_DEPLOY_NETWORK, $domain); } catch (Exception $e) { return(false); }


// Network settings

	$installParams['ip'] = HTML_input("ED_ip", $CDeployNetworkSchoolO->getNextFreeIP(1), 16, 16);
	$installParams['netmask'] = HTML_input("ED_netmask", $CDeployNetworkSchoolO->getNetmask(), 16, 16);
	$installParams['gateway'] = HTML_input("ED_gateway", $CDeployNetworkSchoolO->getGateway(), 16, 16);

	$dns1dns2 = getDNSServers();
	$installParams['dns1'] = HTML_input("ED_dns1", $CDeployNetworkSchoolO->getDNS(), 16, 16);
	$installParams['dns2'] = HTML_input("ED_dns2", $dns1dns2[0], 16, 16);

	$installParams['mac'] = HTML_input("ED_mac", false, 12, 17, INPUT_TYPE_text, false, 'onChange="sanitizeMAC();"');
	$installOptions['packageProxy'] = getServerIP();
	$installOptions['packagePort'] = 2323;
	$installOptions['netRootPwd'] = DB_genPassword(6);
	$installOptions['nfshomeserver'] = '';
	$installOptions['disableSSLCertCheck'] = '0';


// Use the real school to create groups etc. (just in case)
	// Exit function, if the object could not be created
	try { $CSchoolO = new CSchool($_SESSION['aSC']['schoolName'], $domain); } catch (Exception $e) { return(false); }


// CPU architecture

	// Client architecture: 32 or 64 bits
	$archList = getArchList();
	$installOptions['arch'] = HTML_selection("SEL_arch", $archList, SELTYPE_selection);



// Group

	// List available m23 groups that start with the school name
	$filteredGroups = preg_grep('/^'.$_SESSION['aSC']['schoolName'].'_.*/', GRP_listGroups());
	// 	$groupList = HELPER_array2AssociativeArray($groupList);

	/* Build an associative array
		keys: group name
		values: group name and description
	*/
	$groupList = array();
	foreach ($filteredGroups as $group)
		$groupList[$group] =  "$group - ".GRP_getDescrGroup($group);

	$group = HTML_selection("SEL_group", $groupList, SELTYPE_selection, true, false, false, "", 10);


	// Create Deploy + Transfer group
	if (!GRP_exists(SCHOOL_DEPLOY_GROUP))
		GRP_add(SCHOOL_DEPLOY_GROUP, $I18N_PrepareForTransfer, false);

	// Select the default group and the school's group hidded
	$installParams['newgroup'] = array_merge(array('default' => 'default', SCHOOL_DEPLOY_GROUP => SCHOOL_DEPLOY_GROUP, $_SESSION['aSC']['schoolName'] => $_SESSION['aSC']['schoolName']), $group);



// LDAP and Kerberos
/*
	$installOptions['ldapserver'] = $CDeployNetworkSchoolO->getLDAPServer();
	$installOptions['ldaptype'] = 'read';
	$installOptions['ldapTLS'] = 1;
	// dc=<school short name>, will be put in front of the baseDN, to let PAM search users and groups in the school's container only
	$installOptions['ldapbaseprefix'] = 'dc='.$_SESSION['aSC']['schoolName'].',';

	$installOptions['KRB_server'] = getServerIP();
	$installOptions['KRB_adminServer'] = getServerIP();
	$installOptions['KRB_realm'] = SCHOOL_CONFIG_KRB_realm;
*/

// FreeIPA
	$installOptions['ldaptype'] = 'FreeIPARead';
	$installOptions['ldapserver'] = '';



// User account and passwords

	// Disable adding user account information in the clients table
	$installParams['name'] = NULL;
	$installParams['rootpassword'] = SCHOOL_CONFIG_hashedRootpassword;	// HELPER_grubMd5Crypt('xxx',8)
	$installOptions['addNewLocalLogin'] = 'no';
	$installOptions['disableSudoRootLogin'] = '1';



// Booting

	$bootTypeList = CClient::getNetworkBootTypesArrayForSelection();
	$installParams['dhcpBootimage'] = HTML_selection("SEL_boottype", $bootTypeList, SELTYPE_selection);
	$installOptions['bootloader'] = 'grub';
	$installOptions['kernel'] = 'linux-image-generic';



// Language and time

	$installParams['language'] = 'de';
	$installOptions['timeZone'] = 'Europe/Berlin';
	$installOptions['getSystemtimeByNTP'] = 'yes';


	$installParams['school'] = $_SESSION['aSC']['schoolName'];



// Distribution and desktop

	$defaultSourcename = 'LinuxMint 20.3 Una';
	$installOptions['packageSelection'] = '';
	$installOptions['sourcename'] = $installOptions['packagesource'] = SRCLST_storableSelection('SEL_sourcename', '*', 'packageSelection', $installOptions['packagesource'], $defaultSourcename, 'LinuxMint 20', 'onChange="updateDesktops(\'xxx\',\'xxx\');"');
	unset($_SESSION['preferenceSpace']['packageselection']);
	
	$installOptions['distr'] = SRCLST_getValue($installOptions['sourcename'],"distr");
	$installOptions['release'] = SRCLST_getValue($installOptions['sourcename'],"release");

/*	$installOptions['sourcename'] = 'LinuxMint 20.3 Una';
	$installOptions['packagesource'] = 'LinuxMint 20.3 Una';
	$installOptions['distr'] = 'ubuntu';
	$installOptions['release'] = 'focal';*/

	$defaultDesktop = 'Mint20MateNoWelcomeNoVolumeIcons';
	$installOptions['desktop'] = '';
	SRCLST_storableDesktopsSelection('SEL_desktop', $installOptions['sourcename'], 'desktop', $installOptions['desktop'], $defaultDesktop);

// 	$installOptions['desktop'] = HTML_selection('SEL_desktop', array(), SELTYPE_selection, true, $defaultDesktop, false);
// 	if (empty($installOptions['desktop'])) $installOptions['desktop'] = $defaultDesktop;



// Optional features

	$installOptions['buildPoolFromClientDebs'] = '';
	$installOptions['installX2goserver'] = '0';
	$installOptions['installPrinter'] = 'no';
	$installOptions['deployTransfer'] = 1;
	$enableKioskMode = HTML_checkBox('CB_enableKioskMode', '', false);



// Package selections

	// Filter out selections ??? 'school_kiosk' 'school_default'
	$installOptions['packageSelection'] = '';
	$packageSelection = PKG_storablePackageSelectionsSelection('SEL_packageSelection', 'packageSelection', $installOptions['packageSelection']);
	unset($_SESSION['preferenceSpace']['packageselection']);



	if (HTML_submit('BUT_submit', $I18N_add))
	{
// 		print_r2($_SESSION['aSC']);
// 		print_r2($installParams['newgroup']);
		
// 		print_r2($packageSelection);

		// Transform the client name into a FQDN
// 		$installParams['client'] .= '.'.SCHOOL_CONFIG_KRB_defaultDomain;

// 		$installParams['client'] .= '.'.$CSchoolO->getMainDomain();

		$err .= CSchool::checkNewgroupBeforeClientAdd($_SESSION['aSC']['schoolName'], $installParams['newgroup']);
		
// 		print($err);
		if (strlen($err) == 0)
		{
			// Get the one element containing the profile group
			$profileGroupA = preg_grep('/^'.$_SESSION['aSC']['schoolName'].'_.*/', $installParams['newgroup']);
	
			// The string of the profile group is the first entry
			$profileGroup = array_values($profileGroupA)[0];
	
			// Remove all before the last '_' to get the profile name
			$profileName = preg_replace('/.*_/', '' , $profileGroup);
	
			// Build the package selection name from profile, distribution and release names
			$profilePackageSelection = $installOptions['distr'].'_'.$installOptions['release'].'_'.$profileName;
		}

		CLIENT_setAllOptions($installParams['client'], $installOptions);
		$err .= CLIENT_addClient($installParams, $installOptions, CLIENT_ADD_TYPE_add, false);

		if (strlen($err) == 0)
		{
			MSG_showInfo("<b>$I18N_client_added</b>\n<br><p>$I18N_youCanStartYourm23ClientNow</p>");
			
			$CSchoolO = CSchool::createCSchoolObject($_SESSION['aSC']['schoolName'], $_SESSION['aSC']['domain'], $createErrorMsg);
			if ($CSchoolO === false)
				MSG_showWarning($createErrorMsg);
			
			foreach ($installParams['newgroup'] as $group)
			{
				if ('default' == $group || SCHOOL_DEPLOY_GROUP == $group) continue;
				$group = strtolower($group);
				$CSchoolO->addClientToHostGroup($installParams['client'], $group);
			}

			PKG_addJob($installParams['client'],"m23fdiskAutomaticFormat",PKG_getSpecialPackagePriority("m23fdiskAutomaticFormat"),'');

			// Add profile package selection
			PKG_addPackageSelection($installParams['client'], $profilePackageSelection, "orig", $installOptions['distr']);

			// Add package selection(s) from the HTML selection
			PKG_addPackageSelection($installParams['client'], $packageSelection, "orig", $installOptions['distr']);

			// Add the default package selection
			PKG_addPackageSelection($installParams['client'], 'school_default', "orig", $installOptions['distr']);

			if ($enableKioskMode)
				PKG_addPackageSelection($installParams['client'], 'school_kiosk', "orig", $installOptions['distr']);

			// Add FreeIPA client install package, if FreeIPA is used
			if (isset($installOptions['ldaptype']) && (strpos($installOptions['ldaptype'], 'FreeIPA')!==false))
				PKG_addJob($installParams['client'], "m23FreeIPAClient",PKG_getSpecialPackagePriority("m23FreeIPAClient", $installOptions['distr']),'');

			// Set all jobs from package selections to "waiting"
			PKG_acceptJobs($installParams['client'], true);

			return(true);
		}
		else
			MSG_showError($err);
	}




// 				<tr ".HTML_rowColor($rowColor)."><td>$I18N_clientSerial</td><td>".ED_serial."</td></tr> 
	HTML_showJSSanitizeMAC('ED_mac');
	echo("
	<table align=\"center\">
	<tr>
		<td><div class=\"subtable_shadow\">
			<table align=\"center\" class=\"subtable2\">
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_client_name*</br>$I18N_clientSerial</td><td>".ED_client.".".SEL_domain."</br> ($I18N_eg $defaultExampleClientName)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_ip*</td><td>".ED_ip." ($I18N_eg 192.168.0.5)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_netmask*</td><td>".ED_netmask." ($I18N_eg 255.255.255.0)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_gateway*</td><td>".ED_gateway." ($I18N_eg 192.168.0.1)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>DNS1*</td><td>".ED_dns1." ($I18N_eg 192.168.0.1)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>DNS2</td><td>".ED_dns2."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_mac*</td><td>".ED_mac." ($I18N_eg 009b52a5e121)</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_arch*</td><td>".SEL_arch."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_packageSources*</td><td>".SEL_sourcename."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_userInterface*</td><td>".SEL_desktop."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_boottype*</td><td>".SEL_boottype."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_groupAndComputerProfile</td><td><div class=\"selectcheckedicons\">".SEL_group."</div></td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_packageSelection</td><td>".SEL_packageSelection."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_enableKioskMode</td><td>".CB_enableKioskMode."</td></tr>
				<tr ".HTML_rowColor($rowColor)."><td>$I18N_preferences</td><td>".BUT_save_preference."</td></tr>
				<tr><td colspan=\"2\" align=\"center\">".BUT_submit."</td></tr>
			</table>
		</div></td>
	<tr></table>
	");


echo("
<script language=\"javascript\">

function updateDesktops(sourcename, desktop)
{
	// Read the sourcename from SEL_sourcename or function parameter?
	if (sourcename == 'xxx')
		var sourcename = document.getElementById('SEL_sourcename').value;

	// Read the desktop from SEL_desktop or function parameter?
	if (desktop == 'xxx')
		var desktop = document.getElementById('SEL_desktop').value;

	// console.log(sourcename);
	// console.log(desktop);

	// Fetch the list of available desktops for the chosen distro
	$.ajax({
		url: 'http://127.0.0.1/m23admin/getDesktopsJSON.php?sourcename='+sourcename,
		type: 'get',
		success: function(data){

// 			console.log(data);

			// Remove all options
			$('#SEL_desktop').find('option').remove();

			$.each(data, function(key, desktopName){
				// Use the Option() constructor to create a new HTMLOptionElement.
				var option = new Option(desktopName, desktopName);
				// Convert the HTMLOptionElement into a JQuery object that can be used with the append method.
				$(option).html(desktopName);
				// Append the option to our Select element.
				$('#SEL_desktop').append(option);
			});

			// Set the previously selected desktop on the new populated selection again
			document.getElementById('SEL_desktop').value = desktop;
		}
	});
}

$(document).ready(function() {
	updateDesktops('$installOptions[sourcename]', '$installOptions[desktop]');
});
</script>


<h3>$installOptions[sourcename]RAUS</h3>
");

print_r2($installOptions['sourcename']);


	
}


	switch ($_SESSION['aSC']['step'])
	{
		case 0:
			$schoolName = CSchoolManagement::GUI_showSchoolChooser(true, true);
// 			print("<h3>SN:$schoolName</h3>");

			if (isset($schoolName[0]))
			{
				$_SESSION['aSC']['schoolName'] = $schoolName;
				$_SESSION['aSC']['step'] = 1;
				
// 				print("<h3>DRIN</h3>");
			}

		break;
	}


// 	if ($_SESSION['schMan']['schoolName'][0])
// 	{
// 		$_SESSION['aSC']['step'] = 1;
// 		$_SESSION['aSC']['schoolName'] = $_SESSION['schMan']['schoolName'];
// 		print('<h3>aSC: '.$_SESSION['aSC']['schoolName'].'</h3>');
// 	}


	switch ($_SESSION['aSC']['step'])
	{
/*		case 0:
			$schoolName = CSchoolManagement::GUI_showSchoolChooser(true, false);
			print("<h3>SN:$schoolName</h3>");
		break;*/
		case 1:
			showBasicSchoolClient();
		break;
	}

	HTML_setPage('addSchoolClient');

	HTML_showFormEnd(false);
	help_showhelp($helpPage);




?>
