<?PHP
/*
Description: Generates a automatic partition and formating job for the drive with the highest capacity.
Priority:4
*/

include ('m23CommonInstallRoutines.php');
include_once('/m23/inc/distr/debian/clientConfigCommon.php');

function run($id)
{
	$clientName = CLIENT_getClientName();

	$CFDiscAutomaticO = new CFDiscAutomatic($clientName);
	$CFDiscAutomaticO->fdiskSessionReset(true);
	$CFDiscAutomaticO->createAutoPartitionFormatJob();

	$installOptions = CLIENT_getAllAskingOptions();
	if (strlen($installOptions['distr'])>0)
	{
		include_once("/m23/inc/distr/$installOptions[distr]/clientInstall.php");
// 		include_once("/m23/inc/distr/$installOptions[distr]/clientConfig.php");
	}
	DISTR_startInstall($clientName, $installOptions['desktop'], $installOptions['instPart'], $installOptions['swapPart']);

	sendClientStatus($id,"done");
	executeNextWork();
}
?>
