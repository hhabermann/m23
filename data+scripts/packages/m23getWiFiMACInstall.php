<?PHP
/*
Description:Sends Wifi device names and according MAC addresses to the m23 server.
Priority:90
*/

include ('m23CommonInstallRoutines.php');
include ('/m23/inc/distr/debian/clientConfigCommon.php');

function run($id)
{
	$lang = getClientLanguage();
	include("/m23/inc/i18n/".I18N_m23instLanguage($lang)."/m23inst.php");

	MSR_WiFiMACCommand();

	/* =====> */ MSR_statusBarIncCommand(100);

	sendClientStatus($id,"done");
	executeNextWork();
}
?>